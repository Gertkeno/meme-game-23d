#version 330 core

in vec2 TexCoord;
in float Dark;

uniform uint sheetWidth;
uniform uint sheetHeight;
uniform uint sheetFrame;

uniform float negativeColor;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float overa;
uniform vec4 colorMod;
uniform float fade;


void main()
{
	vec4 col0;
	vec2 ftc;
	ftc.x = (TexCoord.x + sheetFrame%sheetWidth) * 1/sheetWidth;
	ftc.y = (TexCoord.y + min ( uint ( sheetFrame/sheetWidth ), sheetHeight )) * 1/sheetHeight;
	col0 = texture2D ( tex0, ftc );
	col0 = col0 * colorMod;
	vec4 col1 = texture2D ( tex1, TexCoord );

	if ( TexCoord.y * TexCoord.x <= overa )
	{
		col0 = mix( col0, col1, col1.a * overa );
	}
	if ( TexCoord.x < negativeColor )
	{
		col0.xyz = 1 - col0.xyz;
	}
	gl_FragColor = vec4 ( col0.r * Dark * fade, col0.g * Dark * fade, col0.b * Dark * fade, col0.a );
}
