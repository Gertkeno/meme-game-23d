#version 330 core

in vec3 position;
in vec2 texcoord;
in vec3 vnormal;

out vec2 TexCoord;
out float Dark;

uniform float shading;
uniform float selected;
uniform uint time;
uniform mat4 model;
uniform mat4 normMod;

const float FULL_RAD = 6.283185307179586476925286766559005768394338798750211641949;
const float tselectedRatio = 1000*3;

void main()
{
	TexCoord = vec2( texcoord.x, 1 - texcoord.y );

	vec3 ctNorm = normalize( vec3( normMod * vec4( vnormal, 1.0 ) ) );
	Dark = ( dot( normalize( vec3( 0.0, 0.0, -1.0 ) ), ctNorm ) );
	Dark = clamp( Dark, shading, 1.0 );

	gl_Position = model * vec4( position, 1.0 );
	gl_Position.x += selected * ( sin( float(time)/tselectedRatio * FULL_RAD )*model[0][0]*0.23f );
}
