#version 330 core
in vec3 positon;
out vec3 textureDir;

uniform mat4 projection;
uniform mat4 view;

void main()
{
	vec4 pos = projection * view * vec4(position, 1.0);
	gl_Position = pos.xyww;
	TexCoords = position;
}
