#version 330 core

in vec3 position;
in vec2 texcoord;
in vec3 vnormal;

uniform vec3 light;
uniform float shading;

out float Dark;
out vec2 TexCoord;
uniform mat4 model;
uniform mat4 normMod;

uniform float zFarFog;

void main()
{
	TexCoord = vec2( texcoord.x, 1 - texcoord.y );

	vec3 ctNorm = normalize( vec3( normMod * vec4( vnormal, 1.0 ) ) );
	Dark = dot( normalize( light ), ctNorm );

	gl_Position = model * vec4( position, 1.0 );

	float farMax = zFarFog * 0.9f;
	float farMin = farMax * 0.87f;
	float fog = 1 - clamp( ( gl_Position.z - farMin ) / ( farMax - farMin ), 0.0, 1.0 );
	Dark = min( fog, Dark );
	Dark = clamp( Dark, shading, 1.0 );
}
