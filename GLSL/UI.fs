#version 330 core

in vec2 TexCoord;
in float Dark;

uniform uint sheetWidth;
uniform uint sheetHeight;
uniform uint sheetFrame;
uniform vec4 colorMod;

uniform float alphaX;

uniform sampler2D tex0;
uniform float selected;

const float ALPHA_X_MIN = 0.08;
const float ALPHA_X_RECIPROCAL = 1/ALPHA_X_MIN;

void main()
{
	vec4 col0;
	vec2 ftc;
	ftc.x = (TexCoord.x + sheetFrame%sheetWidth) * 1/sheetWidth;
	ftc.y = (TexCoord.y + min ( uint ( sheetFrame/sheetWidth ), sheetHeight )) * 1/sheetHeight;
	col0 = texture2D ( tex0, ftc );

	col0 = col0 * colorMod;
	col0.b -= ftc.y * selected;
	col0 = vec4 ( col0.rgb * Dark, col0.a );
	if( TexCoord.x > alphaX )
	{
		col0.a = 0;
	}
	else if( TexCoord.x > alphaX - ALPHA_X_MIN )
	{
		float d = alphaX - TexCoord.x;
		col0.g = min( col0.g, d*ALPHA_X_RECIPROCAL );
		col0.r = min( col0.r, d*ALPHA_X_RECIPROCAL );
	}
	gl_FragColor = col0;
}
