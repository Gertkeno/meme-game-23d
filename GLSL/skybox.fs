#version 330 core
in vec3 textureDir;
uniform samplerCube cubeMap;

void main()
{
	gl_FragColor = texture( cubeMap, textureDir );
}
