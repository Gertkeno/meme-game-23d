#ifndef HURTCIRCLE_H
#define HURTCIRCLE_H

struct Sphere;
struct MovingActor;
class HitCircle;

class HurtCircle
{
	public:
		HurtCircle();
		virtual ~HurtCircle();

		Sphere* area;
		HurtCircle operator=( const Sphere& foo );
		MovingActor* target;

		bool check_collision( HitCircle* hitc ) const;
};

#endif // HURTCIRCLE_H
