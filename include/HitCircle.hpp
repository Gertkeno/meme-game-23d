#ifndef HITCIRCLE_H
#define HITCIRCLE_H

struct Sphere;
class MovingActor;

class HitCircle
{
	public:
		HitCircle();
		virtual ~HitCircle();

		void start( float damage, const MovingActor* from, const Sphere& pos );

		void update( void );

		Sphere* area;

		float damage;
		bool * confirmHit;
		const MovingActor* source;

		float maxTime;
		bool get_active( void );
		void kill( void );
	protected:
		float _lifeTime;
};

#endif // HITCIRCLE_H
