#ifndef PLAYER_H
#define PLAYER_H

#include <MovingActor.hpp>
#include <glm/glm.hpp>

class HurtCircle;
typedef unsigned char byte;

class Player: public MovingActor
{
	public:
		Player();
		~Player();

		void start( void );
		void update( void );
		void draw( void );
		void reticle_draw( void );
		void reset();

		bool get_active( void );
		bool get_hurt( float damage, const HurtCircle* from );

		enum bonuses: byte
		{
			SHEILD       = 1 << 0, //after some time free hit
			BIG_SHOT     = 1 << 1, //third shot is big and double damages
			MORE_SPEED   = 1 << 2, //move even faster when boosting
			REVENGE      = 1 << 3, //getting hit shoots back
			EXPRESS_SHOT = 1 << 4, //shots fired while boosting go faster, do more damage
			RING_SHOT    = 1 << 5, //lots of shots; shoot+special
			LASER        = 1 << 6, //high damage laser; hold stay+special
			BARREL_ROLL  = 1 << 7, //invincible roll; boost+special
		};

		float health;
		bonuses get_bonuses();
		bool equal_bonus( bonuses compare );
		void include_bonus( bonuses add );
		glm::vec3 get_reticle( void );
	private:
		glm::vec3* _delta;
		glm::vec3* _reticle;
		glm::vec3* _thruster;

		///attacking
		bonuses _myBonus;
		float _lastShot;
		float _specialTimer;
		unsigned _shotCount;
		float _sheildTimer;
		float _rollTime;
		bool _laserOn;

		///animation
		float _leanYNormal;
		float _leanXNormal;
		bool _active;
		glm::mat4* _lastModelTrans;

		///Pain
		HurtCircle* _myHurt;
		bool _frameInvicible;
		float _iTime;
};

#endif // PLAYER_H
