#ifndef MOVINGACTOR_H
#define MOVINGACTOR_H

#include <cmath>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/glm.hpp>
struct Sphere;
class HurtCircle;

constexpr float Z_SPEED_DEFAULT{ 6.0f };
constexpr double TAU{ M_PI*2 };

constexpr float RECIPROCAL( float in )
{
	return in == 0.0f ? 0.0f : 1.0f/in;
}

class MovingActor
{
	public:
		MovingActor();
		virtual ~MovingActor();

		virtual void update( void );
		virtual bool get_hurt( float damage, const HurtCircle* from ) = 0;
		glm::vec3 get_vec_point( void ) const;
		void set_vec_point( const glm::vec3& pos );
	protected:
		glm::mat4* _model;
};

#endif // MOVINGACTOR_H
