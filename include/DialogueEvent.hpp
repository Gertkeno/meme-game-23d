#ifndef DIALOGUEEVENT_H
#define DIALOGUEEVENT_H

typedef unsigned char uint8_t;
#include <string>
#include <Texture.hpp>

namespace gert
{
	class Wavefront;
	class Font;
}
extern gert::Wavefront* gModels;
extern gert::Font* gFont;

class DialogueEvent
{
	public:
		DialogueEvent();
		virtual ~DialogueEvent();

		void draw( const float& rot );

		void* soundClip;
		uint8_t modelN;
		uint8_t textureN;
		bool rightSide;

		void set_text( const char* foo );
		const char* get_text( void );
		const static std::string* player_name;

		//background image
		float bgFadeTime;
		uint8_t bgIndex;
	protected:
		std::string _myText;
		float _rotateRandom;
};

#endif // DIALOGUEEVENT_H
