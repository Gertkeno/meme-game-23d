#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <MovingActor.hpp>
#include <glm/detail/type_vec3.hpp>

class HitCircle;
typedef unsigned char uint8_t;

class Projectile : public MovingActor
{
	public:
		Projectile();
		virtual ~Projectile();

		void start( const glm::vec3& targetPos, const glm::vec3& startPos, float speed, float life, float damage, MovingActor* source, float scale = 0.4f );
		void update( void );
		void draw( void );

		void set_scale( float scale );

		bool get_hurt( float damage, const HurtCircle* from );
		bool get_active( void );

		uint8_t useModel;
		uint8_t useTex;
		MovingActor* source;
		float maxTime;
		bool * confirmHit;
	protected:
		glm::vec3* _delta;
		float _damage;
		float _lifeTime;
		float _scale;
		bool _destroy;

		//animation
		glm::vec3* _angle;
	private:
};

#endif // PROJECTILE_H
