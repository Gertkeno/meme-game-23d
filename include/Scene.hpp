#ifndef SCENE_MANAGER_H
#define SCENE_MANAGER_H
typedef unsigned char byte;

namespace Scene
{
	bool open_file( const char* filename );

	enum end_t: byte
	{
		CONTINUE,
		TITLE_SCREEN,
		REPEAT_BOSS,
		HOOPS_START,
		PLAY_GAME,
		MEME_PATH,
		MOD_PATH,
		CREDITS,
	};
	end_t next();
	void draw();
	bool get_skipping();
}

#endif //SCENE_MANAGER_H
