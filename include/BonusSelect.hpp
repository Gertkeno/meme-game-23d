#ifndef BONUS_SELECT_H
#define BONUS_SELECT_H

namespace BonusSelect
{
	void init();
	bool update();
	struct Card
	{
		unsigned char bonus, cost;
	};
	Card get_select();
	void draw( int s, unsigned b );
}

#endif
