#ifndef BOSSBASE_H
#define BOSSBASE_H

#include <cstdlib>
#include <MovingActor.hpp>

class HurtCircle;
typedef unsigned char byte;

#define BOSS_BASE_ATTACK \
	attacks _lastAttack;\
	float _get_attack_time( attacks t ) const;\
	byte _get_attack_count( attacks t ) const;

template<typename T, T max>
T random_fresh( const T& start )
{
	using utype = typename std::underlying_type<T>::type;
	constexpr auto umax{ static_cast<utype>(max) };
	static_assert( umax != 0, "Cannot have 0 maximum" );
	const auto randget{ std::rand()%umax };
	return static_cast<T>( randget == static_cast<utype>(start) ? (randget+1)%umax : randget );
}

class BossBase : public MovingActor
{
	public:
		BossBase();
		virtual ~BossBase();

		virtual void update( void ) = 0;
		virtual void draw( void ) = 0;
		virtual void start( void );

		virtual bool get_alive( void ) const;
		virtual float get_health( void ) const = 0;
		virtual float get_misc( void ) const = 0;
		virtual const char* get_dialogue_file( bool dying = false ) const;
		virtual unsigned get_music( void ) const = 0;
	protected:
		byte _attackProc;
		bool _firstProc;
		float _health;
		float _iTime;
		//HurtCircle** _myHurt;
		float _attackTimer;
	private:
};

#endif // BOSSBASE_H
