#ifndef GERT_TIMER_H
#define GERT_TIMER_H

namespace gert
{
	class Timer
	{
		public:
			Timer( float max ): _max(max), _data(0.0f){}
			bool operator++();
			operator float() const { return _data; }
		private:
			 const float _max;
			 float _data;
	};
}
#endif
