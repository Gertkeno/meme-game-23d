#ifndef MODELLIST_H
#define MODELLIST_H

namespace gert
{
	class Wavefront;
	class Texture;
	class Shader;
	class Font;
	class MusicTrack;
}
typedef unsigned char uint8_t;

namespace model
{
	enum type_t: uint8_t
	{
		///Profiles
		AI_PROF, //0
		PLAYER_PROF, //1
		COMMANDER, //2
		MAKIN, //3
		TOASTER, //4
		ELLIS, //5
		DEACON, //6
		SUPER_SAGE, //7
		SUNBUNNI, //8
		CLOWN, //9
		///other
		PLAYER_SHIP,
		DEBUG_SPHERE,
		PLANE_2D,
		BASIC_SHOT,
		APPLE,
		CYLINDER,
		CUBE,
		BREAD,
		RING,
		STAR,
		///Bosses
		MAKIN_SHIP,
		LETTER_M,
		LETTER_E,
		LETTER_G,
		LETTER_A,
		LETTER_TWO,
		LETTER_THREE,
		LETTER_D,
		TOTAL
	};
}
extern gert::Wavefront* gModels;

namespace texture
{
	const char USE_LAST = -1;
	enum type_t: uint8_t
	{
		///Profiles
		AI_PROF,    //0
		PLAYER_PROF,//1
		COMMANDER,  //2
		MAKIN,      //3
		TOASTER,    //4
		ELLIS,      //5
		DEACON,     //6
		SUPER_SAGE, //7
		SUNBUNNI,   //8
		BOBO,       //9
		FONZI,      //10
		///other
		PLAYER_SHIP,
		THRUSTER_EFFECT,
		RED_TEST,
		YELLOW_TEST,
		BLOOD_TEX,
		ELECTRIC,
		RETICLE,
		APPLE,
		CRACK_TEX,
		HEALTH_BAR_BASIC,
		MAKIN_SHIP,
		PURP_NEBULA,
		TEMP_STORAGE,
		DOG_0,
		DOG_1,
		GOGGLES,
		OPENING_CARDS,
		POWER_ICONS,
		CREDITS_JPG,
		TOTAL
	};
}
extern gert::Texture* gTextures;

namespace shading
{
	enum type_t: uint8_t
	{
		OBJ_LIGHTING,
		FLAT_UI,
		TOTAL
	};
}
extern gert::Shader* gShaders;

extern gert::Font* gFont;

namespace music
{
	enum type_t: unsigned
	{
		TARGETS,
		UNFOUNDED_REVENGE,
		SPARTA_TOASTER,
		COME_DEPART,
		JUNGLE_FEVER,
		THIS_WAY_THAT,
		UH_OH,
		DINO_LAND,
		WOOD_MAN,
		ZERO_TWO,
		TOTAL
	};
}

extern gert::MusicTrack * gMusic;

#endif // MODELLIST_H
