#ifndef DRAW_LIST_H
#define DRAW_LIST_H
#include <glm/glm.hpp>
#include <ModelList.hpp>

/* drawlist is a container for wavefront.draw calls
 * arguments as follow:
 * ====================
 * model mat4
 * model index
 * texture index
 * color vec4
 * draw priority
 * texture frame
 * 2nd texture composite rendering percent
 * 2nd texture index
 * forced shading amount, 0 = use light normals
 */

namespace DrawList
{
	struct Call
	{
		Call( glm::mat4 a,
				model::type_t m,
				texture::type_t t,
				glm::vec4 c,
				float p = 0.0f,
				int f = 0u,
				float o = 0.0f,
				int ot = 0u,
				float s = 0.0f,
				float n = 0.0f
			);

		glm::mat4 model;
		model::type_t index;
		texture::type_t texture;
		glm::vec4 color;
		float priority;//low priority draws first
		uint16_t frame;

		float overa;
		uint16_t overaTexture;

		float shading;
		float negative;
	};

	void add( const Call& );
	void flush();
	void draw_all();
	void destroy_calls();
}

#endif //DRAW_LIST_H
