#ifndef CONTROLLER_LIST_H
#define CONTROLLER_LIST_H
#include <string>
#include <Controller.hpp>

namespace control_t
{
	enum
	{
		CONFIRM,
		SPECIAL,
		UP,
		DOWN,
		LEFT,
		RIGHT,
		PAUSE,
		BOOST,
		BRAKE,
		STAY,
		TOTAL
	};
}

extern gert::Controller gControls;

namespace gert
{
	bool configure_controller( const SDL_Event* );
	void draw_configurer( void(*)(const char *, const char *) );
	std::string name_control( size_t );
}

#endif
