#ifndef TITLESCREEN_H
#define TITLESCREEN_H

typedef unsigned char byte;
union SDL_Event;
class SDL_Point;

namespace TitleScreen
{
	void init();
	void destroy();

	enum options: byte
	{
		RESUME,
		LOAD_GAME,
		NEW_GAME,
		T_FULLSCREEN,
#ifdef _GERT_DEBUG
		SCENE_TEST,
#endif
		CARD_TEST,
		CONTROLLER_CONFIG,
		CREDITS,
		QUIT,
		TOTAL
	};

	void reset_animation();
	void set_inset( options );
	void update( void );
	void draw( void );

	void set_y_invert_text( bool );
};

#endif // TITLESCREEN_H
