#ifndef HOOP_TARGET_H
#define HOOP_TARGET_H

#include <MovingActor.hpp>
class Target: public MovingActor
{
	public:
		Target();
		~Target();
		void init();
		void start( const glm::vec3& );
		void draw() const;

		bool get_hurt( float, const HurtCircle * );
		bool * hitConfirm;
	private:
		HurtCircle * _hurt;
};

#endif
