#ifndef GERT_MUSIC_TRACK_H
#define GERT_MUSIC_TRACK_H

#include <SDL2/SDL_audio.h>

namespace gert
{
	class MusicTrack
	{
		public:
			MusicTrack (const unsigned char *, const unsigned);
			MusicTrack (const unsigned char *, const unsigned, unsigned);

			const unsigned char * const trackStart;
			const unsigned int loopBegin;
			const unsigned int trackEnd;
			unsigned int currentPosition;

			void (*on_loop)();

			static void que_track (MusicTrack *);
			static void pause_track (bool p = true);
			static void init();
			static void destroy();
		private:
			static SDL_AudioSpec _spec;
			static MusicTrack * _working;
			static int _audioDevice;
	};
}
#endif
