#ifndef HOOP_GAME_H
#define HOOP_GAME_H

namespace HoopGame
{
	void start();

	bool update();
	bool active();
	void draw();
	float get_dist_remain();
	unsigned frame_score();

	unsigned short get_combo();
}

#endif //HOOP_GAME_H
