#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <glm/glm.hpp>
typedef unsigned char byte;
union SDL_Event;

class HurtCircle;
class HitCircle;
class Projectile;

#define IF_HURT_GET( p ) if( ( p = GameManager::get_hurt_circle() ) != nullptr )
#define IF_HIT_GET( p ) if( ( p = GameManager::get_hit_circle() ) != nullptr )
#define IF_SHOT_GET( p ) if( ( p = GameManager::get_projectile() ) != nullptr )

namespace GameManager
{
	void init( void );
	void destroy( void );

	void update( void );
	void draw( void );

	HurtCircle* get_hurt_circle( void );
	HitCircle* get_hit_circle( void );
	Projectile* get_projectile( void );

	enum state_t: byte
	{
		gmsPLAYING,
		gmsHOOPS,
		gmsCUT_SCENE,
		gmsNAME_ENTRY,
		gmsTITLE_SCREEN,
		gmsOPENING_CARDS,
		gmsCONTROLLER_CONFIG,
		gmsBOSS_SELECT,
		gmsBONUS_SELECT,
		gmsQUITTING,
		gmsMODEL_VIEW,
		gmsCREDITS,
		gmsNULL,
	};

	extern state_t gameState;
	void change_game_state( state_t ct );
	bool fade_game_state( state_t ct );
	glm::vec3 get_player_pos( void );
	glm::vec3 get_boss_pos( void );

	///this is just to make button functions easier
	enum bfhGuide: byte
	{
		bfhRESUME,
		bfhNEW_GAME,
		bfhSCENE_TEST,
		bfhCARD_TEST,
		bfhMAKIN_RAN,
		bfhCREDIT_START,
		bfhTOTAL
	};
	void help_button_func( bfhGuide foo );
	void clear_resources( void );
	void load_game_file( void );
	void save_game( void );

	bool get_player_invert( void );
};
#endif // GAMEMANAGER_H
