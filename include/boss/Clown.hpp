#ifndef CLOWN_BOSS_H
#define CLOWN_BOSS_H

#include <BossBase.hpp>

namespace boss
{
	class Clown: public BossBase
	{
		public:
			Clown( bool fonz );
			~Clown();

			void update();
			void draw();
			void start();

			const char * get_dialogue_file( bool d = false ) const;
			bool get_hurt( float, const HurtCircle * );

			float get_health() const;
			float get_misc() const;
			unsigned get_music() const;
		private:
			HurtCircle * _myHurt;
			const bool _fonzi;
			const int _texture;

			struct
			{
				glm::vec3 pos;
				glm::vec3 delta;
				float spread;
			} _cluster;

			glm::vec2 _spewPos;
			glm::vec3 _exact_spew_pos() const;

			float _laserStart;
			float _get_laser_x() const;

			static constexpr int MAX_BOMBS{ 10 };
			struct Bomb
			{
				glm::vec3 pos;
				float fuse;
			} _bombs[MAX_BOMBS];

			Bomb * _unused_bomb();

			enum attacks_t: unsigned char
			{
				CLUSTER,
				SPEWER,
				BOX_LASER,
				ODD_BOMBS,
				TOTAL
			} _lastAttack;

			byte _get_attack_count( attacks_t t ) const;
			float _get_attack_time( attacks_t t ) const;
			bool _attack_expired() const;
			void _attack_update();

			void _for_cluster_point( void(*f)(glm::vec3, const MovingActor*) ) const;
	};
}

#endif
