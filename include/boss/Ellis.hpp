#ifndef ELLIS_FREEMAN_H
#define ELLIS_FREEMAN_H

#include <BossBase.hpp>
#include <glm/detail/type_vec3.hpp>

typedef unsigned char byte;

namespace boss
{
	class Ellis: public BossBase
	{
		public:
			Ellis();
			~Ellis();

			void update();
			void draw();
			void start();

			bool get_alive() const;
			float get_health() const;
			float get_misc() const;
			bool get_hurt( float damage, const HurtCircle* from );
			const char* get_dialogue_file( bool dying = false ) const;
			unsigned get_music() const;
		private:
			enum attacks: byte
			{
				RANDOM_SHOOTERS,
				POWER_UP,
				LASERS,
				TOTAL
			};
			BOSS_BASE_ATTACK;

			void _cycle_attack();
			bool _attack_update( attacks );

			static constexpr byte MAX_BUDS = 14u;
			struct Bud
			{
				float health;
				float redTime;
				float deadTime;
				glm::vec3 point;
				HurtCircle* hurt;
			} _buddies[ MAX_BUDS ];
			byte _activeBuds;

			void _buds_reset();
			Bud* _inactive_bud();
			Bud* _random_active_bud();
			Bud* _far_bud();
			bool _bud_dead( byte index );
			bool _bud_dead( Bud* ptr );
			//start bud t in area around vec3 h
			bool _start_bud( Bud* t, glm::vec3 h );
			template<typename Func>
			void _per_active( Func&& x )
			{
				for( auto i = 0u; i < MAX_BUDS; ++i )
				{
					if( _bud_dead( i ) ) continue;
					x( &_buddies[i] );
				}
			}

			//specific skill
			float _budTimer;
			Bud* _poweringBud;
	};
}

#endif //ELLIS_FREEMAN_H
