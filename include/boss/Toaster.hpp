#ifndef TOASTER_H
#define TOASTER_H
#include <BossBase.hpp>
#include <glm/detail/type_vec3.hpp>

typedef unsigned char byte;

namespace boss
{
	class Toasterman: public BossBase
	{
		public:
			Toasterman();
			~Toasterman();

			void update();
			void draw();
			void start();

			bool get_alive() const;
			float get_health() const;
			float get_misc() const;
			bool get_hurt( float damage, const HurtCircle* from );
			const char* get_dialogue_file( bool dying = false ) const;
			unsigned get_music() const;
		protected:
			HurtCircle* _myHurt;
			float _bide;
			enum attacks: byte
			{
				BIDE,
				CROSS,
				LOCK_ON_ISH,
				TOTAL
			};
			BOSS_BASE_ATTACK;
			glm::vec3 _point;

			static constexpr byte MAX_TOAST=2u;
			struct
			{
				glm::vec3 point;
				float timer;
			} _toast[MAX_TOAST];

			struct
			{
				glm::vec3 point;
				glm::vec3 offset;
			} _reticle;

			bool _process_attack( attacks type );
			void _start_new_attack();
	};
}

#endif //TOASTER_H
