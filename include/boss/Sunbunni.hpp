#ifndef BOSS_SUNBUNNI_H
#define BOSS_SUNBUNNI_H

#include <BossBase.hpp>
#include <glm/glm.hpp>
#include <Collision.hpp>
#include <Timer.hpp>

namespace boss
{
	class Sunbunni: public BossBase
	{
		public:
			Sunbunni();
			~Sunbunni();

			void update();
			void draw();
			void start();

			const char* get_dialogue_file( bool d = false ) const;
			bool get_hurt( float damage, const HurtCircle* from );

			float get_health() const;
			float get_misc() const;
			unsigned get_music() const;
		private:
			enum struct attacks_t: byte
			{
				EXPAND_HURT,
				CHANGE_VECTOR,
				PROTECT_RING,
				CROSS_LASER,
				TOTAL,
			} _lastAttack;

			struct
			{
				Sphere position;
				float velocity;
				glm::vec3 vector;
				float blueTime;
			} _ring;
			HurtCircle * _myHurt;

			glm::vec2 _cross;

			byte _get_attack_count( attacks_t t ) const;
			float _get_attack_time( attacks_t t ) const;
			bool _attack_expired() const;
			void _attack_update();
			void _randomize_ring_vector();
	};
}

#endif
