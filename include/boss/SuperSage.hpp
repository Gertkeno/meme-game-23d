#ifndef SUPER_SAGE_H
#define SUPER_SAGE_H

#include <BossBase.hpp>
#include <glm/glm.hpp>
#include <Timer.hpp>

namespace boss
{
	class SuperSage: public BossBase
	{
		public:
			SuperSage();
			~SuperSage();

			void update();
			void draw();
			void start();

			const char* get_dialogue_file( bool dying = false ) const;
			bool get_hurt( float damage, const HurtCircle* from );

			float get_health() const;
			float get_misc() const;
			unsigned get_music() const;
		private:
			gert::Timer _hashTagClock;
			bool _hashHit;

			HurtCircle* _myHurt;
			enum struct attacks_t: byte
			{
				APPLE_GUARD,
				LASER,
				BIG_HASH,
				TOTAL
			} _lastAttack;

			float _forceAttack;
			float _appleClock;
			HurtCircle * _apple;
			bool _apple_alive() const;

			struct
			{
				glm::vec2 pos, delta;
			} _laser;

			byte _get_attack_count( attacks_t t ) const;
			float _get_attack_time( attacks_t t ) const;

			void _attack_update();
			void _attack_draws() const;
			bool _attack_expired() const;
	};
}

#endif //SUPER_SAGE_H
