#ifndef DEACON_H
#define DEACON_H
#include <BossBase.hpp>
#include <glm/detail/type_vec3.hpp>
/*Make several fake renders of Deacon, spin in a circle; faster with lower health, all attacks should use fakers*/

namespace boss
{
	class Deacon: public BossBase
	{
		public:
			Deacon();
			virtual ~Deacon();

			void update();
			void start();
			void draw();

			bool get_alive() const;
			float get_health() const;
			float get_misc() const;

			bool get_hurt( float damage, const HurtCircle* from );
			const char* get_dialogue_file( bool dying = false ) const;
			unsigned get_music() const;
		private:
			HurtCircle** _myHurt;

			enum attacks: uint8_t
			{
				CHANGE_GHOST,
				EXPAND,
				SEQUENCE_SHOT,
				BIG_BALL,
				TOTAL
			};
			BOSS_BASE_ATTACK;
			glm::vec3* _ghostOffsets;
			glm::vec3 _midPoint;
			uint8_t _realGhostIndex;
			float _ghostSpin;
			float _ghostDistance;
			char _distanceIncrement;

			struct
			{
				HurtCircle *hurt;
				char health;
				glm::vec3 pos;
			} _ballAttack;
	};
}

#endif // DEACON_H
