#ifndef DAD_BOSS_H
#define DAD_BOSS_H

#include <BossBase.hpp>

namespace boss
{
	class Dad: public BossBase
	{
		public:
			Dad();
			~Dad();

			void update();
			void draw();
			void start();

			const char * get_dialogue_file( bool d = false ) const;
			bool get_hurt( float, const HurtCircle * );

			float get_health() const;
			float get_misc() const;
			unsigned get_music() const;
		private:
			HurtCircle* _myHurt;
			enum struct attacks_t: byte
			{
				LASER,
				JUST_SHOOTING,
				EXPANDING_LASER,
				MATCH_DIST,
				TOTAL
			} _lastAttack;

			struct
			{
				glm::vec2 point;
				float rotation;
				float distance;
			} _orbiter;

			glm::vec3 _exact_orbit() const;

			void _attack_update();
			void _attack_draws() const;
			bool _attack_expired() const;

			byte _get_attack_count( attacks_t t ) const;
			float _get_attack_time( attacks_t t ) const;
	};
}

#endif
