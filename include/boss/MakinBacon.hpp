#ifndef MAKINBACON_H
#define MAKINBACON_H

#include <BossBase.hpp>

namespace boss
{
	class MakinBacon : public BossBase
	{
		public:
			MakinBacon();
			virtual ~MakinBacon();

			void update( void );
			void draw( void );
			void start( void );

			bool get_hurt( float damage, const HurtCircle* from );
			bool get_alive( void ) const;
			float get_health( void ) const;
			float get_misc( void ) const;

			const char* get_dialogue_file( bool dying ) const;
			unsigned get_music() const;
		private:
			HurtCircle* _myHurt;
			enum attacks: byte
			{
				SHOOTY,
				LASER,
				SHEILD,
				CIRCLE_SHOT,
				TOTAL
			};
			BOSS_BASE_ATTACK

				glm::vec3 _targetPos;
	};
}

#endif // MAKINBACON_H
