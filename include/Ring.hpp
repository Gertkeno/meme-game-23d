#ifndef RING_H
#define RING_H

#include <glm/glm.hpp>
class Ring
{
	public:
		Ring();
		~Ring();
		void update();
		void draw();

		void start( const glm::vec3& );
		bool is_collected();
		bool is_dead();
	private:
		glm::vec3 _position;
		bool _collect;
		float _sinceCollect;
};

#endif //RING_H
