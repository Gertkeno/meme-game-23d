#ifndef BUTTON_FUNCTION_H
#define BUTTON_FUCNTION_H

namespace gert
{
	class Button;
}

namespace button_function
{
	using namespace gert;
	void basic_draw( Button* b );

	void resume_game( void );
	void new_game( void );
	void load_game( void );
	void quit_game( void );
	void toggle_fullscreen( void );
	void controller_config( void );
	void keyboard_config( void );
	void invert_controller_y( void );

	void scene_test( void );
	void card_test( void );
	void credit_start( void );
}
#endif // BUTTON_FUNCTION_H
