#ifndef WINDOW_LOADER_H
#define WINDOW_LOADER_H

typedef unsigned Uint32;
struct SDL_Window;

namespace window_Load
{
	struct Settings
	{
		int sw, sh;
		Uint32 flag;
	};

	Settings open_settings();
	bool save_settings( SDL_Window* dat );
}

#endif // WINDOW_LOADER_H
