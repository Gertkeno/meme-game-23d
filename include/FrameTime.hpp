#ifndef FRAME_TIME_H
#define FRAME_TIME_H

namespace FrameTime
{
	double get_pure();
	double get_mod();
	void set_mod( double set );

	void update();
	unsigned since_update();
}

#endif //FRAME_TIME_H
