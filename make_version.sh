echo "[GERT] UPDATING VERSION"
FILE_PREFIX="./"
if [ -n "${1}" ]
then
	FILE_PREFIX="${1}"
fi

FILE="${FILE_PREFIX}include/Version.h"
echo "- at file [${FILE}]"
echo -n "#define VERSION \"" > $FILE
git rev-parse --short HEAD | xargs echo -n >> $FILE
echo "\"" >> $FILE
