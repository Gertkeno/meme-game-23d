https://www.gnu.org/licenses/gpl.html

## required libraries
- [SDL2](https://www.libsdl.org/download-2.0.php)
- [SDL2 ttf](https://www.libsdl.org/projects/SDL_ttf/)
- [SDL2 image](https://www.libsdl.org/projects/SDL_image/)
- [glm](https://glm.g-truc.net/0.9.8/index.html)
- [GLEW](http://glew.sourceforge.net/)

## helpful apps
- [cmake](https://cmake.org/)

## using apt
`apt install cmake libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev libglew-dev libglm-dev`

## building
`cmake . && make`

This command in the base directory should successfully build the app, run with `./MG23D`

The game uses relative paths to find assets and shaders so it'd be best to run `MG23D` from the base directory

## notes about building
Only tested with glm version 9.7 and up. Using C++14
