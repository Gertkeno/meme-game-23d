echo "====Baking assets"
cd $1
echo "[INFO] moved to ${1}"
for i in $( ls | grep '.jpg$\|.png$' )
do
	echo "[INFO] Processing: ${i}"
	xxd -i ${i} ${i}.c
done
echo "====Done"
