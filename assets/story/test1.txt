#TEST SCENE
# anything starting with a # is a comment, stops processing the file until the next line.
# prefix 'm' sets model index; prefix 't' sets texture index model and texture index will have to be found from the 'ModelList.h' file
# keywords left or right set which side the model will be on, relative to the text box. Default is left if not set.
# '}' will start the next dialogue, keep this file in order
# seperate keywords and prefixed data with a comma ',' note that the last input does not need a comma.
# out of scope ! indicates what to do at the end of the cut scene
# out of scop <...> loads a image for later use as a background, specify which with b(1-9)

!N
<assets/texture/bloody.png>
<assets/texture/electrics.png>
<assets/texture/ship1_UV.jpg>
{ m2, b0, left, "hello hacker fucker this is garrett hale heres the deal. if you hack this game i will find you where ever you are and break your legs and thats a promise" }
{ m4, b0, right, "$, here to test dollar signs $. $---- $$$$ $$ now for regular text." }
{ m2, b0, left, "This is text, please read me. I should be the commander on the left." } #index 0
{ m1, right, "SECONDARY TEXT ALL CAPS now lower case ending in a period. Should be player character." } #index 1
{ "third text this time with the AI.", t0 } #index 2
{ m4, b1, right, t1, "hope this works some weird testing agin horay" } #index 3
{ "short", b1, m5, m4, right }
{ m8, t3, b2, left, "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea ommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." }

#bananazillion has to be said by a scientist named mike
#Julian styles
