!C
<assets/background/bg_victor.jpg>
<assets/background/bg_false_victory.jpg>
{ m0, b0, right, "AI: Great now there's clown blood on your hands." }
{ m1, b0, left, "$: Okay." }
{ m1, b0, left, "Sounds like a good end to the day." }
{ m2, b0, right, "Commander: Congratulations $, you did some things I asked of you." }
{ m2, b0, right, "Just a heads up, there's a lot of police outside my door and I snitch at the drop of a hat." }
{ m1, b1, left, "$: Do I have any special rights in this case?" }
{ m2, b1, right, "Commander: I'm not your laywer. Just take this as a life lesson to question what your told." }
{ m2, b1, right, "However, in this case you don't have much time to ponder." }
{ m1, b1, left, "$: Thanks. I'm not getting paid right?" }
{ m2, b1, right, "Commander: Correct!" }
