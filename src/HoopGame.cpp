#include <HoopGame.hpp>
#include <Ring.hpp>
#include <Target.hpp>
#include <GameManager.hpp>
#include <vector>
#include <random>

typedef unsigned char byte;
namespace HoopGame
{
	namespace
	{
		constexpr byte RING_TOTAL{ 5 };
		constexpr byte TARGET_TOTAL{ 14 };
		constexpr float DIST{ 16.0f };
		constexpr float STARTING_Z{ DIST*2 };
		constexpr int FINAL_COUNT{ 14 };
		constexpr float RANDOM_RADIUS{ 14.0f };
		constexpr int RANDOM_GRADE{ 900 };
		constexpr float RANDOM_CT_TAU{ (M_PI*2)/RANDOM_GRADE };
		constexpr float TARGET_DISTANCE{ 12.0f };

		Ring _myRings[RING_TOTAL];
		Target _myTarget;

		bool _ringCollect[ RING_TOTAL ];

		int _frameCollect{0};
		bool _frameShot{false};
		glm::vec3 _lastPos;
		int _incCount;
		bool _active{ false };
		glm::vec3 _target;
	}

	void _increment_ring( int index )
	{
		glm::vec3 pos{ _lastPos };
		const float angle{ (rand()%RANDOM_GRADE)*RANDOM_CT_TAU };
		pos.x += std::cos( angle ) * RANDOM_RADIUS;
		pos.y += std::sin( angle ) * RANDOM_RADIUS;
		_lastPos = pos;
		_lastPos.z += DIST;
		_myRings[index].start( pos );
		_ringCollect[index] = false;
		++_incCount;
	}

	void _set_target()
	{
		const int grit{ 900 };
		const float randomAngle( (std::rand()%grit) * TAU / static_cast<float>(grit) );
		_target.x = std::cos( randomAngle ) * TARGET_DISTANCE;
		_target.y = std::sin( randomAngle ) * TARGET_DISTANCE;
		_target.z = 2*TARGET_DISTANCE;
	}

	bool update()
	{
		if( _frameShot )
			_set_target();

		_frameCollect = 0;
		_frameShot = false;

		_myTarget.start( _target + GameManager::get_player_pos() );

		for( byte i = 0; i < RING_TOTAL; i++ )
		{
			_myRings[i].update();
			if( _myRings[i].is_collected() and !_ringCollect[i] )
			{
				//collected
				_frameCollect = 3;
				_ringCollect[i] = true;
			}
			if( _myRings[i].is_dead() )
			{
				//reset ring to front
				_increment_ring( i );
			}
		}
		_active = _incCount < FINAL_COUNT;
		return not _active;
	}

	bool active()
	{
		return _active;
	}

	void draw()
	{
		for( byte i = 0; i < RING_TOTAL; ++i )
			_myRings[i].draw();

		_myTarget.draw();
	}

	unsigned frame_score()
	{
		return _frameCollect + _frameShot;
	}

	void start()
	{
		_lastPos = glm::vec3(0.0f);
		_lastPos.z += STARTING_Z;
		_frameCollect = false;
		for( byte i = 0; i < RING_TOTAL; i++ )
		{
			_increment_ring( i );
		}
		_myTarget.init();
		_myTarget.hitConfirm = &_frameShot;
		_set_target();
		_incCount = 0;
	}

	float get_dist_remain()
	{
		return static_cast<float>( _incCount ) / FINAL_COUNT;
	}
}
