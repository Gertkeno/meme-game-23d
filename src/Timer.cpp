#include <Timer.hpp>
#include <FrameTime.hpp>

namespace gert
{
	bool Timer::operator++()
	{
		_data += FrameTime::get_mod();
		if( _data > _max )
		{
			_data -= _max;
			return true;
		}
		return false;
	}
}
