#include <ControllerList.hpp>

namespace gert
{
	namespace
	{
		constexpr int MINIMUM_AXIS{ 3000 };
		gert::Input temporary{ Input::type_t::NONE, 0, 0 };
		size_t index{0u};
	}

	bool configure_controller( const SDL_Event * e )
	{
		switch( e->type )
		{
			case SDL_KEYDOWN:
				if( SDLK_RETURN == e->key.keysym.sym )
				{
					gControls.mutate_value( index++, temporary );
					temporary.myType = Input::type_t::NONE;
					temporary.axisMax = 0;
					if( index >= gControls.input_count() )
					{
						index = 0u;
						return true;
					}
					else
						return false;
				}
				temporary.myType = Input::type_t::KEYBOARD;
				temporary.button = e->key.keysym.sym;
				break;
			case SDL_JOYBUTTONDOWN:
				temporary.myType = Input::type_t::JOYBUTTON;
				temporary.button = e->jbutton.button;
				break;
			case SDL_MOUSEBUTTONDOWN:
				temporary.myType = Input::type_t::MOUSE;
				temporary.button = e->button.button;
				break;
			case SDL_JOYAXISMOTION:
				if( e->jaxis.value < MINIMUM_AXIS and e->jaxis.value > -MINIMUM_AXIS )
					break;
				temporary.myType = Input::type_t::JOYAXIS;
				if( e->jaxis.axis != temporary.button )
				{
					temporary.button = e->jaxis.axis;
					temporary.axisMax = e->jaxis.value;
				}
				else if( std::abs( e->jaxis.value ) > std::abs( temporary.axisMax ) )
				{
					temporary.axisMax = e->jaxis.value;
				}
				break;
		}
		return false;
	}

	std::string name_control( size_t i )
	{
		static const std::string ctName[]{ "Confirm", "Special", "Up", "Down", "Left", "Right", "Pause", "Boost", "Brake", "Stay" };
		return ctName[i];
	}

	void draw_configurer( void(*d)(const char *, const char *) )
	{
		static const std::string NAMES[]{ "Key", "Button", "Axis", "Mouse", "None" };
		std::string text{ NAMES[static_cast<int>(temporary.myType)] };
		text += ' ';
		switch( temporary.myType )
		{
			case Input::type_t::KEYBOARD:
				text += SDL_GetKeyName( temporary.button );
				break;
			case Input::type_t::JOYBUTTON:
			case Input::type_t::MOUSE:
				text += std::to_string( temporary.button );
				break;
			case Input::type_t::JOYAXIS:
				text += std::to_string( temporary.button );
				text += " Max: ";
				text += std::to_string( temporary.axisMax );
				break;
			case Input::type_t::NONE:
				text += '?';
				break;
		}

		std::string description{ "[Return] to rebind: " + name_control(index) };

		d( description.c_str(), text.c_str() );
	}
}
