#include <HurtCircle.hpp>
#include <HitCircle.hpp>

#include <Collision.hpp>
#include <MovingActor.hpp>

HurtCircle::HurtCircle()
{
	//ctor
	area = new Sphere;
	target = nullptr;
}

HurtCircle::~HurtCircle()
{
	//dtor
	delete area;
}

bool HurtCircle::check_collision( HitCircle* hitc ) const
{
	///  check if both are active.                    source is not the same       collision check
	if( ( target != nullptr and hitc->get_active() ) and target != hitc->source and collision::get_collide( *hitc->area, *area ) )
	{
		if( target->get_hurt( hitc->damage, this ) and hitc->confirmHit != nullptr )
		{
			*(hitc->confirmHit) = true;
		}
		return true;
	}
	return false;
}

HurtCircle HurtCircle::operator=( const Sphere& foo )
{
	*area = foo;
	return *this;
}
