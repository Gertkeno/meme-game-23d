#include <WindowLoader.hpp>
#include <SDL2/SDL_video.h>

#ifdef _GERT_DEBUG
#include <iostream>
#define DE_OUT( x ) std::cout << "[WindowLoader]  " << x << std::endl
#endif // _GERT_DEBUG

namespace window_Load
{
	static const char* FILE_NAME{ "settings.dat" };
	static const char SIZE_PREFIX{ 'o' };
	static const char FLAG_PREFIX{ 'f' };

	Settings open_settings()
	{
		//making default settings
		constexpr Uint32 defFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
		Settings out;
		out.sw = 1280;
		out.sh = 720;
		out.flag = defFlags | SDL_WINDOW_RESIZABLE;
		SDL_RWops* sfile{ SDL_RWFromFile( FILE_NAME, "r" ) };
		if( sfile == nullptr )
		{
			#ifdef _GERT_DEBUG
			DE_OUT( "Couldn't find file: " << FILE_NAME );
			#endif //_GERT_DEBUG
			return out;
		}
		//import data
		SDL_RWread( sfile, &out, sizeof( out ), 1 );
		SDL_RWclose( sfile );
		#ifdef _GERT_DEBUG
		if( (out.flag & defFlags) != defFlags )
			DE_OUT( FILE_NAME << " \'s information may be incorrect, does not contain constant flags" );
		#endif //_GERT_DEBUG
		out.flag = out.flag | defFlags;
		return out;
	}

	bool save_settings( SDL_Window* dat )
	{
		SDL_RWops* sfile{ SDL_RWFromFile( FILE_NAME, "w" ) };
		if( sfile == nullptr ) return false;
		Settings out;

		//get data
		SDL_GetWindowSize( dat, &out.sw, &out.sh );
		out.flag = SDL_GetWindowFlags( dat );

		//write
		SDL_RWwrite( sfile, &out, sizeof( out ), 1 );
		SDL_RWclose( sfile );
		#ifdef _GERT_DEBUG
		DE_OUT( "wrote settings to " << FILE_NAME );
		#endif // _GERT_DEBUG
		return true;
	}
}
