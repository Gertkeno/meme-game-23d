#include <MovingActor.hpp>

#include <glm/gtc/matrix_transform.hpp>

MovingActor::MovingActor()
{
	//ctor
	_model = new glm::mat4;
}

MovingActor::~MovingActor()
{
	//dtor
	delete _model;
}

void MovingActor::update( void )
{

}

glm::vec3 MovingActor::get_vec_point( void ) const
{
	glm::vec3 retMe;
	retMe.x = (*_model)[3][0];
	retMe.y = (*_model)[3][1];
	retMe.z = (*_model)[3][2];
	/* translations
	 * 1 0 0 X
	 * 0 1 0 Y
	 * 0 0 1 Z
	 * 0 0 0 1
	 *
	 * scaling
	 * X 0 0 0
	 * 0 Y 0 0
	 * 0 0 Z 0
	 * 0 0 0 1
	 */
	return retMe;
}

void MovingActor::set_vec_point( const glm::vec3& pos )
{
	(*_model)[3][0] = pos.x;
	(*_model)[3][1] = pos.y;
	(*_model)[3][2] = pos.z;
}
