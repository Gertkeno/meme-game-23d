#include <Wavefront.hpp>
#include <Texture.hpp>
#include <Shader.hpp>
#include <FontGL.hpp>
#include <ModelList.hpp>
#include <MusicTrack.hpp>
#include <GL/gl.h>

#ifdef _GERT_DEBUG
#include <iostream>
#define DE_OUT( x ) std::cout << "[AssetOpener]  " << x << std::endl
#endif //_GERT_DEBUG

gert::Wavefront* gModels;
gert::Texture* gTextures;
gert::Shader* gShaders;
gert::Font* gFont;
gert::MusicTrack * gMusic;

//typedef unsigned char GLubyte;
bool open_models( void )// opens all assets
{
	///shaders
	gShaders = new gert::Shader[ shading::TOTAL ];
	#ifdef _GERT_DEBUG
	DE_OUT( "Allocated Shaders" );
	#endif //_GERT_DEBUG
	gShaders[ shading::OBJ_LIGHTING ].open_shader_file( "GLSL/basicObjLighting.vs", "GLSL/basicObjLighting.fs" );
	gShaders[ shading::FLAT_UI ].open_shader_file( "GLSL/UI.vs", "GLSL/UI.fs" );
	#ifdef _GERT_DEBUG
	DE_OUT( "Loaded shaders" );
	#endif //_GERT_DEBUG
	gShaders[ shading::FLAT_UI ].use_shader();

	///models
	gModels = new gert::Wavefront[ model::TOTAL ];
	#ifdef _GERT_DEBUG
	DE_OUT( "Allocated models" );
	#endif //_GERT_DEBUG
	gModels[ model::PLAYER_SHIP  ].file_load( "assets/models/ship1.obj" );
	gModels[ model::DEBUG_SPHERE ].file_load( "assets/models/sphere.obj" );
	{
		#define RAW_LOAD( x, i ) extern float x##_vertices; extern unsigned int x##_len; gModels[ i ].raw_verts_load( & x##_vertices, x##_len )
		RAW_LOAD( plane, model::PLANE_2D );
		RAW_LOAD( model_hoop, model::RING );
		RAW_LOAD( M, model::LETTER_M );
		RAW_LOAD( E, model::LETTER_E );
		RAW_LOAD( G, model::LETTER_G );
		RAW_LOAD( A, model::LETTER_A );
		RAW_LOAD( Two, model::LETTER_TWO );
		RAW_LOAD( Three, model::LETTER_THREE );
		RAW_LOAD( D, model::LETTER_D );
	}
	gModels[ model::BASIC_SHOT ].file_load( "assets/models/basicShot.obj" );
	gModels[ model::CYLINDER   ].file_load( "assets/models/tube.obj" );
	gModels[ model::CUBE       ].file_load( "assets/models/cube.obj" );
	gModels[ model::MAKIN_SHIP ].file_load( "assets/models/makin_ship.obj" );

	gModels[ model::COMMANDER   ].file_load( "assets/models/profile/commander.obj" );
	gModels[ model::PLAYER_PROF ].file_load( "assets/models/profile/playerCharacter.obj" );
	gModels[ model::AI_PROF     ].file_load( "assets/models/profile/AI.obj" );
	gModels[ model::MAKIN       ].file_load( "assets/models/profile/makin.obj" );
	gModels[ model::TOASTER     ].file_load( "assets/models/profile/Toaster.obj" );
	gModels[ model::ELLIS       ].file_load( "assets/models/profile/Toad_Profile.obj" );
	gModels[ model::DEACON      ].file_load( "assets/models/profile/Deacon_prof.obj" );
	gModels[ model::SUPER_SAGE  ].file_load( "assets/models/profile/super_sage.obj" );
	gModels[ model::SUNBUNNI    ].file_load( "assets/models/profile/sunbunni.obj" );
	gModels[ model::CLOWN       ].file_load( "assets/models/profile/clown.obj" );
	gModels[ model::BREAD       ].file_load( "assets/models/Bread.obj" );
	gModels[ model::APPLE       ].file_load( "assets/models/Apple.obj" );
	gModels[ model::STAR        ].file_load( "assets/models/star.obj" );
	#ifdef _GERT_DEBUG
	DE_OUT( "Loaded models" );
	#endif //_GERT_DEBUG
	///textures
	gTextures = new gert::Texture[ texture::TOTAL ];
	#ifdef _GERT_DEBUG
	DE_OUT( "Allocated Textures" );
	#endif //_GERT_DEBUG
	gTextures[ texture::PLAYER_SHIP      ].file_load( "assets/texture/ship1_UV.jpg" );
	gTextures[ texture::BLOOD_TEX        ].file_load( "assets/texture/bloody.png" );
	gTextures[ texture::CRACK_TEX        ].file_load( "assets/texture/cracks.png" );
	gTextures[ texture::ELECTRIC         ].file_load( "assets/texture/electrics.png", 2, 2 );
	gTextures[ texture::RETICLE          ].file_load( "assets/texture/retic.png" );
	gTextures[ texture::HEALTH_BAR_BASIC ].file_load( "assets/texture/HealthBarBasic.jpg" );
	gTextures[ texture::THRUSTER_EFFECT  ].file_load( "assets/texture/thrustingEffect.png", 3, 1 );
	gTextures[ texture::MAKIN_SHIP       ].file_load( "assets/texture/makin_ship.jpg" );
	gTextures[ texture::PURP_NEBULA      ].file_load( "assets/texture/nebula_sphere.jpg" );
	gTextures[ texture::APPLE            ].file_load( "assets/texture/Apple.jpg" );
	gTextures[ texture::POWER_ICONS      ].file_load( "assets/texture/power_icons.png", 3, 3 );
	gTextures[ texture::CREDITS_JPG      ].file_load( "assets/credits.jpg" );

	gTextures[ texture::AI_PROF     ].file_load( "assets/texture/profile/AI_UV_layout.jpg" );
	gTextures[ texture::PLAYER_PROF ].file_load( "assets/texture/profile/playerprof.png" );
	gTextures[ texture::COMMANDER   ].file_load( "assets/texture/profile/commander_layout.jpg" );
	gTextures[ texture::TOASTER     ].file_load( "assets/texture/profile/Toaster_UV.jpg" );
	gTextures[ texture::ELLIS       ].file_load( "assets/texture/profile/Toad_Profile.jpg" );
	gTextures[ texture::DEACON      ].file_load( "assets/texture/profile/Deacon_prof.jpg" );
	gTextures[ texture::SUPER_SAGE  ].file_load( "assets/texture/profile/super_sage.jpg" );
	gTextures[ texture::SUNBUNNI    ].file_load( "assets/texture/profile/sunbunni_UV.jpg" );
	gTextures[ texture::MAKIN       ].file_load( "assets/texture/profile/makin.jpg" );
	gTextures[ texture::FONZI       ].file_load( "assets/texture/profile/fonzi.jpg" );
	gTextures[ texture::BOBO        ].file_load( "assets/texture/profile/bobo_uv.jpg" );

	{///baked textures
		const unsigned char foo[] = "\377\0\0\177\377\0\377\177\377\0\377\177\377\0\0\177";
		gTextures[ texture::RED_TEST ].load( foo, 4, 2, 2 );
		const unsigned char bar[] = "\377\377\0\177\0\377\377\177\0\377\377\177\377\377\0\177";
		gTextures[ texture::YELLOW_TEST ].load( bar, 4, 2, 2 );
	}
	{
		extern const unsigned char cards_jpg;
		extern const unsigned int cards_jpg_len;
		gTextures[ texture::OPENING_CARDS ].RW_load( &cards_jpg, cards_jpg_len );
		gTextures[ texture::OPENING_CARDS ].set_sheet_data( 3, 4 );

		extern const unsigned char felix0_jpg;
		extern const unsigned int felix0_jpg_len;
		gTextures[ texture::DOG_0 ].RW_load( &felix0_jpg, felix0_jpg_len );

		extern const unsigned char felix1_jpg;
		extern const unsigned int felix1_jpg_len;
		gTextures[ texture::DOG_1 ].RW_load( &felix1_jpg, felix1_jpg_len );

		extern const unsigned char SPEED_jpg;
		extern const unsigned int SPEED_jpg_len;
		gTextures[ texture::GOGGLES ].RW_load( &SPEED_jpg, SPEED_jpg_len );
	}
	#ifdef _GERT_DEBUG
	DE_OUT( "Loaded textures" );
	#endif //_GERT_DEBUG

	extern unsigned char Loop_Targets_s24_649_aiff;
	extern unsigned int Loop_Targets_s24_649_aiff_len;
	extern unsigned char unfounded_revenge_556263_aiff;
	extern unsigned int unfounded_revenge_556263_aiff_len;
	extern unsigned char Toaster_aiff;
	extern unsigned int Toaster_aiff_len;
	extern unsigned char come_depart_aiff;
	extern unsigned int come_depart_aiff_len;
	extern unsigned char YOSHI_JUNGLE_FEVER_aiff;
	extern unsigned int YOSHI_JUNGLE_FEVER_aiff_len;
	extern unsigned char this_way_that_aiff;
	extern unsigned int this_way_that_aiff_len;
	extern unsigned char Uh_Oh_raw;
	extern unsigned int Uh_Oh_raw_len;
	extern unsigned char dinoland_raw;
	extern unsigned int dinoland_raw_len;
	extern unsigned char mm2wood_aiff;
	extern unsigned int mm2wood_aiff_len;
	extern unsigned char zero_two_aiff;
	extern unsigned int zero_two_aiff_len;
	//order matters, *2 for stereo
	gMusic = new gert::MusicTrack[music::TOTAL]{
		{&Loop_Targets_s24_649_aiff, Loop_Targets_s24_649_aiff_len, 545789*2},
		{&unfounded_revenge_556263_aiff, unfounded_revenge_556263_aiff_len, 279116*2},
		{&Toaster_aiff, Toaster_aiff_len, 271822*2},
		{&come_depart_aiff, come_depart_aiff_len, 259774*2},
		{&YOSHI_JUNGLE_FEVER_aiff, YOSHI_JUNGLE_FEVER_aiff_len, 166820*2},
		{&this_way_that_aiff, this_way_that_aiff_len, 86407*2},
		{&Uh_Oh_raw, Uh_Oh_raw_len, 6788*2},
		{&dinoland_raw, dinoland_raw_len, 113410*2},
		{&mm2wood_aiff, mm2wood_aiff_len, 176886*2},
		{&zero_two_aiff, zero_two_aiff_len, 733603*2},
	};

	gFont = new gert::Font;
	gFont->load_font( "assets/Quicksand-Bold.otf", 20 );

	return true;
}

bool close_assets( void )
{
	delete gFont;
	delete[] gTextures;
	delete[] gModels;
	delete[] gShaders;
	delete[] gMusic;
	return true;
}
