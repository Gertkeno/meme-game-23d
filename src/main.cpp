#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <cstring>

#include <GameManager.hpp>
#include <ControllerList.hpp>
#include <WindowLoader.hpp>
#include <FrameTime.hpp>
#include <random>
#include <ctime>

#include <MusicTrack.hpp>
#ifdef _GERT_DEBUG
#include <iostream>
#define DE_OUT( x ) std::cout << "[main]  " << x << std::endl
#endif //_GERT_DEBUG

SDL_Window* gWindow;
const char * gSaveString;
gert::Controller gControls;

bool open_models( void );
bool close_assets( void );

int main( int argc, char* argv[] )
{
	///SDL INITS
	if( SDL_Init( SDL_INIT_EVERYTHING ) > 0 )
	{
		#ifdef _GERT_DEBUG
		DE_OUT( "SDL couldn't init\n" << SDL_GetError() );
		#endif //_GERT_DEBUG
		return 1;
	}
	SDL_StopTextInput();
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );

	{
		static constexpr int flags{ IMG_INIT_JPG | IMG_INIT_PNG };
		if( ( IMG_Init( flags )&flags ) != flags )
		{
			#ifdef _GERT_DEBUG
			DE_OUT( "SDL_image couldn't init\n" << IMG_GetError() );
			#endif //_GERT_DEBUG
			return 3;
		}
	}
	if( TTF_Init() > 0 )
	{
		#ifdef _GERT_DEBUG
		DE_OUT( "SDL_ttf couldn't init\n" << TTF_GetError() );
		#endif //_GERT_DEBUG
		return 2;
	}

	gert::MusicTrack::init();

	{
		window_Load::Settings foo{ window_Load::open_settings() };
		gSaveString = "save.dat";
		for( auto i = 1; i < argc; i++ )
		{
			if( argv[i][0] != '-' ) continue;
			switch( argv[i][1] )
			{
				case 'o':
					if( argc <= i+2 ) break;
					foo.sw = strtol( argv[i+1], nullptr, 10 );
					foo.sh = strtol( argv[i+2], nullptr, 10 );
					break;
				case 'f':
					if( argv[i][2] == 't' )
						foo.flag |= SDL_WINDOW_FULLSCREEN_DESKTOP;
					else if( argv[i][2] == 'f' )
						foo.flag &= ~SDL_WINDOW_FULLSCREEN_DESKTOP;
					break;
				case 'b':
					if( argv[i][2] == 't' )
					{
						foo.flag |= SDL_WINDOW_BORDERLESS;
						foo.flag &= ~SDL_WINDOW_RESIZABLE;
					}
					else if( argv[i][2] == 'f' )
					{
						foo.flag &= ~SDL_WINDOW_BORDERLESS;
						foo.flag |= SDL_WINDOW_RESIZABLE;
					}
					break;
				case 's':
					if( argc <= i+1 ) break;
					gSaveString = argv[i+1];
					break;
			}//switch( argv[i][1] )
		}
		gWindow = SDL_CreateWindow( "Super MemeGame 23D Downhill Jam &knuckles [Turbo] Gold Edition, Featuring Dante from the Devil May Cry Series!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, foo.sw, foo.sh, foo.flag );

		//set window icon
		extern unsigned char Sans_face_png;
		extern unsigned int Sans_face_png_len;
		SDL_Surface *ico{ IMG_Load_RW( SDL_RWFromMem( &Sans_face_png, Sans_face_png_len ), 1 ) };
		if( ico != nullptr )
		{
			SDL_SetWindowIcon( gWindow, ico );
			SDL_FreeSurface( ico );
		}
		#ifdef _GERT_DEBUG
		else DE_OUT( "Couldn't load icon" );
		#endif //_GERT_DEBUG
	}
	const SDL_GLContext wcontext{ SDL_GL_CreateContext( gWindow ) };

	///OpenGL INITS
	glewExperimental = GL_TRUE;
	if( glewInit() > 0 )
	{
		#ifdef _GERT_DEBUG
		DE_OUT( "GLEW couldn't init\n" << glewGetErrorString( glGetError() ) );
		#endif //_GERT_DEBUG
		return 3;
	}

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	{
		int w, h;
		SDL_GetWindowSize( gWindow, &w, &h );
		glViewport( 0, 0, w, h );
	}

	if( SDL_GL_SetSwapInterval( -1 ) != 0 ) //vsync
	{
		#ifdef _GERT_DEBUG
		DE_OUT( "Late swap not supported." );
		#endif //_GERT_DEBUG
		SDL_GL_SetSwapInterval( 1 );
	}

	///Assets INIT
	open_models();
	#ifdef _GERT_DEBUG
	DE_OUT( "Loaded assets" );
	#endif //_GERT_DEBUG

	srand( time( nullptr ) );
	GameManager::init();
	#ifdef _GERT_DEBUG
	DE_OUT( "Initialized Game Manager" );
	#endif //_GERT_DEBUG

	if( not gControls.load_file( "controls.ini" ) )
	{
		GameManager::change_game_state( GameManager::gmsOPENING_CARDS );
		//Loading default controls
		#ifdef _GERT_DEBUG
		DE_OUT( "Loading default controls" );
		#endif //_GERT_DEBUG
		static constexpr gert::cunit dat[]{
			{control_t::CONFIRM, {gert::Input::type_t::KEYBOARD, SDLK_z}},
			{control_t::SPECIAL, {gert::Input::type_t::KEYBOARD, SDLK_LSHIFT}},
			{control_t::UP, {gert::Input::type_t::KEYBOARD, SDLK_UP}},
			{control_t::DOWN, {gert::Input::type_t::KEYBOARD, SDLK_DOWN}},
			{control_t::LEFT, {gert::Input::type_t::KEYBOARD, SDLK_LEFT}},
			{control_t::RIGHT, {gert::Input::type_t::KEYBOARD, SDLK_RIGHT}},
			{control_t::PAUSE, {gert::Input::type_t::KEYBOARD, SDLK_F1}},
			{control_t::BOOST, {gert::Input::type_t::KEYBOARD, SDLK_x}},
			{control_t::BRAKE, {gert::Input::type_t::KEYBOARD, SDLK_c}},
			{control_t::STAY, {gert::Input::type_t::KEYBOARD, SDLK_SPACE}},
		};
		gControls.load_raw( dat, control_t::TOTAL );
	}

	constexpr float MAX_FPS{ 1000.0f/90.0f };
	#ifdef _GERT_DEBUG
	DE_OUT( "Starting game loop at tick " << SDL_GetTicks() );
	#endif //_GERT_DEBUG

	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	while( GameManager::gameState != GameManager::gmsQUITTING )
	{
		FrameTime::update();

		GameManager::update();
		GameManager::draw();

		if( FrameTime::since_update() < MAX_FPS )
		{
			SDL_Delay( MAX_FPS - FrameTime::since_update() );
		}
	}

	///deinit
	window_Load::save_settings( gWindow );
	gControls.save_file( "controls.ini" );
	#ifdef _GERT_DEBUG
	DE_OUT( "Exiting all" );
	#endif //_GERT_DEBUG
	close_assets();
	gert::MusicTrack::destroy();
	GameManager::destroy();
	SDL_GL_DeleteContext( wcontext );
	SDL_DestroyWindow( gWindow );
	SDL_Quit();
	IMG_Quit();
	TTF_Quit();
	return 0;
}
