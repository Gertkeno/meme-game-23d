#include <ui/ButtonFunctions.hpp>

#include <FontGL.hpp>
#include <Wavefront.hpp>
#include <Shader.hpp>

#include <ModelList.hpp>
#include <Button.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <gert_GLmath.hpp>
#include <SDL2/SDL.h>
#include <GameManager.hpp>
#include <TitleScreen.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif//_GERT_DEBUG
extern SDL_Window* gWindow;

namespace button_function
{

void basic_draw( gert::Button* b )
{
	Shader::Active_Shader->set_uniform( "selected", float(b->get_inset()) );
	gFont->set_active( b->get_text() );
	gModels[ model::PLANE_2D ].draw( b->get_box(), texture::USE_LAST, color::WHITE );
}

void resume_game( void )
{
	GameManager::help_button_func( GameManager::bfhRESUME );
}

void quit_game( void )
{
	GameManager::change_game_state( GameManager::gmsQUITTING );
}

void scene_test( void )
{
	GameManager::help_button_func( GameManager::bfhSCENE_TEST );
}

void card_test( void )
{
	GameManager::help_button_func( GameManager::bfhCARD_TEST );
}

void controller_config( void )
{
	GameManager::change_game_state( GameManager::gmsCONTROLLER_CONFIG );
}

void new_game( void )
{
	GameManager::help_button_func( GameManager::bfhNEW_GAME );
}

void load_game( void )
{
	GameManager::load_game_file();
}

void toggle_fullscreen( void )
{
	const unsigned int flags{ SDL_GetWindowFlags( gWindow ) };
	if( flags & SDL_WINDOW_FULLSCREEN_DESKTOP )
	{
		SDL_SetWindowFullscreen( gWindow, 0 );
		SDL_SetWindowSize( gWindow, 800, 600 );
		SDL_SetWindowPosition( gWindow, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED );
		return;
	}
	SDL_SetWindowFullscreen( gWindow, SDL_WINDOW_FULLSCREEN_DESKTOP );
}

void credit_start( void )
{
	GameManager::help_button_func( GameManager::bfhCREDIT_START );
}
}
