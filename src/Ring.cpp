#include <Ring.hpp>

#include <GameManager.hpp>
#include <ModelList.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Collision.hpp>
#include <FrameTime.hpp>
#include <Wavefront.hpp>
#include <DrawList.hpp>

static constexpr float COLLECT_ANIMATION{ 0.4721f };
static constexpr float RADIUS{ 7.3f };

Ring::Ring()
{
	_collect = false;
	_sinceCollect = COLLECT_ANIMATION;
}

Ring::~Ring()
{
}

void Ring::update()
{
	if( _collect )
	{
		_sinceCollect += FrameTime::get_mod();
		const float z{ _position.z };
		_position = GameManager::get_player_pos();
		_position.z = z;
		return;
	}
	_position.z -= 5.0f * FrameTime::get_mod();
	const glm::vec3 ppos{ GameManager::get_player_pos() };
	static constexpr float TRIGGER_RADIUS{ RADIUS };
	if( collision::distance( ppos, _position ) < TRIGGER_RADIUS )
	{
		_collect = true;
	}
	else if( ppos.z > _position.z + TRIGGER_RADIUS )
	{
		_sinceCollect = COLLECT_ANIMATION;
	}
}

void Ring::draw()
{
	if( _sinceCollect >= COLLECT_ANIMATION )
		return;
	glm::mat4 place = glm::translate( glm::mat4(), _position );
	float ctR{ RADIUS };
	float rot{ 0.0f };
	if( _collect )
	{
		ctR = pow( (COLLECT_ANIMATION - _sinceCollect)/COLLECT_ANIMATION, 2 ) * RADIUS;
		rot = _sinceCollect/COLLECT_ANIMATION * 2*M_PI;
	}
	place = glm::scale( place, glm::vec3( ctR ) );
	place = glm::rotate( place, rot, glm::vec3( 0.0f, 0.0f, 1.0f ) );
	DrawList::add( {place, model::RING, texture::YELLOW_TEST, color::WHITE} );
}

bool Ring::is_collected()
{
	return _collect;
}

bool Ring::is_dead()
{
	return _sinceCollect >= COLLECT_ANIMATION;
}

void Ring::start( const glm::vec3& pos )
{
	_collect = false;
	_sinceCollect = 0.0f;
	_position = pos;
}
