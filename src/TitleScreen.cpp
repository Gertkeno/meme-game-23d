#include "TitleScreen.hpp"

#include <Button.hpp>
#include <ui/ButtonFunctions.hpp>
#include <FontGL.hpp>
#include <Shader.hpp>
#include <Wavefront.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ModelList.hpp>
#include <FrameTime.hpp>
#include <ControllerList.hpp>
#include <string>
#include <Version.h>

extern gert::Font* gFont;
extern SDL_Window* gWindow;
#ifdef _GERT_DEBUG
#include <iostream>
#define DE_OUT( x ) std::cout << "[TitleScreen]  " << x << std::endl
#endif //_GERT_DEBUG

namespace TitleScreen
{
	namespace
	{
		gert::Button* _myButtons;
		glm::vec2 _logoPos;
		float _logoAnimate;
		constexpr float LOGO_HEIGHT{ 0.3f };
		constexpr float LOGO_WIDTH{ 0.5f };
	}

	void init()
	{
		//ctor
		_myButtons = new gert::Button[ TOTAL ];

		static constexpr auto dd{ button_function::basic_draw };
		_myButtons[ RESUME            ].set( dd, button_function::resume_game );
		_myButtons[ QUIT              ].set( dd, button_function::quit_game );
#ifdef _GERT_DEBUG
		_myButtons[ SCENE_TEST        ].set( dd, button_function::scene_test );
#endif //_GERT_DEBUG
		_myButtons[ CARD_TEST         ].set( dd, button_function::card_test );
		_myButtons[ CONTROLLER_CONFIG ].set( dd, button_function::controller_config );
		_myButtons[ NEW_GAME          ].set( dd, button_function::new_game );
		_myButtons[ T_FULLSCREEN      ].set( dd, button_function::toggle_fullscreen );
		_myButtons[ LOAD_GAME         ].set( dd, button_function::load_game );
		_myButtons[ CREDITS           ].set( dd, button_function::credit_start );

		_myButtons[ RESUME            ].set_text( "RESUME" );
		_myButtons[ QUIT              ].set_text( "QUIT" );
#ifdef _GERT_DEBUG
		_myButtons[ SCENE_TEST        ].set_text( "Scene Test" );
#endif //_GERT_DEBUG
		_myButtons[ CARD_TEST         ].set_text( "Opening Cards" );
		_myButtons[ CONTROLLER_CONFIG ].set_text( "Rebind Controls" );
		_myButtons[ NEW_GAME          ].set_text( "NEW GAME" );
		_myButtons[ T_FULLSCREEN      ].set_text( "Toggle Fullscreen" );
		_myButtons[ LOAD_GAME         ].set_text( "LOAD GAME" );
		_myButtons[ CREDITS           ].set_text( "Credits" );

		//set sizes
		static constexpr float DESIRE_SIZE{ 0.035f };
		_logoPos = { 0.0f, 1.0f - LOGO_HEIGHT };
		for( byte i = 0; i < TOTAL; ++i )
		{
			const float ratio{ DESIRE_SIZE / gFont->ratio( _myButtons[ i ].get_text().c_str() ) };
			const glm::vec2 size{ ratio, DESIRE_SIZE };
			const glm::vec2 pos{ 0, size.y*2*-i + 0.5f };

			_myButtons[ i ].set_box( pos, size );
		}
	}

	void destroy()
	{
		//dtor
		delete[] _myButtons;
	}

	void draw( void )
	{
		//title drawing
		static constexpr model::type_t TITLE_MODEL_INDEXES[]{
			model::LETTER_M,
			model::LETTER_E,
			model::LETTER_M,
			model::LETTER_E,
			model::LETTER_G,
			model::LETTER_A,
			model::LETTER_M,
			model::LETTER_E,
			model::LETTER_TWO,
			model::LETTER_THREE,
			model::LETTER_D
		};
		static constexpr size_t TITLE_MODEL_INDEX_SIZE{ sizeof(TITLE_MODEL_INDEXES)/sizeof(TITLE_MODEL_INDEXES[0]) };
		static constexpr float TITLE_MODEL_ANGLE{ M_PI/2.0 };
		static constexpr float TITLE_MODEL_SIZE{ 0.16f };
		static constexpr float TITLE_MODEL_EVEN_TRANS{ TITLE_MODEL_INDEX_SIZE*TITLE_MODEL_SIZE/2.0f };

		static constexpr float TITLE_ROTATE_STURDY{  M_PI*2/30.0f };
		gert::Shader::Active_Shader->set_uniform( "shading", 0.4f );
		for( auto i = 0u; i < TITLE_MODEL_INDEX_SIZE; ++i )
		{
			glm::mat4 titleMat = glm::translate( glm::mat4(), { _logoPos.x + i*TITLE_MODEL_SIZE - TITLE_MODEL_EVEN_TRANS, _logoPos.y, 0.0f } );
			titleMat = glm::scale ( titleMat, glm::vec3( 0.06f ) );
			titleMat = glm::rotate( titleMat, TITLE_MODEL_ANGLE, { 1.0f, 0.0f, 0.0f } );
			const float rotAmount{ _logoAnimate*TITLE_ROTATE_STURDY*(1+i) };
			titleMat = glm::rotate( titleMat, rotAmount, { 0.0f, 0.0f, 1.0f } );
			/* translate, scale, rotate */
			gert::Shader::Active_Shader->set_uniform( "alphaX", _logoAnimate*4.4f - i );
			gModels[ TITLE_MODEL_INDEXES[i] ].draw( titleMat, i, color::WHITE );
		}

		gert::Shader::Active_Shader->set_uniform( "alphaX", 2.0f );
		gert::Shader::Active_Shader->set_uniform( "shading", 1.0f );
		//secret draw
		glm::mat4 secret = glm::translate( glm::mat4(), { _logoPos.x, _logoPos.y - 3.0f, 0.9f } );
		gModels[ model::PLANE_2D ].draw( secret, texture::DOG_0, color::WHITE );
		//button draw
		for( byte i = 0; i < TOTAL; ++i )
		{
			_myButtons[ i ].draw();
		}

		//version display
		gert::Shader::Active_Shader->set_uniform( "selected", 0.0f );
		static constexpr float V_SIZE{ 0.08f };
		static constexpr auto stamp{ VERSION };
		static const float V_HSIZE{ gFont->ratio(stamp) * V_SIZE };
		static const float V_YPOS{ -1.0f + V_HSIZE };
		gFont->draw( gModels + model::PLANE_2D, stamp, color::CYAN, { 0.0f, V_YPOS }, V_SIZE );
	}

	void update( void )
	{
		_logoAnimate += FrameTime::get_mod();
		for( byte i = 0; i < TOTAL; ++i )
		{
			_myButtons[ i ].update();
		}

		if( gControls.get_press( control_t::UP ) )
		{
			byte bindex = TOTAL-1;
			for( byte i = 1; i < TOTAL; ++i )
			{
				if( !_myButtons[ i ].get_inset() ) continue;
				bindex = i-1;
				break;
			}
			_myButtons[ bindex ].force_inset();
		}
		else if( gControls.get_press( control_t::DOWN  ) )
		{
			byte bindex = 0;
			for( byte i = 0; i < TOTAL-1; ++i )
			{
				if( !_myButtons[ i ].get_inset() ) continue;
				bindex = i+1;
				break;
			}
			_myButtons[ bindex ].force_inset();
		}
		else if( gControls.get_press( control_t::CONFIRM ) or gControls.get_press( control_t::PAUSE ) )
		{
			for( byte i = 0u; i < TOTAL; ++i )
			{
				if( not _myButtons[ i ].get_inset() ) continue;
				_myButtons[ i ].force_inc();
			}
		}

		if( gControls[control_t::STAY] > 0.5f )
		{
			_logoPos.x += (-gControls[control_t::LEFT] + gControls[control_t::RIGHT]) * FrameTime::get_mod();
			_logoPos.y += (-gControls[control_t::DOWN] + gControls[control_t::UP]) * FrameTime::get_mod();
		}
	}

	void set_inset( options t )
	{
		_myButtons[t].force_inset();
	}

	void reset_animation()
	{
		_logoAnimate = -0.07f;
		_logoPos = { 0.0f, 1.0f - LOGO_HEIGHT };
	}
}
