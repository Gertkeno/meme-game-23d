#include <Scene.hpp>

#include <DialogueEvent.hpp>
#include <SDL2/SDL_rwops.h>
#include <Wavefront.hpp>
#include <ModelList.hpp>
#include <FrameTime.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Texture.hpp>
#include <Shader.hpp>
#include <iostream>
#include <cstring>
#include <vector>

namespace Scene
{
	namespace
	{
		constexpr float SKIP_MIN{ 0.32f };
		unsigned char _page;
		DialogueEvent* _talk{ nullptr };
		unsigned int _pageTotal;
		float _timer{ 0.0f };
		unsigned char _skipped;
		end_t _ending;
		constexpr auto MAX_TEXTURE{9};
		unsigned char _lastBG;
		gert::Texture _backGrounds[MAX_TEXTURE];

		std::string get_word( const char * data, char delim )
		{
			std::string ret;
			while( *data != delim and *data != '\0' )
				ret += *data++;
			return ret;
		}
	}

	void draw()
	{
		if( _page > _pageTotal ) return;
		gShaders[ shading::FLAT_UI ].set_uniform( "shading", 1.0f );
		gert::Shader::Active_Shader->set_uniform( "alphaX", 2.0f );
		if( get_skipping() )
		{
			gModels[ model::PLANE_2D ].draw( glm::mat4(), texture::GOGGLES, color::WHITE );
			return;
		}

		//Background image
		auto & bg{ _backGrounds[_talk[_page].bgIndex] };
		if( _talk[_page].bgIndex < MAX_TEXTURE and bg.has_data() )
		{
			const glm::mat4 modFoo{ glm::translate( glm::mat4(), { 0.0f, 0.0f, 0.5f } ) };
			bg.set_active();
			gModels[ model::PLANE_2D ].draw( modFoo, texture::USE_LAST, color::WHITE );
		}
		_talk[_page].draw( _timer );
	}

	bool get_skipping()
	{
		_timer += FrameTime::get_pure();
		if( _timer > SKIP_MIN*2 )
			_skipped = 0u;
		return _skipped > 3;
	}

	end_t next()
	{
		_page++;
		if( _timer < SKIP_MIN )
			_skipped++;
		else
			_skipped = 0u;

		_timer = 0.0f;
		if( _page < _pageTotal )
			return CONTINUE;
		else
			return _ending;
	}

	bool open_file( const char* filename )
	{
		std::cerr << "[Scene][open_file]  Starting file read of " << filename << std::endl;
		_ending = HOOPS_START;
		_timer = 0.0f;
		_page = 0u;
		_skipped = 0u;
		_lastBG = 0u;

		if( filename == nullptr ) return false;
		SDL_RWops* dfile = SDL_RWFromFile( filename, "r" );
		if( dfile == nullptr ) return false;
		if( _talk != nullptr )
			delete[] _talk;
		const size_t fullSize{ SDL_RWsize( dfile ) };
		char fullFile[ fullSize ];
		SDL_RWread( dfile, fullFile, sizeof(char), fullSize );
		SDL_RWclose( dfile );

		int activeComp{ 0 };
		unsigned int index{ 0u };

		//count {} blocks
		_pageTotal = 0;
		while( index < fullSize and fullFile[index] != '\0' )
		{
			if( fullFile[index++] == '{' )
				++_pageTotal;
		}
		_talk = new DialogueEvent[_pageTotal];
		index = 0;

		//fill _talk pages
		bool inCommand{ false };
		bool mtset{ false };//copies model/texture if other isn't set (m2, t2) == (t2)
		while( index < fullSize and fullFile[ index ] != '\0' )
		{
			if( fullFile[index] == '#' )
			{
				while( fullFile[index] != '\n' and fullFile[index] != '\0' ) index++;
			}
			if( !inCommand )
			{
				//scene data
				if( fullFile[ index ] == '!' )//ending function
				{
					switch( fullFile[ ++index ] )
					{
						case 'T': _ending = TITLE_SCREEN; break;
						case 'R': _ending = REPEAT_BOSS; break;
						case 'N': _ending = HOOPS_START; break;
						case 'P': _ending = PLAY_GAME; break;
						case 'E': _ending = MEME_PATH; break;
						case 'D': _ending = MOD_PATH; break;
						case 'C': _ending = CREDITS; break;
						default: break;
					}
					++index;
					continue;
				}

				if( fullFile[ index ] == '{' )
				{
					inCommand = true;
				}
				else if( fullFile[ index ] == '<' )//background image
				{
					std::string fn{ get_word( fullFile+(++index), '>' ) };
					_backGrounds[_lastBG++].file_load( fn.c_str() );
					index += fn.size();
				}
				else
				{
					++index;
					continue;
				}
			}

			bool commanded{ true };
			if( fullFile[ index ] == 'm' )//set model index
			{
				_talk[ activeComp ].modelN = strtol( &fullFile[ index+1 ], nullptr, 10 );
				if( !mtset )
				{
					mtset = true;
					_talk[ activeComp ].textureN = _talk[ activeComp ].modelN;
				}
			}
			else if( fullFile[ index ] == 't' )//set texture index
			{
				_talk[ activeComp ].textureN = strtol( &fullFile[ index+1 ], nullptr, 10 );
				if( !mtset )
				{
					mtset = true;
					_talk[ activeComp ].modelN = _talk[ activeComp ].textureN;
				}
			}
			else if( fullFile[ index ] == 'b' )
			{
				_talk[ activeComp ].bgIndex = strtol( fullFile+index+1, nullptr, 10 );
				//std::cout << "got bg index of " << static_cast<int>( _talk[activeComp].bgIndex ) << std::endl;
			}
			else if( fullFile[ index ] == 'l' )//display profile on left
				_talk[ activeComp ].rightSide = false;
			else if( fullFile[ index ] == 'r' )//display profile on right
				_talk[ activeComp ].rightSide = true;
			else if( fullFile[ index ] == '"' )//dialogue text
			{
				index++;
				unsigned int textTotal{ 0 };
				while( fullFile[ index + textTotal ] != '"' ) textTotal++;

				char foo[ textTotal+1 ];
				memcpy( foo, &fullFile[index], textTotal+1 );
				foo[ textTotal ] = '\0';
				_talk[ activeComp ].set_text( foo );

				index+=textTotal+1;
			}
			else
			{
				commanded = false;
			}
			if( commanded )
			{
				while( fullFile[index] != ',' and fullFile[index] != '}' and fullFile[index] != '\0' ) index++;

				if( fullFile[ index ] == '}' )
				{
					inCommand = false;
					mtset = false;
					++activeComp;
				}
			}
			index++;
		}//while end
		return true;
	}
}
