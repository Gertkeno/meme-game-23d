#include <HitCircle.hpp>

#include <Collision.hpp>
#include <FrameTime.hpp>

HitCircle::HitCircle()
{
	//ctor
	area = new Sphere;
	source = nullptr;
	_lifeTime = 999.9f;
	maxTime = 0.0f;
}

HitCircle::~HitCircle()
{
	//dtor
	delete area;
}

bool HitCircle::get_active( void )
{
	return _lifeTime <= maxTime and source != nullptr;
}

void HitCircle::update( void )
{
	_lifeTime += FrameTime::get_mod();
}

void HitCircle::start( float damage, const MovingActor* from, const Sphere& pos )
{
	_lifeTime = 0.0f;
	maxTime = 0.0f;
	this->damage = damage;
	this->source = from;
	*(this->area) = pos;
	confirmHit = nullptr;
}

void HitCircle::kill( void )
{
	_lifeTime = maxTime+1.0f;
}
