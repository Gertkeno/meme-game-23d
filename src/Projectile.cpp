#include <Projectile.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GameManager.hpp>
#include <HitCircle.hpp>
#include <ModelList.hpp>
#include <Wavefront.hpp>
#include <DrawList.hpp>
#include <Collision.hpp>
#include <gert_GLmath.hpp>
#include <FrameTime.hpp>

Projectile::Projectile()
{
	//ctor
	_delta = new glm::vec3;
	_lifeTime = 99.9f;
	maxTime = 0;
	_damage = 0;
	_destroy = false;
	_scale = 1;
	useModel = model::BASIC_SHOT;
	useTex = texture::PLAYER_SHIP;

	_angle = new glm::vec3;
	confirmHit = nullptr;
}

Projectile::~Projectile()
{
	//dtor
	delete _delta;
	delete _angle;
}

void Projectile::start( const glm::vec3& targetPos, const glm::vec3& startPos, float speed, float life, float damage, MovingActor* source, float scale )
{
	this->source = source;
	_damage = damage;
	*_delta = gert::normal_points( startPos, targetPos );
	*_angle = gert::get_angles( startPos, targetPos );

	_scale = scale;
	*_delta = *_delta * speed;

	_lifeTime = 0;
	maxTime = life;
	useModel = model::BASIC_SHOT;
	useTex = texture::PLAYER_SHIP;
	*_model = glm::translate( glm::mat4(), startPos );

	_destroy = false;
	confirmHit = nullptr;
}

bool Projectile::get_active( void )
{
	return _lifeTime <= maxTime;
}

void Projectile::set_scale( float scale )
{
	this->_scale = scale;
}

void Projectile::update( void )
{
	_lifeTime += FrameTime::get_mod();
	if( _destroy )
	{
		_lifeTime = maxTime + 1;
		if( nullptr != confirmHit )
		{
			*confirmHit = true;
			confirmHit = nullptr;
		}
		return;
	}

	if( !get_active() ) return;
	HitCircle * const foo{ GameManager::get_hit_circle() };
	if( nullptr != foo )
	{
		foo->start( _damage, source, { get_vec_point(), _scale*0.75f } );
		foo->confirmHit = &_destroy;
	}

	//maybe divide by size when translating matrix
	*_model = glm::translate( *_model, *_delta );
}

bool Projectile::get_hurt( float damage, const HurtCircle* from )
{
	_lifeTime += damage;
	return false;
}

void Projectile::draw( void )
{
	if( !get_active() ) return;
	glm::mat4 copyModel = *_model;
	copyModel = glm::rotate( copyModel, _angle->y, glm::vec3( 0, 1, 0 ) );
	copyModel = glm::rotate( copyModel, _angle->z, glm::vec3( 0, 0, 1 ) );
	copyModel = glm::scale( copyModel, glm::vec3( _scale, _scale, _scale ) );
	DrawList::add( {copyModel, static_cast<model::type_t>(useModel), static_cast<texture::type_t>(useTex), color::WHITE} );
}
