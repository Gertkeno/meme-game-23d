#include <DialogueEvent.hpp>

#include <Shader.hpp>
#include <cstring>
#include <Wavefront.hpp>
#include <FontGL.hpp>
#include <SDL2/SDL_shape.h>
#include <ModelList.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif

static constexpr float TIME_TO_READ{ 0.013f };//Time per character

const std::string* DialogueEvent::player_name{ nullptr };
static unsigned char FESH_MARKS{0};

DialogueEvent::DialogueEvent()
{
	//ctor
	soundClip = nullptr;
	modelN = 0;
	textureN = 0;
	rightSide = false;
	static constexpr float RAND_NORM{1.0f/100.0f};
	_rotateRandom = (rand()%700)*RAND_NORM;
	bgIndex = 0;
}

DialogueEvent::~DialogueEvent()
{
	//dtor
}

void DialogueEvent::set_text( const char* foo )
{
	if( foo == nullptr or player_name == nullptr ) return;
	//fesh pince secret meme
	static const std::string FESH_PINCE = "Pince";
	if( *player_name == FESH_PINCE )
	{
		_myText = "And you find Geoffrey";
		_myText.append( ++FESH_MARKS, '!' );
		return;
	}
	_myText.clear();
	unsigned int b = 0, e = 0;
	while( foo[ e ] != '\0' )
	{
		if( foo[ e++ ] != '$' ) continue;
		_myText.append( foo, b, (e-1) - b );
		b = e;
		_myText.append( *player_name );
	}
	_myText.append( foo, b, e - b );

	if( std::rand()%0x64 == 0 )
		_myText.append( " shoutouts to simpleflips." );
}

const char* DialogueEvent::get_text( void )
{
	return _myText.c_str();
}

void DialogueEvent::draw( const float& rot )
{
	//variable setup, string size and profile position
	static constexpr float MODEL_SIZE{ 0.2f };
	static constexpr float REMAINING_MODEL_SIZE{ 1.0f - MODEL_SIZE };
	glm::mat4 modFoo;

	//text black backdrop
	glm::mat4 backBlack = glm::translate( glm::mat4(), { 0.0f, REMAINING_MODEL_SIZE, 0.4f } );
	backBlack = glm::scale( backBlack, { 1.0f, MODEL_SIZE, 1.0f } );
	gModels[ model::PLANE_2D ].draw( backBlack, texture::PLAYER_SHIP, { 0.0f, 0.0f, 0.0f, 0.6f } );

	//set up head rotation
	const float sider{ rightSide ? 1.0f - MODEL_SIZE : -1.0f + MODEL_SIZE };
	modFoo = glm::translate( glm::mat4(), glm::vec3( sider, REMAINING_MODEL_SIZE, 0.0f ) );
	modFoo = glm::scale( modFoo, glm::vec3( MODEL_SIZE ) );
	modFoo = glm::rotate( modFoo, _rotateRandom + rot, { 0.0f, 1.0f, 0.0f } );

	//draw rotating head
	gert::Shader::Active_Shader->set_uniform( "alphaX", rot*1.2f );
	gert::Shader::Active_Shader->set_uniform( "shading", 0.0f );
	gModels[ modelN ].draw( modFoo, textureN, color::WHITE );

	//majority of this is terrible because my font code is terrible
	static constexpr float mh{ 0.043f };

	const float xSide{ rightSide ? -MODEL_SIZE : MODEL_SIZE };
	const float ratio{ gFont->ratio( _myText.c_str() ) };
	const float scalar{ ratio <= mh ? REMAINING_MODEL_SIZE : std::min( REMAINING_MODEL_SIZE, mh/ratio ) };
	/* gFont->draw( &gModels[ model::PLANE_2D ], subText, colMod, { b, 1.f - std::max( h, mh ) }, 1.0f - MODEL_SIZE, 0.f, mh ); */
	gFont->draw( &gModels[ model::PLANE_2D ], _myText, color::WHITE, {  xSide, 1.0f - mh }, scalar, 0.0f, mh );
}
