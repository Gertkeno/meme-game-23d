#include <DrawList.hpp>
#include <ModelList.hpp>
#include <Wavefront.hpp>
#include <Texture.hpp>
#include <Shader.hpp>
#include <algorithm>
#include <vector>

namespace DrawList
{
	Call::Call( glm::mat4 a,
			model::type_t m,
			texture::type_t t,
			glm::vec4 c,
			float p,
			int f,
			float o,
			int ot,
			float s,
			float n
			):
		model(a),
		index(m),
		texture(t),
		color(c),
		priority(p),
		frame(f),
		overa(o),
		overaTexture(ot),
		shading(s),
		negative(n)
	{}

	namespace
	{
		constexpr int MAX_LIST{ 300 };
		Call * _list[MAX_LIST];
		size_t _lastInsert{ 0u };
		size_t _currentSize{ 0u };
	}

	void add( const Call& t )
	{
		if( _lastInsert >= _currentSize )
		{
			_list[_currentSize++] = new Call{ t };
		}
		else
		{
			*(_list[_lastInsert]) = t;
		}

		++_lastInsert;
		if( _lastInsert > MAX_LIST )
			_lastInsert = 0;
	}

	void flush()
	{
		_lastInsert = 0u;
	}

	void _safe_sort()
	{
		for( auto g = 0u; g < _lastInsert; ++g )
		{
			for( auto i = 1u + g; i < _lastInsert; ++i )
			{
				auto & f { _list[i] };
				auto & p {_list[i-1]};
				if( f->priority < p->priority or
					f->model[3][2] > p->model[3][2] )
					std::swap (f, p);
			}
		}
	}

	void draw_all()
	{
		gert::Shader::Active_Shader->set_uniform( "alphaX", 2.0f );
		_safe_sort();
		for( auto i = 0u; i < _lastInsert; ++i )
		{
			gTextures[ _list[i]->overaTexture ].set_active( 1 );
			gert::Shader::Active_Shader->set_uniform( "overa", _list[i]->overa );
			gert::Shader::Active_Shader->set_uniform( "shading", _list[i]->shading );
			gert::Shader::Active_Shader->set_uniform( "negativeColor", _list[i]->negative );
			gModels[ _list[i]->index ].draw( _list[i]->model, _list[i]->texture, _list[i]->color, _list[i]->frame );
		}
	}

	void destroy_calls()
	{
		for( auto i = 0u; i < _currentSize; ++i )
		{
			delete _list[i];
		}
		_currentSize = 0u;
	}
}
