#include <MusicTrack.hpp>
#include <iostream>
#include <cstring>

using namespace gert;
namespace
{
	//SDL_MixAudioFormat will be useful
	void loop_mix (void * udata, unsigned char * stream, int length)
	{
		MusicTrack * pos {*static_cast<MusicTrack**>( udata )};
		if (pos == nullptr)
			return;

		if (pos->currentPosition + length > pos->trackEnd)
		{
			const auto leftOver{ pos->trackEnd - pos->currentPosition };

			std::memcpy (stream, pos->trackStart+pos->currentPosition, leftOver);
			std::memcpy (stream+leftOver, pos->trackStart+pos->loopBegin, length-leftOver);
			pos->currentPosition = pos->loopBegin + (length-leftOver);
			//std::cout << "[MusicTrack]  Hey guess what, that was the loop point. did you notice? ;)\n";
			if (pos->on_loop != nullptr)
				pos->on_loop();
			return;
		}
		std::memcpy (stream, pos->trackStart+pos->currentPosition, length);
		pos->currentPosition += length;
	}
}


MusicTrack::MusicTrack( const unsigned char * ts, const unsigned len ):
MusicTrack (ts, len, 0)
{}

MusicTrack::MusicTrack (const unsigned char * ts, const unsigned len, unsigned b)
: trackStart {ts}
, loopBegin {b}
, trackEnd {len}
, currentPosition {0u}
, on_loop {nullptr}
{
	on_loop = nullptr;
}

//static
int MusicTrack::_audioDevice;
MusicTrack * MusicTrack::_working{nullptr};
SDL_AudioSpec MusicTrack::_spec;

void MusicTrack::init()
{
	_spec.freq = 22050;
	_spec.format = AUDIO_U8;
	_spec.channels = 2;
	_spec.samples = 4096;
	_spec.userdata = &_working;
	_spec.callback = loop_mix;
	_audioDevice = SDL_OpenAudioDevice (nullptr, false, &_spec, nullptr, 0);
	if (_audioDevice == 0)
		std::cerr << "[MusicTrack][init] Error getting audio device\n " << SDL_GetError() << std::endl;
}

void MusicTrack::destroy()
{
	SDL_CloseAudioDevice (_audioDevice);
}

void MusicTrack::que_track (MusicTrack * f)
{
	SDL_LockAudioDevice (_audioDevice);
	_working = f;
	_working->currentPosition = 0u;
	SDL_UnlockAudioDevice (_audioDevice);
	SDL_PauseAudioDevice (_audioDevice, false);
}

void MusicTrack::pause_track (bool t)
{
	SDL_PauseAudioDevice (_audioDevice, t);
}
