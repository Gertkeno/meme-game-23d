#include <GameManager.hpp>

///Library includes
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_timer.h>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

///Game Data
#include <Shader.hpp>
#include <HurtCircle.hpp>
#include <Projectile.hpp>
#include <ModelList.hpp>
#include <HitCircle.hpp>
#include <Player.hpp>
#include <ControllerList.hpp>
#include <gert_GLCamera.hpp>
#include <FrameTime.hpp>
#include <HoopGame.hpp>
#include <MusicTrack.hpp>

///UI
#include <TitleScreen.hpp>
#include <Button.hpp>
#include <DialogueEvent.hpp>
#include <BonusSelect.hpp>
#include <Scene.hpp>

///Bosses specific
#include <boss/MakinBacon.hpp>
#include <boss/Deacon.hpp>
#include <boss/Toaster.hpp>
#include <boss/Ellis.hpp>
#include <boss/SuperSage.hpp>
#include <boss/Sunbunni.hpp>
#include <boss/Dad.hpp>
#include <boss/Clown.hpp>

///Drawing headers
#include <FontGL.hpp>
#include <Wavefront.hpp>
#include <ModelList.hpp>
#include <gert_GLmath.hpp>
#include <Texture.hpp>
#include <DrawList.hpp>
#ifdef _GERT_DEBUG
#include <Collision.hpp>
#include <iostream>
#endif // _GERT_DEBUG

extern const char * gSaveString;

namespace GameManager
{
	//declarations
	void _update_shader( bool calcProj = false );
	void _empty_start( void );
	void _boss_init( void );

	//public externd
	state_t gameState;
	namespace //private
	{
		constexpr float CONTROL_INFO_HEIGHT{ 0.04f };
		//config draw for ControlInfo
#ifdef _GERT_DEBUG
		//debug draw for ControlInfo
		void d_draw( int pos, float hi )
		{
			gert::Shader::Active_Shader->set_uniform( "shading", 1.0f );
			const std::string text{ gert::name_control( pos ) };
			const float h{ CONTROL_INFO_HEIGHT / gFont->ratio( text.c_str() ) };
			const glm::vec2 purePos( -1.0f + h, 1.0f - (pos+1)*CONTROL_INFO_HEIGHT*2 );
			const float invertHi{ 1.0f - hi };
			const glm::vec4 col{ 1.0f, invertHi, invertHi, 1.0f };//held to red color
			gFont->draw( &gModels[ model::PLANE_2D ], text, col, purePos, h );
		}
#endif //_GERT_DEBUG

		//constants
		constexpr byte gmMAX_HURT_CIRCS{ 55 };
		constexpr byte gmMAX_HIT_CIRCS{ 111 };
		constexpr byte gmMAX_PROJECTILE{ 123 };

		constexpr float gmCARD_TIME{ 3.8f };//time in seconds to display  per card
		constexpr byte gmCARD_MAX{ 12 };//total opening card count
		constexpr byte gmNAME_LIMIT{ 220 };

		constexpr float Z_FAR{ 280.0f };//1520.0f;
		constexpr auto gmDEFAULT_NAME{ "Mook" };
		constexpr float FADE_TIME_MAX{ 0.44f };

		constexpr float CAMERA_DRAG{ 0.020f };
		constexpr float CAMERA_STEP_BACK{ 12.0f };
		constexpr float CAMERA_STEP_DOWN{ -2.3f };

		constexpr float CREDIT_HEIGHT_SCALAR{ 1200/1000 };
		constexpr auto CREDIT_UP_MAX{ CREDIT_HEIGHT_SCALAR };
		constexpr auto CREDIT_TIME{ 90 };
		constexpr double CREDIT_UP_PER_SEC{ CREDIT_UP_MAX / (CREDIT_TIME/4) };

		//basic data
		SDL_Event* _event;
		glm::vec3* _lightDirection;
		glm::mat4* _view;
		glm::mat4* _proj;

		//damage system
		HurtCircle* _hurts;
		HitCircle* _hits;
		Projectile* _bullets;
		gert::Camera* _cam;

		///game data
		struct
		{
			state_t to;
			float timer;
		} _stateFader;

		bool _writeSave;
		std::string _pname( gmDEFAULT_NAME );
		Player* _playa;
		BossBase* _boss;
		float _cardTime;
		unsigned _score;
		enum pathGuide: byte
		{
			MAKIN,
			TOASTER_DEACON,
			ELLIS_SUPERSAGE,
			SUNBUNNI_DAD,
			BOBO_FONZI,
			P_MOD_RIGHT = 1 << 7,
		};
		constexpr auto MAX_BOSS_MEME{ BOBO_FONZI };
		constexpr auto MAX_BOSS_MOD{ MAX_BOSS_MEME & P_MOD_RIGHT  };
		pathGuide _pathTracker;
		int _modelBoss;
		double _creditPos;

		//konami code boss select
		int _konamiCodeState;
		bool _increment_konami( int key )
		{
			constexpr int keys[]{ SDLK_UP, SDLK_UP, SDLK_DOWN, SDLK_DOWN, SDLK_LEFT, SDLK_RIGHT, SDLK_LEFT, SDLK_RIGHT, SDLK_b, SDLK_a };
			_konamiCodeState = (key == keys[_konamiCodeState]) ? _konamiCodeState+1 : 0;

			if( _konamiCodeState > 9 )
			{
				_konamiCodeState = 0;
				return true;
			}
			return false;
		}

		void _camera_update()
		{
			//pretty gross
			const auto s{ get_player_pos() };
			const auto z{ s.z - CAMERA_STEP_BACK };
			const auto y{ s.y - CAMERA_STEP_DOWN };
			const glm::vec3 intendedSpot{ s.x, y, z };
			const glm::vec3 cameraDifference{ intendedSpot - _cam->position };
			_cam->position += cameraDifference*CAMERA_DRAG;
			_cam->front = _cam->position;
			_cam->front.z += 1.0f;
		}

		void _camera_reset()
		{
			_cam->position = get_player_pos();
			_cam->position.z -= CAMERA_STEP_BACK;
			_cam->position.y -= CAMERA_STEP_DOWN;
		}

		//hoop game start
		void _init_hoops()
		{
			clear_resources();
			HoopGame::start();
			_playa->start();
			_camera_reset();
			change_game_state( gmsHOOPS );
		}
	}

	void init( void )
	{
		_writeSave = false;
		_view = new glm::mat4;
		_proj = new glm::mat4;
		gert::Wavefront::set_proj_view( _proj, _view );
		_pathTracker = pathGuide(0);
		_konamiCodeState = 0;

		_event = new SDL_Event;
		_playa = new Player;
		_lightDirection = new glm::vec3;
		*_lightDirection = { 0, 1, -1 };

		_hurts = new HurtCircle[ gmMAX_HURT_CIRCS ];
		_hits = new HitCircle[ gmMAX_HIT_CIRCS ];
		_bullets = new Projectile[ gmMAX_PROJECTILE ];
		_score = 0u;

		_boss = nullptr;

		TitleScreen::init();
		DialogueEvent::player_name = &_pname;

		_pname = gmDEFAULT_NAME;
		_cam = new gert::Camera;
		_cam->above = { 0.0f, 1.0f, 0.0f };
		_cam->position = glm::vec3( 0.0f );

		_update_shader( true );
		change_game_state( gmsTITLE_SCREEN );
		gert::MusicTrack::que_track( gMusic+music::TARGETS );
	}

	void destroy( void )
	{
		if( _writeSave ) save_game();
		delete _event;
		delete _playa;
		delete _lightDirection;
		delete _view;
		delete _proj;

		if( _boss != nullptr ) delete _boss;

		delete[] _hurts;
		delete[] _hits;
		delete[] _bullets;

		delete _cam;
		TitleScreen::destroy();
		DrawList::destroy_calls();
	}

	void _collision_checks()
	{
		for( byte u = 0; u < gmMAX_HURT_CIRCS; u++ )
		{
			if( _hurts[ u ].target == nullptr )
				continue;

			for( byte i = 0; i < gmMAX_HIT_CIRCS; i++ )
			{
				if( not _hits[ i ].get_active() )
					continue;
				_hurts[ u ].check_collision( _hits + i );
			}
		}
	}

	void update( void )
	{
		///GLOBAL ACTIONS
		gControls.reset_press();
		bool nameReturn{false};
		while( SDL_PollEvent( _event ) )
		{
			switch( _event->type )//global events
			{
				case SDL_KEYDOWN:
					if( gmsTITLE_SCREEN == gameState and _increment_konami( _event->key.keysym.sym ) )
					{
#ifdef _GERT_DEBUG
						std::cout << "[GameManager] KONAMI CODE ENTER\n"; //change to boss select screen
#endif //_GERT_DEBUG
						change_game_state( gmsBOSS_SELECT );
					}
					switch( _event->key.keysym.sym )
					{
						case SDLK_q:
							if( (SDL_GetModState() & KMOD_CTRL) > 0 )
							{
								gameState = gmsQUITTING;
							}
							break;
						case SDLK_RETURN:
							nameReturn = true;
							break;
#ifdef _GERT_DEBUG
						case SDLK_F4:
							if( gameState != gmsMODEL_VIEW )
							{
								change_game_state( gmsMODEL_VIEW );
								break;
							}
							if( _modelBoss < model::TOTAL and _modelBoss < texture::TOTAL )
								++_modelBoss;
							else
								_modelBoss = 0;
							break;
						case SDLK_F10:
							BonusSelect::init();
							change_game_state( gmsBONUS_SELECT );
							break;
						case SDLK_HOME:
							glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
							break;
						case SDLK_END:
							glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
							break;
#endif //_GERT_DEBUG
					}
					break;
				case SDL_QUIT:
					gameState = gmsQUITTING;
					break;
				case SDL_WINDOWEVENT:
					switch( _event->window.event )
					{
						case SDL_WINDOWEVENT_RESIZED:
							_update_shader( true );
							break;
					}
					break;
				case SDL_JOYDEVICEADDED:
					gControls.joy_add( _event->jdevice.which );
					break;
				case SDL_JOYDEVICEREMOVED:
					gControls.joy_remove( _event->jdevice.which );
					break;
				case SDL_MOUSEMOTION:
					SDL_GetMouseState( &gert::Button::mx, &gert::Button::my );
					break;
				case SDL_MOUSEBUTTONDOWN:
					gert::Button::mb = _event->button.button;
					break;
				case SDL_MOUSEBUTTONUP:
					gert::Button::mb = 0xFF;
					break;
			}

			///polling for controls
			gControls.manage_inputs( _event );//have to poll this first
			switch( gameState )//gamestate events
			{
				case gmsNAME_ENTRY:
					{
						bool finishNameEntry{ false };
						if( _event->type == SDL_TEXTINPUT and _pname.length() < gmNAME_LIMIT ) _pname += _event->text.text;
						if( nameReturn or gControls(control_t::PAUSE) ) finishNameEntry = true;
						if( _event->type != SDL_KEYDOWN )break;
						if( _event->key.keysym.sym == SDLK_BACKSPACE )
						{
							if( _pname.length() <= 0 )break;
							_pname.pop_back();
							if( not (SDL_GetModState() & KMOD_CTRL) )break;
							while( _pname[ _pname.length()-1 ] != ' ' and _pname.length() > 0 ) _pname.pop_back();
						}
						else if( _event->key.keysym.sym == SDLK_RETURN ) finishNameEntry = true;

						if( not finishNameEntry ) break;
						SDL_StopTextInput();//post name entry
						if( _boss != nullptr )
						{
							delete _boss;
							_boss = nullptr;
						}
						_empty_start();
						Scene::open_file( "assets/story/intro.txt" );
						change_game_state( gmsCUT_SCENE );
					}
					break;
				case gmsCONTROLLER_CONFIG:
					if( gert::configure_controller( _event ) )
					{
						change_game_state( gmsTITLE_SCREEN );
					}
					break;
				case gmsOPENING_CARDS:
				case gmsMODEL_VIEW:
				case gmsCREDITS:
				case gmsBOSS_SELECT:
					if( _event->type == SDL_KEYDOWN and _event->key.keysym.sym == SDLK_ESCAPE )
					{
						change_game_state( gmsTITLE_SCREEN );
					}
					break;
				default:
					break;
			}
		}//SDL_PollEvent( _event )

		///UPDATING OBJECTS
		if( _stateFader.to != gmsNULL )
		{
			_stateFader.timer += FrameTime::get_mod();
			if( _stateFader.timer > FADE_TIME_MAX )
				change_game_state( _stateFader.to );
		}
		switch( gameState )
		{
			case gmsPLAYING:
				for( byte i = 0; i < gmMAX_HIT_CIRCS; i++ )
					_hits[ i ].update();
				for( byte i = 0; i < gmMAX_PROJECTILE; i++ )
					_bullets[ i ].update();

				_playa->update();

				if( _boss != nullptr )
					_boss->update();

				_camera_update();
				*_view = glm::lookAt( _cam->position, _cam->front, _cam->above );

				_collision_checks();

				if( _boss != nullptr )
				{
					if( not _boss->get_alive() )
					{
						//NOTE: boss die
						if( Scene::open_file( _boss->get_dialogue_file( true ) ) )
							change_game_state( gmsCUT_SCENE );
					}
					else if( not _playa->get_active() )
					{
						//NOTE: player die
						if( Scene::open_file( "assets/story/udie.txt" ) )
							change_game_state( gmsCUT_SCENE );
						else
							change_game_state( gmsTITLE_SCREEN );
						_boss_init();
					}
				}
				if( gControls( control_t::PAUSE ) )
					change_game_state( gmsTITLE_SCREEN );
				break;
			case gmsHOOPS:
				for( byte i = 0; i < gmMAX_HIT_CIRCS; i++ )
					_hits[ i ].update();
				for( byte i = 0; i < gmMAX_PROJECTILE; i++ )
					_bullets[ i ].update();

				if( HoopGame::update() and fade_game_state( gmsBONUS_SELECT ) )
				{
					BonusSelect::init();
				}
				_playa->update();

				_collision_checks();

				_score += HoopGame::frame_score();
				//cam update hoops
				_camera_update();
				*_view = glm::lookAt( _cam->position, _cam->front, _cam->above );
				if( gControls( control_t::PAUSE ) )
					change_game_state( gmsTITLE_SCREEN );
				break;
			case gmsBONUS_SELECT:
				if( BonusSelect::update() and fade_game_state( gmsCUT_SCENE ) )
				{
					_boss_init();
					Scene::open_file( _boss->get_dialogue_file( false ) );
				}
				{
					const auto f{ BonusSelect::get_select() };
					if( f.bonus != 0 and f.cost <= _score and (_playa->get_bonuses() & f.bonus) == 0 )
					{
						_playa->include_bonus( Player::bonuses(f.bonus) );
						_score -= f.cost;
					}
				}
				break;
			case gmsTITLE_SCREEN:
				TitleScreen::update();
				break;
			case gmsBOSS_SELECT:
				_cardTime += FrameTime::get_pure();
				if( gControls(control_t::UP) or gControls(control_t::DOWN) )
					_pathTracker = static_cast<pathGuide>(_pathTracker ^ P_MOD_RIGHT);
				else if( gControls(control_t::LEFT) and (_pathTracker &~ P_MOD_RIGHT) != 0 )
					_pathTracker = static_cast<pathGuide>(_pathTracker-1);
				else if( gControls(control_t::RIGHT) and (_pathTracker & MAX_BOSS_MEME) != MAX_BOSS_MEME )
					_pathTracker = static_cast<pathGuide>(_pathTracker+1);
				else if( gControls(control_t::CONFIRM) or gControls(control_t::PAUSE) )
				{
					_boss_init();
					Scene::open_file( _boss->get_dialogue_file( false ) );
					fade_game_state( gmsCUT_SCENE );
				}
				break;
			case gmsOPENING_CARDS:
				if( gControls(control_t::CONFIRM) or gControls(control_t::PAUSE) )
				{
					float leftOver{ _cardTime };
					while( leftOver > gmCARD_TIME ) leftOver -= gmCARD_TIME;
					_cardTime += gmCARD_TIME - leftOver;
				}
				_cardTime += FrameTime::get_pure();
				if( _cardTime >= gmCARD_MAX * gmCARD_TIME )
				{
					change_game_state( gmsTITLE_SCREEN );
				}
				break;
			case gmsCUT_SCENE:
				if( gControls(control_t::CONFIRM) or gControls(control_t::PAUSE) )
				{
					/*cut scene end*/
					const Scene::end_t ender{ Scene::next() };
					if( ender == Scene::CONTINUE ) break;
					switch( ender )
					{
						case Scene::CONTINUE: break;
						case Scene::CREDITS:
							if( _boss != nullptr )
							{
								delete _boss;
								_boss = nullptr;
							}
							change_game_state( gmsCREDITS );
							break;
						case Scene::PLAY_GAME:
							change_game_state( gmsPLAYING );
							break;
						case Scene::TITLE_SCREEN:
							change_game_state( gmsTITLE_SCREEN );
							break;
						case Scene::REPEAT_BOSS:
							_boss_init();
							Scene::open_file( _boss->get_dialogue_file( false ) );
							change_game_state( gmsCUT_SCENE );
							break;
						case Scene::MEME_PATH:
							_pathTracker = pathGuide( _pathTracker | P_MOD_RIGHT );
						if( false )
						{
						case Scene::MOD_PATH:
							_pathTracker = pathGuide( _pathTracker & ~P_MOD_RIGHT );
						}
						case Scene::HOOPS_START:
							_pathTracker = pathGuide(_pathTracker+1);
							_init_hoops();
							break;
					}
				}
				break;
			case gmsCREDITS:
				_creditPos += CREDIT_UP_PER_SEC * FrameTime::get_mod();
				if( _creditPos >= (CREDIT_UP_MAX+1) )
					change_game_state( gmsTITLE_SCREEN );
				break;
			case gmsNAME_ENTRY:
			case gmsQUITTING:
			case gmsCONTROLLER_CONFIG:
			case gmsMODEL_VIEW:
			case gmsNULL:
				break;
		}
	}

	void draw( void )
	{
		glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		DrawList::flush();

		///DRAWS
		gert::Shader::Active_Shader->set_uniform( "time", ( SDL_GetTicks() ) );

		if( gameState == gmsPLAYING or gameState == gmsHOOPS )
		{
			///PLAYERS AND WHAT NOT
			gert::Wavefront::set_proj_view( _proj, _view );
			gShaders[ shading::OBJ_LIGHTING ].use_shader();
			_update_shader();
			if( _stateFader.to != gmsNULL )
				gert::Shader::Active_Shader->set_uniform( "fade", static_cast<float>( 1.0f - _stateFader.timer*RECIPROCAL(FADE_TIME_MAX) ) );
			else
				gert::Shader::Active_Shader->set_uniform( "fade", 1.0f );

			gShaders[ shading::OBJ_LIGHTING ].set_uniform( "light", *_lightDirection );
			gShaders[ shading::OBJ_LIGHTING ].set_uniform( "negativeColor", 0.0f );///skybox
			gShaders[ shading::OBJ_LIGHTING ].set_uniform( "shading", 1.0f );///skybox
			{
				glm::mat4 foo{ glm::translate( glm::mat4(), _cam->position ) };
				foo = glm::rotate( foo, 4.6f, { 0.0f, 0.0f, 1.0f } );
				foo = glm::scale( foo, glm::vec3( Z_FAR - 0.03f ) );
				gModels[ model::DEBUG_SPHERE ].draw( foo, texture::PURP_NEBULA, color::WHITE );
			}
			gShaders[ shading::OBJ_LIGHTING ].set_uniform( "shading", 0.0f );

			for( byte i = 0; i < gmMAX_PROJECTILE; i++ )
			{
				_bullets[ i ].draw();
			}

			_playa->draw();
			if( gameState == gmsHOOPS )
			{
				HoopGame::draw();
			}
			else if( _boss != nullptr )
			{
				_boss->draw();
			}
			_playa->reticle_draw();

#ifdef _GERT_DEBUG
			for( byte i = 0; i < gmMAX_HIT_CIRCS; i++ )
			{
				if( not _hits[ i ].get_active() ) continue;
				DrawList::add( {_hits[i].area->ct_mat(), model::DEBUG_SPHERE, texture::RED_TEST, color::WHITE} );
			}

			for( byte i = 0; i < gmMAX_HURT_CIRCS; i++ )
			{
				if( _hurts[ i ].target == nullptr ) continue;
				DrawList::add( {_hurts[i].area->ct_mat(), model::DEBUG_SPHERE, texture::YELLOW_TEST, color::WHITE} );
			}
#endif // _GERT_DEBUG

			DrawList::draw_all();
			///HEALTH DISPLAY
			gert::Wavefront::set_proj_view( nullptr, nullptr );
			gShaders[ shading::FLAT_UI ].use_shader();
			_update_shader();
			gShaders[ shading::FLAT_UI ].set_uniform( "selected", 0.0f );
			gShaders[ shading::FLAT_UI ].set_uniform( "shading", 1.0f );
			static constexpr float HEIGHT{ 0.04f };

			static const auto draw_misc{[]( const float s )
			{
				const glm::vec4 col{ s, 1.0f, 1.0f - s, 1.0f };
				glm::mat4 barmat{ glm::translate( glm::mat4(), glm::vec3( 1 - HEIGHT, -0.5f, 0 ) ) };
				static constexpr float HALF_PI{ M_PI/2.0f };
				barmat = glm::rotate( barmat, HALF_PI, glm::vec3( 0, 0, 1 ) );
				barmat = glm::scale( barmat, glm::vec3( s/2, HEIGHT, 1 ) );
				gModels[ model::PLANE_2D ].draw( barmat, texture::HEALTH_BAR_BASIC, col );
			}};

			const bool ishoop{ gameState == gmsHOOPS };
			static constexpr float player_text_height{ 0.1f };
			if( ishoop )
			{
				draw_misc( HoopGame::get_dist_remain() );

				const std::string h{ std::to_string( _score ) };
				const float rat{ gFont->ratio( h.c_str() ) };
				const float scale{ 1.0f / rat*player_text_height };
				gFont->draw( &gModels[ model::PLANE_2D ], h, color::CYAN, glm::vec2( 0.0f, -1.0f+player_text_height ), scale );
			}
			else if( _boss != nullptr and _boss->get_alive() )
			{
				glm::mat4 barmat{ glm::translate( glm::mat4(), glm::vec3( 0, 1 - HEIGHT, 0 ) ) };
				barmat = glm::scale( barmat, glm::vec3( _boss->get_health(), HEIGHT, 1 ) );

				gModels[ model::PLANE_2D ].draw( barmat, texture::HEALTH_BAR_BASIC, color::RED );
				draw_misc( _boss->get_misc() );

				const std::string h( std::to_string(static_cast<int>(_playa->health)) );
				const float rat{ gFont->ratio( h.c_str() ) };
				const float scale{ 1.0f / rat*player_text_height };
				gFont->draw( &gModels[ model::PLANE_2D ], h, color::RED, glm::vec2( 0.0f, -1.0f+player_text_height ), scale );
			}

#ifdef _GERT_DEBUG
			gControls.debug_draw( &d_draw );
#endif //_GERT_DEBUG
		}
		else if( gameState == gmsCUT_SCENE )
		{
			Scene::draw();
		}
		else if( gameState == gmsBONUS_SELECT )
		{
			BonusSelect::draw( _score, _playa->get_bonuses() );
		}
		else if( gameState == gmsOPENING_CARDS )
		{
			const byte frame{ std::min<byte>( _cardTime/gmCARD_TIME, gmCARD_MAX-1 ) };

			const float ntimer{ ( _cardTime - (gmCARD_TIME*frame) )/gmCARD_TIME };
			const float sinTimer{ std::min<float>( std::sin(ntimer*M_PI)*3, 1.0f ) };
			const glm::vec4 col{ 1.0f, 1.0f, 1.0f, sinTimer };
			int w, h;
			SDL_GetWindowSize( gWindow, &w, &h );
			w *= 9; h *= 16;
			const float big{ std::max<float>( w, h ) };
			const glm::mat4 foo{ glm::scale( glm::mat4(), glm::vec3( h/big, w/big, 1.0f ) ) };
			gModels[ model::PLANE_2D ].draw( foo, texture::OPENING_CARDS, col, frame );
		}
		else if( gameState == gmsTITLE_SCREEN )
		{
			TitleScreen::draw();
		}
		else if( gameState == gmsCONTROLLER_CONFIG )
		{
			gert::draw_configurer( [](const char * n, const char * t)
			{
				const float nh{ CONTROL_INFO_HEIGHT / gFont->ratio( n ) };
				const float th{ CONTROL_INFO_HEIGHT / gFont->ratio( t ) };
				gFont->draw( &gModels[ model::PLANE_2D ], n, color::WHITE, { 0.0f, CONTROL_INFO_HEIGHT }, nh );
				gFont->draw( &gModels[ model::PLANE_2D ], t, color::YELLOW, { 0.0f, -CONTROL_INFO_HEIGHT }, th );
			});
			/* ControlInfo::controller_config_draw( &c_draw ); */
		}
		else if( gameState == gmsNAME_ENTRY )
		{
			if( _pname.compare( "Felix" ) == 0 )
			{
				gert::Shader::Active_Shader->set_uniform( "selected", 0.0f );
				glm::mat4 dogBG{ glm::translate( glm::mat4(), { 0.0f, 0.0f, 0.9f } ) };
				gModels[ model::PLANE_2D ].draw( dogBG, texture::DOG_1, color::WHITE );
			}
			const std::string temp{ _pname + '_' };
			static constexpr float NAME_H{ 0.058f };
			const float h{ NAME_H / gFont->ratio( temp.c_str() ) };
			gFont->draw( &gModels[ model::PLANE_2D ], temp, color::WHITE, glm::vec2( 0.f, 0.f ), h );

			static constexpr auto WHAT_DO{ "Enter a name:" };
			const float h_{ NAME_H / gFont->ratio( WHAT_DO ) };
			gFont->draw( &gModels[ model::PLANE_2D ], WHAT_DO, color::BLUE, glm::vec2( 0.f, NAME_H*3 ), h_ );
		}
		else if( gameState == gmsBOSS_SELECT )
		{
			gert::Shader::Active_Shader->set_uniform( "shading", 0.0f );
			if( _stateFader.to != gmsNULL )
				gert::Shader::Active_Shader->set_uniform( "alphaX", static_cast<float>( 1.0f - _stateFader.timer*RECIPROCAL(FADE_TIME_MAX) ) );
			else
				gert::Shader::Active_Shader->set_uniform( "alphaX", 1.2f );

			struct mt
			{
				model::type_t m;
				texture::type_t t;
			} match[]{
				{model::MAKIN, texture::MAKIN},
				{model::TOASTER, texture::TOASTER},
				{model::DEACON, texture::DEACON},
				{model::ELLIS, texture::ELLIS},
				{model::SUPER_SAGE, texture::SUPER_SAGE},
				{model::SUNBUNNI, texture::SUNBUNNI},
				{model::PLAYER_PROF, texture::PLAYER_PROF},
				{model::CLOWN, texture::BOBO},
				{model::CLOWN, texture::FONZI},
			};
			constexpr auto total{ sizeof(match)/sizeof(*match) };
			constexpr auto size{ 0.8f/total };
			const unsigned currentSelect( [](){
				const auto noMod{ _pathTracker &~ P_MOD_RIGHT };
				if( noMod == 0 )
					return 0;
				if( _pathTracker & P_MOD_RIGHT )
					return noMod * 2;
				else
					return noMod * 2 - 1;
			}() );
			//const unsigned currentSelect( _pathTracker &~ P_MOD_RIGHT );
			for( unsigned i = 0; i < total; ++i )
			{
				const float percent{ i/static_cast<float>(total) };
				const float y{ i == 0 ? 0.0f : size*2 * (i&1?1:-1) };
				glm::mat4 pos{ glm::translate( glm::mat4(), { percent*2 - 1.0f + size, y, 0.0f } ) };
				pos = glm::rotate( pos, _cardTime + percent, { 0.0f, 1.0f, 0.0f } );
				pos = glm::scale( pos, glm::vec3( size ) );

				gModels[match[i].m].draw( pos, match[i].t, i == currentSelect ? color::CYAN : color::WHITE );
			}
		}
		else if( gameState == gmsMODEL_VIEW )
		{
			static float rot{0.0f};
			rot += FrameTime::get_mod();

			glm::mat4 modFoo = glm::scale( glm::mat4(), glm::vec3( 0.9f ) );
			modFoo = glm::rotate( modFoo, rot, { 0.0f, 1.0f, 0.0f } );
			gert::Shader::Active_Shader->set_uniform( "alphaX", 2.0f );
			gert::Shader::Active_Shader->set_uniform( "shading", 0.0f );
			gModels[ _modelBoss ].draw( modFoo, _modelBoss, color::WHITE );
		}
		else if( gameState == gmsCREDITS )
		{
			int w, h;
			SDL_GetWindowSize( gWindow, &w, &h );
			const float width_reduction{ std::min( 1.0f, h/static_cast<float>(w) ) };

			glm::mat4 creditPage{ glm::translate( glm::mat4(), {0.0f, _creditPos, 0.0f} ) };
			creditPage = glm::scale( creditPage, { width_reduction, CREDIT_UP_MAX, 1.0f} );
			
			gModels[ model::PLANE_2D ].draw( creditPage, texture::CREDITS_JPG, color::WHITE );
		}

		SDL_GL_SwapWindow( gWindow );
	}

	HurtCircle* get_hurt_circle( void )
	{
		for( byte i = 0; i < gmMAX_HURT_CIRCS; i++ )
		{
			if( _hurts[ i ].target != nullptr ) continue;
			return _hurts + i;
		}
		return nullptr;
	}

	HitCircle* get_hit_circle( void )
	{
		for( byte i = 0; i < gmMAX_HIT_CIRCS; i++ )
		{
			if( _hits[ i ].get_active() ) continue;
			return _hits + i;
		}
		return nullptr;
	}

	Projectile* get_projectile( void )
	{
		for( byte i = 0; i < gmMAX_PROJECTILE; i++ )
		{
			if( _bullets[ i ].get_active( ) ) continue;
			return _bullets + i;
		}
		return nullptr;
	}

	void change_game_state( state_t ct )
	{
		gameState = ct;
		_stateFader.to = gmsNULL;
		_stateFader.timer = 0.0f;
		gControls.reset_press();
		switch( ct )
		{
			case gmsNAME_ENTRY:
				SDL_StartTextInput();
				_pname = gmDEFAULT_NAME;
				break;
			case gmsOPENING_CARDS:
				_cardTime = 0;
				break;
			case gmsTITLE_SCREEN:
				SDL_ShowCursor( SDL_ENABLE );
				TitleScreen::reset_animation();
				TitleScreen::set_inset( TitleScreen::RESUME );
				break;
			case gmsMODEL_VIEW:
				_modelBoss = 0;
				break;
			case gmsCREDITS:
				_creditPos = -(CREDIT_UP_MAX + 1);
				break;
			default:
				break;
		}

		switch( ct )///shaders set
		{
			case gmsHOOPS:
			case gmsPLAYING:
				SDL_ShowCursor( SDL_DISABLE );
				gert::Wavefront::set_proj_view( _proj, _view );
				gShaders[ shading::OBJ_LIGHTING ].use_shader();
				break;
			case gmsNAME_ENTRY:
			case gmsOPENING_CARDS:
			case gmsCONTROLLER_CONFIG:
			case gmsCUT_SCENE:
			case gmsTITLE_SCREEN:
			case gmsBOSS_SELECT:
			case gmsBONUS_SELECT:
			case gmsMODEL_VIEW:
			case gmsCREDITS:
				gert::Wavefront::set_proj_view( nullptr, nullptr );
				gShaders[ shading::FLAT_UI ].use_shader();
				break;
			case gmsQUITTING:
			case gmsNULL:
				break;
		}
		_update_shader();
	}

	bool fade_game_state( state_t ct )
	{
		if( _stateFader.to != gmsNULL ) return false;
		_stateFader.timer = 0.0f;
		_stateFader.to = ct;
		return true;
	}

	glm::vec3 get_player_pos( void )
	{
		return _playa->get_vec_point();
	}

	glm::vec3 get_boss_pos( void )
	{
		if( _boss == nullptr ) return glm::vec3(0.0f);
		return _boss->get_vec_point();
	}

	void _empty_start( void )
	{
		_writeSave = true;
		_score = 0u;
		_playa->reset();
		_pathTracker = pathGuide( 0 );
		_cam->position = _playa->get_vec_point();
		//_boss_init();
		//if( Scene::open_file( _boss->get_dialogue_file( false ) ) )
			//change_game_state( gmsCUT_SCENE );
	}

	void help_button_func( bfhGuide foo )
	{
		switch( foo )
		{
			case bfhNEW_GAME:
				change_game_state( gmsNAME_ENTRY );
				break;
			case bfhCARD_TEST:
				change_game_state( gmsOPENING_CARDS );
				break;
			case bfhRESUME:
				if( _boss == nullptr )
				{
					change_game_state( gmsNAME_ENTRY );
					break;
				}
				if( HoopGame::active() )
					change_game_state( gmsHOOPS );
				else
					change_game_state( gmsPLAYING );
				break;
			case bfhSCENE_TEST:
				Scene::open_file( "assets/story/test1.txt" );
				change_game_state( gmsCUT_SCENE );
				break;
			case bfhMAKIN_RAN:
				_pathTracker = pathGuide( _pathTracker | P_MOD_RIGHT );
				if( Scene::open_file( "assets/story/makin_evade.txt" ) )
				{
					change_game_state( gmsCUT_SCENE );
				}
				break;
			case bfhCREDIT_START:
				change_game_state( gmsCREDITS );
				break;
			case bfhTOTAL:
				break;
		}
	}

	void clear_resources( void )
	{
		for( auto i = 0; i < gmMAX_HIT_CIRCS; i++ )
		{
			_hits[ i ].kill();
		}
		for( auto i = 0; i < gmMAX_PROJECTILE; i++ )
		{
			_bullets[ i ].get_hurt( 999999999999.9f, nullptr );
		}
		for( auto i = 0; i < gmMAX_HURT_CIRCS; i++ )
		{
			_hurts[ i ].target = nullptr;
		}
	}

	#define SUPER_WRITE(x) SDL_RWwrite( sfile, & x, sizeof( x ), 1 )
	void save_game( void )
	{
		SDL_RWops* sfile{ SDL_RWFromFile( gSaveString, "w" ) };
		if( sfile == nullptr ) return;
		const size_t size{ _pname.length() };
		SUPER_WRITE( size );
		SDL_RWwrite( sfile, _pname.c_str(), sizeof( char ), size );
		SUPER_WRITE( _pathTracker );
		const Player::bonuses pbonus{ _playa->get_bonuses() };
		SUPER_WRITE( pbonus );
		SUPER_WRITE( _score );
		_writeSave = false;

		#ifdef _GERT_DEBUG
		std::cout << "[GameManager][save_game]  Save written to " << gSaveString << std::endl;
		#endif //_GERT_DEBUG
		SDL_RWclose( sfile );
	}

	#define SUPER_READ( x ) SDL_RWread( sfile, & x, sizeof( x ), 1 )
	void load_game_file( void )
	{
		_writeSave = true;
		clear_resources();
		SDL_RWops* sfile{ SDL_RWFromFile( gSaveString, "r" ) };
		_playa->reset();
		if( sfile == nullptr )
		{
			change_game_state( gmsNAME_ENTRY );
			return;
		}
		size_t nameSize;
		SUPER_READ( nameSize );
		{
			char name[nameSize+1];
			SDL_RWread( sfile, name, sizeof( char ), nameSize );
			name[nameSize] = '\0';
			_pname = name;
		}
		SUPER_READ( _pathTracker );
		Player::bonuses pbonus;
		SUPER_READ( pbonus );
		_playa->include_bonus( pbonus );
		SUPER_READ( _score );

		SDL_RWclose( sfile );
		_playa->start();
		_camera_reset();
		_cam->position = _playa->get_vec_point();
		_boss_init();
		Scene::open_file( _boss->get_dialogue_file( false ) );
		change_game_state( gmsCUT_SCENE );
	}

	void _boss_init( void )
	{
		clear_resources();
		const bool rightMod{ _pathTracker & P_MOD_RIGHT };
		#ifdef _GERT_DEBUG
		std::cout << "[GameManager][_boss_init]  starting boss path: ";
		if( rightMod )
			std::cout << "memes";
		else
			std::cout << "mods";
		std::cout << ", num: " << ( _pathTracker & ~P_MOD_RIGHT ) << std::endl;
		#endif //_GERT_DEBUG
		if( _boss != nullptr )
		{
			delete _boss;
			_boss = nullptr;
		}
		switch( (_pathTracker & ~P_MOD_RIGHT) )
		{
			case MAKIN:
				_boss = new boss::MakinBacon;
				break;
			case TOASTER_DEACON:
				if( rightMod )
					_boss = new boss::Deacon;
				else
					_boss = new boss::Toasterman;
				break;
			case ELLIS_SUPERSAGE:
				if( rightMod )
					_boss = new boss::SuperSage;
				else
					_boss = new boss::Ellis;
				break;
			case SUNBUNNI_DAD:
				if( rightMod )
					_boss = new boss::Dad;
				else
					_boss = new boss::Sunbunni;
				break;
			case BOBO_FONZI:
				_boss = new boss::Clown( rightMod );
				break;
			default:
				_boss = new boss::MakinBacon;
				break;
		}
		_playa->start();
		_camera_reset();
		_boss->start();
		const auto bossbgm{ _boss->get_music() };
		if( bossbgm < music::TOTAL )
			gert::MusicTrack::que_track( gMusic + bossbgm );
		else
			gert::MusicTrack::pause_track();
		_cam->position = _playa->get_vec_point();
		_writeSave = true;
		//NOTE: boss init, start inbetween bonus
	}

	void _update_shader( bool calcProj )
	{
		gert::Shader::Active_Shader->set_uniform( "alphaX", 2.0f );
		gert::Shader::Active_Shader->set_uniform( "time", ( SDL_GetTicks() ) );
		//gert::Shader::Active_Shader->set_uniform( "fade", 1.0f );
		gert::Shader::Active_Shader->set_uniform( "zFarFog", Z_FAR );
		gert::Wavefront::update_attribs();

		if( calcProj )
		{
			int w, h;
			SDL_GetWindowSize( gWindow, &w, &h );
			glViewport( 0, 0, w, h );
			gert::Button::screenWidth = w;
			gert::Button::screenHeight = h;
			*_proj = glm::perspective( glm::radians(50.0f), float(w) / float(h), 0.9f, Z_FAR );///last two are zNear and zFar clipping might need to increase far
		}
	}
}
