#include <BossBase.hpp>
#include <GameManager.hpp>

BossBase::BossBase()
{
	//ctor
	_attackProc = 0u;
	_firstProc = false;
}

BossBase::~BossBase()
{
	//dtor
}

const char* BossBase::get_dialogue_file( bool dying ) const
{
	return "assets/story/test2.txt";
}

void BossBase::start( void )
{
	glm::vec3 tempPos{ GameManager::get_player_pos() };
	tempPos.z += 50.0f;
	set_vec_point( tempPos );
	_attackProc = 0u;
	_firstProc = false;
	_attackTimer = 0.0f;
	_iTime = 99.9f;
}

bool BossBase::get_alive() const
{
	return _health > 0;
}
