#include <Target.hpp>

#include <GameManager.hpp>
#include <Collision.hpp>
#include <HurtCircle.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Wavefront.hpp>
#include <FrameTime.hpp>
#include <DrawList.hpp>

#include <iostream>

static constexpr float SIZE{ 3.2f };

Target::Target():
_hurt(nullptr)
{
	*_model = glm::scale( glm::mat4(), glm::vec3(SIZE) );
	hitConfirm = nullptr;
}

Target::~Target()
{
}

void Target::init()
{
	_hurt = GameManager::get_hurt_circle();
	_hurt->target = this;
}

void Target::start( const glm::vec3 & pos )
{
	set_vec_point( pos );

	if( _hurt == nullptr )
		return;

	*(_hurt->area) = Sphere( pos, SIZE );
}

void Target::draw() const
{
	DrawList::add( {*_model, model::STAR, texture::MAKIN, color::WHITE, -1.0f} );
}

bool Target::get_hurt( float d, const HurtCircle * f )
{
	if( _hurt != nullptr and f == _hurt and hitConfirm != nullptr )
	{
		*hitConfirm = true;
		return true;
	}
	return false;
}
