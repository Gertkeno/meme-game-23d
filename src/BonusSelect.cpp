#include <BonusSelect.hpp>
#include <ModelList.hpp>
#include <ControllerList.hpp>
#include <FontGL.hpp>
#include <Player.hpp>
#include <Wavefront.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif

namespace BonusSelect
{
	namespace
	{
		constexpr float HEIGHT{ 0.63f };
		constexpr int MARKET_COST_MAX{ 60 };
		constexpr int MARKET_COST_MIN{ 30 };
		constexpr int MARKET_COST_DELTA{ MARKET_COST_MAX - MARKET_COST_MIN };

		constexpr int MARKET_SIZE{3u};
		Card market[MARKET_SIZE];

		int cursor;
	}

	void init()
	{
		cursor = 0;
		for( auto i = 0u; i < MARKET_SIZE; ++i )
		{
			market[i].cost = std::rand()%MARKET_COST_DELTA + MARKET_COST_MIN;
			market[i].bonus = 1 << std::rand()%8;
		}
	}
	
	bool update()
	{
		if( gControls(control_t::SPECIAL) )
			return true;

		return false;
	}

	Card get_select()
	{
		if( gControls(control_t::LEFT) )
			--cursor;
		else if( gControls(control_t::RIGHT) )
			++cursor;

		if( cursor < 0 )
			cursor = MARKET_SIZE-1;
		else if( cursor >= MARKET_SIZE )
			cursor = 0;

		if( gControls(control_t::CONFIRM) )
			return market[cursor];

		return {0, 0xFF};
	}

	struct BonusDescription
	{
		std::string text;
		unsigned icon;
	};
	
	BonusDescription description_finder( unsigned char bonus )
	{
		static const std::string text[]{
			"Gain a sheild after some time",
			"Every third shot is big and hurts more",
			"Move even faster when boosting",
			"Getting hit shoots back",
			"While boosting shots go faster, hurt more",
			"Fire a ring of bullets (shoot+special)",
			"Laser (stay+special)",
			"Be invincible for a short time (boost+special)",
		};
		unsigned char tester{1};
		for( unsigned char i = 0u; i < 8; ++i )
		{
			if( tester == bonus )
				return {text[i], i};
			tester = tester << 1;
		}

		return {std::string{"ERROR"}, 9};
	}

	void draw( int playerScore, unsigned playerInv )
	{
		/*SHEILD     = 1 << 0, //after some time free hit
		BIG_SHOT     = 1 << 1, //third shot is big and double damages
		MORE_SPEED   = 1 << 2, //move even faster when boosting
		REVENGE      = 1 << 3, //getting hit shoots back
		EXPRESS_SHOT = 1 << 4, //shots fired while boosting go faster, do more damage
		RING_SHOT    = 1 << 5, //lots of shots; shoot+special
		LASER        = 1 << 6, //high damage laser; hold stay+special
		BARREL_ROLL  = 1 << 7, //invincible roll; boost+special*/

		static constexpr float offset{ 0.04f };
		static constexpr float width{ 1.0f/MARKET_SIZE };
		for( int i = 0; i < MARKET_SIZE; ++i )
		{
			const auto t{ description_finder( market[i].bonus ) };
			if( t.icon >= 9 )
				continue;

			const glm::vec3 position{ -1.0f + i*width*2 + width, 0.0f, 0.0f };
			glm::mat4 foo{ glm::translate( glm::mat4(), position ) };
			foo = glm::scale( foo, { width - offset, HEIGHT, 1.0f } );
			const float ca{ (market[i].bonus & playerInv) ? 0.48f : 1.0f };
			gModels[model::PLANE_2D].draw( foo, texture::POWER_ICONS, color::WHITE*ca, t.icon );
			if( cursor != i )
				continue;
			foo = glm::translate( foo, {0.0f,0.0f, -0.01f} );
			gModels[model::PLANE_2D].draw( foo, texture::POWER_ICONS, color::WHITE, 8 );
			const auto str{ t.text + ": " + std::to_string(market[i].cost) };
			const auto ratio{ gFont->ratio( str.c_str() ) };
			static constexpr float textHeight{0.04f};
			const auto scale{ 1.0f/ratio * textHeight };
			gFont->draw( &gModels[ model::PLANE_2D ], str, color::WHITE, { 0.0f, -1.0f+textHeight }, scale );
			{
				const auto money{ "Your money: " + std::to_string(playerScore) };
				const auto ratio{ gFont->ratio(money.c_str()) };
				const auto scale{ 1.0f/ratio * textHeight };
				gFont->draw( &gModels[ model::PLANE_2D ], money, color::CYAN, { 0.0f, -1.0f+textHeight*3 }, scale );
			}
		}
	}
}
