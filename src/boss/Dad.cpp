#include <boss/Dad.hpp>

#include <algorithm>
#include <FrameTime.hpp>
#include <HurtCircle.hpp>
#include <HitCircle.hpp>
#include <GameManager.hpp>
#include <DrawList.hpp>
#include <Wavefront.hpp>
#include <Projectile.hpp>
#include <Collision.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace boss;

namespace//const
{
	constexpr float MAX_HEALTH{ 400 };
	constexpr float SIZE{ 4.8f };

	constexpr float FULL_ORBIT_TIME{ 5.0f };
	constexpr float I_TIME_MAX{ 0.24f };

	constexpr float ORBITER_SIZE{ 3.0f };
	constexpr float LASER_SIZE{ ORBITER_SIZE };
	constexpr float ORBITER_DISTANCE_START{ 12.0f };
	constexpr float ATTACK_DELAY{ -1.1f };
	constexpr float EXPAND_MAX{ ORBITER_SIZE + 2.0f };
}

Dad::Dad():
_myHurt{nullptr}
{
	*_model = glm::scale( glm::mat4(), glm::vec3(SIZE) );
}

Dad::~Dad()
{
}

void Dad::update()
{
	const auto ft{ FrameTime::get_mod() };
	_iTime += ft;

	_orbiter.rotation += FrameTime::get_mod();
	if( _orbiter.rotation > FULL_ORBIT_TIME )
		_orbiter.rotation -= FULL_ORBIT_TIME;
	const auto rotCt{ _orbiter.rotation / FULL_ORBIT_TIME * TAU };
	_orbiter.point.x = std::cos( rotCt ) * _orbiter.distance;
	_orbiter.point.y = std::sin( rotCt ) * _orbiter.distance;

	if( _attack_expired() )
	{
		_attackProc = 0u;
		_attackTimer = ATTACK_DELAY;
		_firstProc = false;
		_lastAttack = random_fresh<attacks_t, attacks_t::TOTAL>(_lastAttack);
	}
	_attack_update();

	const float speed( Z_SPEED_DEFAULT/SIZE * FrameTime::get_mod() );
	*_model = glm::translate( *_model, {0.0f, 0.0f, speed} );
	if( _myHurt != nullptr )
		*_myHurt->area = get_vec_point();
}

void Dad::_attack_update()
{
	_attackTimer += FrameTime::get_mod();

	if( _attackTimer <= 0.0f )
		return;

	const float ctTime{ _attackTimer*(_get_attack_count(_lastAttack)/_get_attack_time(_lastAttack)) };
	if( ctTime > 1.0f + _attackProc )
	{
		switch( _lastAttack )
		{
			case attacks_t::JUST_SHOOTING:
				{
					Projectile * const foo{ GameManager::get_projectile() };
					if( foo == nullptr )
						break;
					glm::vec3 target{ GameManager::get_player_pos() };
					target.z += Z_SPEED_DEFAULT;
					foo->start( target, _exact_orbit(), 1.2f, 6.0f, 9.0f, this );
				}break;
			default: break;
		}
		++_attackProc;
	}

	switch( _lastAttack )
	{
		case attacks_t::EXPANDING_LASER:
			if( _attackProc > 0 )
			{
				HitCircle * const foo{ GameManager::get_hit_circle() };
				if( foo == nullptr )
					break;
				const float size{ _attackTimer / _get_attack_time(_lastAttack) * EXPAND_MAX };
				const auto p{ get_vec_point() };
				const glm::vec3 point{ p.x, p.y, GameManager::get_player_pos().z };
				foo->start( 12, this, { point, size } );
			}
			break;
		case attacks_t::LASER:
			if( _attackProc	> 0 )
			{
				HitCircle * const foo{ GameManager::get_hit_circle() };
				if( foo == nullptr )
					break;
				const auto & o{ _orbiter.point };
				const glm::vec3 point{ o.x, o.y, GameManager::get_player_pos().z };
				foo->start( 11, this, { point, LASER_SIZE } );
			}
			break;
		case attacks_t::MATCH_DIST:
			{
				const auto ppos{ GameManager::get_player_pos() };
				const auto bpos{ get_vec_point() };
				const glm::vec2 diff{ ppos.x - bpos.x, ppos.y - bpos.y };
				const float dist{ std::sqrt( diff.x*diff.x + diff.y*diff.y ) };
				_orbiter.distance = std::max( SIZE + 1.0f, dist );
			}break;
		default: break;
	}
}

float Dad::get_health() const
{
	return _health * RECIPROCAL(MAX_HEALTH);
}

unsigned Dad::get_music() const
{
	return music::DINO_LAND;
}

float Dad::get_misc() const
{
	return 0.0f;
}

const char * Dad::get_dialogue_file( bool d ) const
{
	if( d )
		return "assets/story/dad_die.txt";
	return "assets/story/dad_start.txt";
}

void Dad::start()
{
	BossBase::start();
	if( _myHurt == nullptr and (_myHurt = GameManager::get_hurt_circle()) == nullptr )
		return;

	_lastAttack = attacks_t::TOTAL;
	_orbiter.distance = ORBITER_DISTANCE_START;

	_myHurt->target = this;
	_myHurt->area->r = SIZE;
	_health = MAX_HEALTH;
}

void Dad::draw()
{
	const float colorScale{ std::min( _iTime/I_TIME_MAX, 1.0f ) };
	const glm::vec4 col{ 1.0f, colorScale, colorScale, 1.0f };
	DrawList::add( {*_model, model::PLAYER_PROF, texture::PLAYER_PROF, col, -1.0f, 0, 1.0f - get_health(), texture::BLOOD_TEX} );

	glm::mat4 orbitModel{ glm::translate( glm::mat4(), _exact_orbit() ) };
	orbitModel = glm::rotate( orbitModel, static_cast<float>(M_PI), { 1.0f, -1.0f, 0.0f } );
	orbitModel = glm::scale( orbitModel, glm::vec3( ORBITER_SIZE ) );
	DrawList::add( {orbitModel, model::PLAYER_SHIP, texture::PLAYER_SHIP, color::WHITE} );

	_attack_draws();
}

void Dad::_attack_draws() const
{
	if( _attackTimer <= 0.0f )
		return;
	switch( _lastAttack )
	{
		case attacks_t::LASER:
			{
				const float ls{ _attackProc > 0 ? LASER_SIZE : LASER_SIZE*0.8f };
				const float sh{ _attackProc > 0 ? 1.0f : 0.0f };
				glm::mat4 mod = glm::translate( glm::mat4(), _exact_orbit() );
				mod = glm::scale( mod, { ls, ls, 200 } );
				mod = glm::translate( mod, { 0.0f, 0.0f, -1.0f } );
				DrawList::add( {mod, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, sh} );
			}break;
		case attacks_t::EXPANDING_LASER:
			{
				const float ls{ _attackTimer/_get_attack_time(_lastAttack) * EXPAND_MAX };
				const float sh{ _attackProc > 0 ? 1.0f : 0.0f };
				glm::mat4 mod = glm::translate( glm::mat4(), get_vec_point() );
				mod = glm::scale( mod, { ls, ls, 200 } );
				mod = glm::translate( mod, { 0.0f, 0.0f, -1.0f } );
				DrawList::add( {mod, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, sh} );
			}break;
		default: break;
	}
}

bool Dad::get_hurt( float d, const HurtCircle * f )
{
	if( _myHurt != f )
		return false;

	_iTime = 0.0f;
	_health -= d;
	return true;
}

glm::vec3 Dad::_exact_orbit() const
{
	return { _orbiter.point.x, _orbiter.point.y, get_vec_point().z };
}

byte Dad::_get_attack_count( attacks_t t ) const
{
	switch( t )
	{
		case attacks_t::JUST_SHOOTING:
			return 18u;
		case attacks_t::EXPANDING_LASER:
		case attacks_t::LASER:
			return 3u;
		case attacks_t::MATCH_DIST:
		case attacks_t::TOTAL:
			return 1u;
	}
	return 1u;
}

float Dad::_get_attack_time( attacks_t t ) const
{
	switch( t )
	{
		case attacks_t::JUST_SHOOTING:
			return FULL_ORBIT_TIME;
		case attacks_t::LASER:
			return 3.4f;
		case attacks_t::EXPANDING_LASER:
			return 3.1f;
		case attacks_t::MATCH_DIST:
			return 1.2f;
		case attacks_t::TOTAL:
			return 1.0f;
	}
	return 1.0f;
}

bool Dad::_attack_expired() const
{
	return _attackProc > _get_attack_count( _lastAttack );
}
