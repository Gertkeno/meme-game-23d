#include "boss/MakinBacon.hpp"

#include <FrameTime.hpp>
///drawing includes
#include <glm/gtc/matrix_transform.hpp>
#include <Collision.hpp>
#include <GameManager.hpp>
#include <ModelList.hpp>
#include <Wavefront.hpp>
#include <DrawList.hpp>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

///attack includes
#include <Projectile.hpp>
#include <HurtCircle.hpp>
#include <HitCircle.hpp>
#include <gert_GLmath.hpp>

namespace
{
	constexpr float MAKIN_I_TIME{ 0.1f };
	constexpr float MAKIN_SCALE{ 5.0f };
	constexpr float MAKIN_MAX_HEALTH{ 244.0f };
	constexpr float MAKIN_ATTACK_DELAY{ -1.3f };

	constexpr float MAKIN_SHEILD_RANGE{ 40.0f };
	constexpr float MAKIN_LASER_RADIUS{ 3.6f };
	constexpr float MAKIN_CIRCLE_RADIUS{ 20.0f };

	constexpr float MAKIN_RUN_DIST{ 210.0f };
}

using namespace boss;
MakinBacon::MakinBacon()
{
	//ctor
	_lastAttack = SHOOTY;
	_myHurt = nullptr;
	_firstProc = false;
	*_model = glm::scale( *_model, glm::vec3( MAKIN_SCALE ) );//{ MAKIN_SCALE, MAKIN_SCALE, MAKIN_SCALE } );
}

MakinBacon::~MakinBacon()
{
	//dtor
	if( _myHurt != nullptr )
		_myHurt->target = nullptr;
}

void MakinBacon::update( void )
{
	_iTime += FrameTime::get_mod();
	_attackTimer += FrameTime::get_mod();

	if( _attackTimer > 0.0f )
	{
		const float ctTime{ _attackTimer*( _get_attack_count( _lastAttack )/_get_attack_time( _lastAttack ) ) };
		if( not _firstProc )
		{
			switch( _lastAttack )
			{
				case CIRCLE_SHOT:
					_targetPos = get_vec_point();
					_targetPos.x -= MAKIN_CIRCLE_RADIUS;
					break;
				default:
					break;
			}
			_firstProc = true;
		}

		switch( _lastAttack )///constantly attacking
		{
		case SHEILD:{
			HitCircle * const foo{ GameManager::get_hit_circle() };
			if( nullptr == foo ) break;
			const Sphere area{ get_vec_point(), _attackProc == 0 ? ctTime * MAKIN_SHEILD_RANGE : (1 - ctTime + _attackProc) * MAKIN_SHEILD_RANGE };
			foo->start(9, this, area);
			}break;
		case LASER:
			if( _attackProc == 0 )
			{
				_targetPos = GameManager::get_player_pos();
			}
			else if( _attackProc == 2 )
			{
				HitCircle* foo;
				IF_HIT_GET( foo )
				{
					Sphere area;
					const float dist{ glm::distance( GameManager::get_player_pos(), get_vec_point() ) };
					area = gert::normal_points( get_vec_point(), _targetPos ) * dist;
					area = area + get_vec_point();
					area.r = MAKIN_LASER_RADIUS;

					foo->start( 9, this, area );
				}
			}
			break;
		case CIRCLE_SHOT:{
			glm::vec3 npoint = get_vec_point();
			const auto acomp{ _attackTimer/_get_attack_time( _lastAttack ) * TAU };
			npoint.y = _targetPos.y + sin( acomp ) * MAKIN_CIRCLE_RADIUS;
			npoint.x = _targetPos.x + cos( acomp ) * MAKIN_CIRCLE_RADIUS;

			set_vec_point( npoint );
			} break;
		default:
			break;
		}

		if( ctTime >= (_attackProc + 1.0f) )///once per proc
		{
			switch( _lastAttack )
			{
			case SHOOTY:
				static constexpr byte SHOOTY_MAX{ 5 };
				static constexpr float SHOOTY_RADIUS{ 1.5f };
				for( uint8_t i = 0; i < SHOOTY_MAX; i++ )
				{
					glm::vec3 ctPos = GameManager::get_player_pos();
					ctPos.z += Z_SPEED_DEFAULT;
					ctPos.y += sin( i*RECIPROCAL(SHOOTY_MAX) * TAU ) * SHOOTY_RADIUS;
					ctPos.x += cos( i*RECIPROCAL(SHOOTY_MAX) * TAU ) * SHOOTY_RADIUS;
					Projectile* foo;
					IF_SHOT_GET( foo )
					{
						foo->start( ctPos, get_vec_point(), 0.8f, 7.0f, 6, this );
					}
				}break;
			case CIRCLE_SHOT:
				{
					Projectile* foo;
					IF_SHOT_GET( foo )
					{
						foo->start( GameManager::get_player_pos(), get_vec_point(), 1.2f, 3.0f, 4, this, 1.5f );
					}
				}break;
			default:
				break;
			}
			_attackProc += 1;
		}

		if( _attackProc >= _get_attack_count( _lastAttack ) )///get new attack
		{
			_attackTimer = MAKIN_ATTACK_DELAY;
			_attackProc = 0;
			_firstProc = false;

			_lastAttack = random_fresh<attacks, TOTAL>(_lastAttack);
		}
	}// (_attackTimer > 0.0f)

	float zspeed;
	switch( _lastAttack )
	{
	case CIRCLE_SHOT:
		zspeed = Z_SPEED_DEFAULT * 1.8f;
		break;
	case SHEILD:
		zspeed = Z_SPEED_DEFAULT * 0.2f;
		break;
	default:
		zspeed = Z_SPEED_DEFAULT;
		break;
	}
	zspeed *= FrameTime::get_mod();
	*_model = glm::translate( *_model, glm::vec3( 0.0f, 0.0f, zspeed / MAKIN_SCALE ) );
	///sync hurts
	if( _myHurt != nullptr )
	{
		constexpr float convertedSize{ MAKIN_SCALE*1.2f };
		*_myHurt->area = Sphere( get_vec_point(), convertedSize );
	}
	if( get_vec_point().z - MAKIN_RUN_DIST > GameManager::get_player_pos().z )
	{
		GameManager::help_button_func( GameManager::bfhMAKIN_RAN );
	}
}

void MakinBacon::draw( void )
{
	const float p{ std::min( _iTime/MAKIN_I_TIME, 1.0f ) };
	const glm::vec4 cmod{ 1.0f, p, p, 1.0f };

	glm::mat4 copyModel{ *_model };
	copyModel = glm::rotate( copyModel, static_cast<float>(M_PI), { 0.0f, 1.0f, 0.0f } );
	if( _attackTimer > 0 and _lastAttack == CIRCLE_SHOT )
	{
		copyModel = glm::rotate( copyModel, ( _attackTimer ), { 1, 0, 0 } );
	}
	DrawList::add( {copyModel, model::MAKIN_SHIP, texture::MAKIN_SHIP, cmod, -1.0f, 0, 1.0f - get_health(), texture::CRACK_TEX} );

	//Attacks draw
	if( _attackTimer <= 0 ) return;
	const float ctTime{ _attackTimer/( _get_attack_time( _lastAttack ) / _get_attack_count( _lastAttack ) ) };
	switch( _lastAttack )
	{
		case SHEILD:
			{
				Sphere foo;
				foo = get_vec_point();
				if( _attackProc == 0 )
					foo.r = ( ctTime )*MAKIN_SHEILD_RANGE;
				else
					foo.r = ( 1 - ( ctTime - _attackProc) )*MAKIN_SHEILD_RANGE;

				copyModel = foo.ct_mat();
				const auto f{ int( _attackTimer * 1000/150)%4 };
				DrawList::add( {copyModel, model::DEBUG_SPHERE, texture::ELECTRIC, color::WHITE, 0.0f, f} );
			}
			break;
		case LASER:
			if( not( _attackProc >= 2 and int(_attackTimer*1000)%300 > 150 ) )
			{
				glm::mat4 modspace;
				const float dist{ get_vec_point().z - _targetPos.z };
				modspace = glm::translate( modspace, get_vec_point() );
				const glm::vec3 rotTo{ gert::get_angles( _targetPos, get_vec_point() ) };//rotating
				modspace = glm::rotate( modspace, rotTo.y, { 1.0f, 0.0f, 0.0f } );
				modspace = glm::rotate( modspace, rotTo.x, { 0.0f, -1.0f, 0.0f } );
				modspace = glm::translate( modspace, { 0.0f, 0.0f, -dist } );//offset to start at makin
				float rads = 0.6666f;//radius modifier
				float shadin = 0.0f;
				if( _attackProc >= 2 )
				{
					rads = 1.0f;
					shadin = 1.0f;
				}
				rads *= MAKIN_LASER_RADIUS;//convert radius to laser radius
				modspace = glm::scale( modspace, { rads, rads, dist } );

				DrawList::add( {modspace, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, shadin} );
			}
			break;
		default:
			break;
	}
}

void MakinBacon::start( void )
{
	BossBase::start();
	_health = MAKIN_MAX_HEALTH;
	_attackTimer = 0;
	_iTime = MAKIN_I_TIME;
	_attackProc = 0;
	_lastAttack = TOTAL;
	_firstProc = false;

	if( _myHurt != nullptr ) return;
	IF_HURT_GET( _myHurt )
	{
		_myHurt->target = this;
	}
}

bool MakinBacon::get_hurt( float damage, const HurtCircle* from )
{
	if( _myHurt == from )
	{
		if( _lastAttack == SHEILD and _attackProc == 0 )
		{
			_health -= damage * 0.4f;
		}
		else
		{
			_health -= damage;
		}
		_iTime = 0;
		return true;
	}
	return false;
}

bool MakinBacon::get_alive( void ) const
{
	return _health > 0;
}

float MakinBacon::get_health( void ) const
{
	return _health * RECIPROCAL(MAKIN_MAX_HEALTH);
}

float MakinBacon::get_misc( void ) const
{
	return std::max(0.0f, (get_vec_point().z - GameManager::get_player_pos().z)*RECIPROCAL(MAKIN_RUN_DIST));
}

const char* MakinBacon::get_dialogue_file( bool dying ) const
{
	if( dying ) return "assets/story/makin_die.txt";
	return "assets/story/makin_start.txt";
}

float MakinBacon::_get_attack_time( attacks t ) const
{
	switch( t )
	{
		case LASER:
			return 3.3f;
		case SHOOTY:
			return 3;
		case SHEILD:
			return 5;
		case CIRCLE_SHOT:
			return M_PI*2;
		case TOTAL:
			return 2.6f;
	}
	return 1;
}

byte MakinBacon::_get_attack_count( attacks t ) const
{
	switch( t )
	{
		case LASER:
			return 3;
		case SHOOTY:
			return 5;
		case CIRCLE_SHOT:
			return 13;
		case SHEILD:
			return 2;
		case TOTAL:
			return 1;
	}
	return 1;
}

unsigned MakinBacon::get_music() const
{
	return music::UNFOUNDED_REVENGE;
}
