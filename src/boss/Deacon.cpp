#include <boss/Deacon.hpp>

#include <HurtCircle.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GameManager.hpp>
#include <cmath>
#include <Collision.hpp>
#include <Wavefront.hpp>
#include <gert_GLmath.hpp>
#include <ModelList.hpp>
#include <random>
#include <FrameTime.hpp>
#include <HitCircle.hpp>
#include <Projectile.hpp>
#include <DrawList.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

namespace
{
	constexpr char DEACON_GHOST_MAX{ 5 };
	constexpr float DEACON_SPIN_TIME{ 9.0f };
	constexpr float DEACON_HEALTH_MAX{ 118.0f };
	constexpr float DEACON_ATTACK_DOWN_TIME{ -0.78f };
	constexpr float DIST_MIN{ 9.8f };
	constexpr float DIST_MAX{ 23.0f };
	constexpr float DEACON_SIZE{ 4.0f };
	constexpr float DEACON_I_TIME{ 0.15f };
	constexpr char SEQ_COUNT{ 3 };
	constexpr float BALL_RAD{ 6.0f };
	constexpr float BALL_SPEED{ 9.0f };
	constexpr char BALL_MAX_HEALTH{ 4 };
}

using namespace boss;

Deacon::Deacon()
{
	//ctor
	_ghostOffsets = new glm::vec3[DEACON_GHOST_MAX];
	_myHurt = new HurtCircle*;
	*_myHurt = nullptr;
	_realGhostIndex = 0u;
	_ghostDistance = (DIST_MIN + DIST_MAX) / 2;
	_distanceIncrement = '\0';
	_ballAttack.health = 0;
}

Deacon::~Deacon()
{
	//dtor
	if( *_myHurt != nullptr ) (*_myHurt)->target = nullptr;
	delete _myHurt;
	if( _ballAttack.hurt != nullptr ) _ballAttack.hurt->target = nullptr;
	delete[] _ghostOffsets;
}

void Deacon::update()
{
	_ghostSpin += FrameTime::get_mod();
	_attackTimer += FrameTime::get_mod();
	_iTime += FrameTime::get_mod();

	if( _attackTimer > 0.0f )
	{
		if( !_firstProc )//first proc
		{
			switch( _lastAttack )
			{
				case EXPAND:
					static constexpr char dist{ 6 };
					if( _ghostDistance < DIST_MIN )
						_distanceIncrement = dist;
					else if( _ghostDistance > DIST_MAX )
						_distanceIncrement = -dist;
					else
					{
						_distanceIncrement = (rand()%2)*dist;
						if( _distanceIncrement == 0 )
						{
							_distanceIncrement = -dist;
						}
					}
					break;
				case CHANGE_GHOST:
					_realGhostIndex = rand()%DEACON_GHOST_MAX; //activate this when more moves are up
					break;
				case BIG_BALL:
					_ballAttack.health = BALL_MAX_HEALTH;
					_ballAttack.pos = _midPoint;
					break;
				default:
					break;
			}
			_firstProc = true;
		}
		const float ctTime{ _attackTimer*( _get_attack_count( _lastAttack ) / _get_attack_time( _lastAttack ) ) };
		if( ctTime >= ( _attackProc + 1.0f ) )
		{
			switch( _lastAttack )
			{
				case SEQUENCE_SHOT:
					{
						Projectile * const foo{ GameManager::get_projectile() };
						if( foo != nullptr )
						{
							const int index{ DEACON_GHOST_MAX - static_cast<int>(_attackProc*RECIPROCAL(SEQ_COUNT)) - 1 };
							glm::vec3 target{ GameManager::get_player_pos() };
							target.z += Z_SPEED_DEFAULT;
							foo->start( target, _ghostOffsets[index], 1.4f, 9.0f, 7, this, 1.2f );
						}
					}
					break;
				default:
					break;
			}
			_attackProc++;
		}

		//constantly
		switch( _lastAttack )
		{
			case EXPAND:
				_ghostDistance += (_distanceIncrement/_get_attack_time( _lastAttack )) * FrameTime::get_mod();
				break;
			case BIG_BALL:
				if( _ballAttack.health > 0 )
				{
					const glm::vec3 norm{ gert::normal_points( _ballAttack.pos, GameManager::get_player_pos() ) };
					_ballAttack.pos += norm * BALL_SPEED * static_cast<float>(FrameTime::get_mod());
					HitCircle * const foo{ GameManager::get_hit_circle() };
					if( nullptr == foo ) break;
					const Sphere spot{_ballAttack.pos, BALL_RAD};
					foo->start(21, this, spot);
					*(_ballAttack.hurt->area) = spot;
				}
				break;
			default:
				break;
		}

		//get new attack
		if( _attackProc >= _get_attack_count( _lastAttack ) )
		{
			_attackTimer = DEACON_ATTACK_DOWN_TIME;
			_attackProc = 0u;
			_firstProc = false;

			_lastAttack = random_fresh<attacks, TOTAL>(_lastAttack);
		}
	}

	//move
	_midPoint.z += Z_SPEED_DEFAULT * FrameTime::get_mod();
	//update offsets
	for( uint8_t i = 0; i < DEACON_GHOST_MAX; i++ )
	{
		const double rads{ TAU * ( (i/float(DEACON_GHOST_MAX)) + _ghostSpin/DEACON_SPIN_TIME ) };
		_ghostOffsets[ i ].x = cos( rads ) * _ghostDistance + _midPoint.x;
		_ghostOffsets[ i ].y = sin( rads ) * _ghostDistance + _midPoint.y;
		_ghostOffsets[ i ].z = _midPoint.z;
	}
	*(*_myHurt)->area = Sphere{ _ghostOffsets[ _realGhostIndex ], DEACON_SIZE };
	*_model = glm::translate( glm::mat4(), _ghostOffsets[ _realGhostIndex ] );
}

void Deacon::start()
{
	_realGhostIndex = rand()%DEACON_GHOST_MAX;
	BossBase::start();
	_ghostDistance = (DIST_MIN + DIST_MAX)/2;
	_ghostSpin = 0.0f;
	_midPoint = get_vec_point();
	_health = DEACON_HEALTH_MAX;
	_iTime = DEACON_I_TIME;
	for( uint8_t i = 0u; i < DEACON_GHOST_MAX; i++ )
	{
		_ghostOffsets[ i ] = _midPoint;
	}
	if( *_myHurt != nullptr ) return;
	IF_HURT_GET( *_myHurt )
	{
		(*_myHurt)->target = this;
	}
	IF_HURT_GET( _ballAttack.hurt )
	{
		_ballAttack.hurt->target = this;
	}
	_attackTimer = 0.0f;
	_firstProc = false;
	_attackProc = 0u;
	_lastAttack = TOTAL;
}

void Deacon::draw()
{
	const float p{ std::min(_iTime/DEACON_I_TIME, 1.0f) };
	const glm::vec4 col{ 1.0f, p, p, 1.0f };
	for( unsigned char i = 0; i < DEACON_GHOST_MAX; i++ )
	{
		glm::mat4 foo{ glm::translate( glm::mat4(), _ghostOffsets[ i ] ) };
		if( i == _realGhostIndex and _lastAttack == CHANGE_GHOST and _attackTimer > 0.0f )
		{
			const float rot{ _attackTimer/_get_attack_time(_lastAttack) * static_cast<float>(2*M_PI) };
			foo = glm::rotate( foo, rot, { 0.0f, 1.0f, 0.0f } );
		}
		foo = glm::scale( foo, glm::vec3( DEACON_SIZE ) );
		//still needs an actual model
		DrawList::add( {foo, model::DEACON, texture::DEACON, col, -1.0f, 0u, 1.0f - get_health(), texture::BLOOD_TEX} );
	}

	//attacks
	if( _attackTimer <= 0.0f ) return;
	//ball
	if( _ballAttack.health > 0 and _lastAttack == BIG_BALL )
	{
		glm::mat4 ballMod{ glm::translate( glm::mat4(), _ballAttack.pos ) };
		static constexpr float WAVE_SIZE{ 0.42f };
		static constexpr float WAVE_REMAIN{ BALL_RAD - WAVE_SIZE };
		static constexpr float WAVE_TIMER{ 42.0f/TAU };
		const float wavx{ WAVE_SIZE * static_cast<float>(cos( (_attackTimer)*WAVE_TIMER) ) };
		const float wavy{ WAVE_SIZE * static_cast<float>(sin( (_attackTimer)*WAVE_TIMER) ) };
		ballMod = glm::scale( ballMod, glm::vec3(WAVE_REMAIN + wavx, WAVE_REMAIN + wavy, WAVE_REMAIN) );
		const unsigned short frame( _attackTimer*7.142f );
		DrawList::add( {ballMod, model::DEBUG_SPHERE, texture::ELECTRIC, color::WHITE, 0.0f, frame%4} );
	}
}

bool Deacon::get_hurt( float damage, const HurtCircle* from )
{
	if( from == _ballAttack.hurt )
	{
		if( _lastAttack == BIG_BALL and _ballAttack.health > 0 )
		{
			_ballAttack.health -= 1;
			return true;
		}
		return false;
	}
	if( from == *_myHurt )
	{
		_iTime = 0.0f;
		_health -= damage;
		return true;
	}
	return false;
}

float Deacon::_get_attack_time( attacks t ) const
{
	switch( t )
	{
		case BIG_BALL:
			return 4.4f;
		case EXPAND:
			return 3.0f;
		case CHANGE_GHOST:
			return 1.8f;
		case SEQUENCE_SHOT:
			return DEACON_SPIN_TIME;
		case TOTAL:
			return 1.0f;
	}
	return 1.0f;
}

uint8_t Deacon::_get_attack_count( attacks t ) const
{
	switch( t )
	{
		case BIG_BALL:
			return 1u;
		case EXPAND:
			return 1u;
		case CHANGE_GHOST:
			return 1u;
		case SEQUENCE_SHOT:
			return DEACON_GHOST_MAX*SEQ_COUNT;
		case TOTAL:
			return 1u;
	}
	return 1u;
}

bool Deacon::get_alive() const
{
	return _health > 0.0f;
}

float Deacon::get_health() const
{
	return _health * RECIPROCAL(DEACON_HEALTH_MAX);
}

float Deacon::get_misc() const
{
	return 0.0f;
}

const char* Deacon::get_dialogue_file( bool dyin ) const
{
	if( dyin )
		return "assets/story/deacon_die.txt";
	return "assets/story/deacon_start.txt";
}

unsigned Deacon::get_music() const
{
	return music::COME_DEPART;
}
