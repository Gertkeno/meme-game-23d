#include <boss/SuperSage.hpp>

#include <FrameTime.hpp>
#include <algorithm>
#include <HurtCircle.hpp>
#include <HitCircle.hpp>
#include <GameManager.hpp>
#include <Collision.hpp>
#include <DrawList.hpp>
#include <Wavefront.hpp>
#include <Projectile.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <gert_GLmath.hpp>

using namespace boss;
namespace//constants
{
	constexpr float MAX_HP{ 270.0f };
	constexpr float ATTACK_START_DELAY{ 0.0f };
	constexpr float HASH_SPREAD{ 3.3f };
	constexpr float I_TIME_MAX{ 0.3f };
	constexpr float FORCE_ATTACK_TIME{ 5.0f };

	constexpr float LASER_START_MOD{ 7.6f };
	constexpr float LASER_SIZE{ 2.8f };
	constexpr float LASER_TIME{ 3.2f };

	constexpr float APPLE_START_SIZE{ 6.0f };
	constexpr float APPLE_WAVE_PEAK{ 2.2f };
	constexpr float APPLE_CLOSE_OFFSET{ 6.0f };
	constexpr float SIZE{ 5.0f };

	constexpr float RANDF( RAND_MAX );
	constexpr float BIG_HASH_SIZE{ 4.0f };
	const glm::vec3 BIG_HASH_POINTS[]{
		{-0.33f, 1.0f,0.0f}, { 0.33f, 1.0f,0.0f},
		{-1.0f, 0.33f,0.0f}, {-0.33f, 0.33f,0.0f}, {0.33f, 0.33f,0.0f}, {1.0f, 0.33f,0.0f},
		{-1.0f,-0.33f,0.0f}, {-0.33f,-0.33f,0.0f}, {0.33f,-0.33f,0.0f}, {1.0f,-0.33f,0.0f},
		{-0.33f,-1.0f,0.0f}, { 0.33f,-1.0f,0.0f} };
	constexpr size_t BIG_HASH_POINT_TOTAL{ sizeof(BIG_HASH_POINTS)/sizeof(*BIG_HASH_POINTS) };
}

SuperSage::SuperSage(): _hashTagClock( 0.8f )
{
	_myHurt = nullptr;
	_apple = nullptr;
	_hashHit = false;
	*_model = glm::scale( glm::mat4(), glm::vec3(SIZE) );
}

SuperSage::~SuperSage()
{
	if( _myHurt )
		_myHurt->target = nullptr;
}

void SuperSage::update()
{
	const auto ft{ FrameTime::get_mod() };
	_iTime += ft;
	if( _hashHit or _forceAttack >= FORCE_ATTACK_TIME )
	{
		_attackTimer = ATTACK_START_DELAY;
		_attackProc = 0u;
		_firstProc = false;
		_lastAttack = random_fresh<attacks_t, attacks_t::TOTAL>(_lastAttack);
		_hashHit = false;
		_forceAttack = 0.0f;
	}
	else if( _attack_expired() )
		_forceAttack += ft;

	if( _attack_expired() and ++_hashTagClock )
	{
		Projectile * const foo{ GameManager::get_projectile() };
		if( nullptr != foo )
		{
			glm::vec3 target{ GameManager::get_player_pos() };
			target.z += Z_SPEED_DEFAULT;
			const auto rangle{ std::rand()/RANDF * TAU };
			const auto spread{ std::rand()/RANDF * HASH_SPREAD };
			target.x += std::cos(rangle) * spread;
			target.y += std::sin(rangle) * spread;
			foo->start( target, get_vec_point(), 1.0f, 3.0f, 2.5f, this, 1.0f );
			foo->useModel = model::APPLE;
			foo->useTex = texture::APPLE;
			foo->confirmHit = &_hashHit;
		}
	}

	_attack_update();

	const auto speed{ _lastAttack == attacks_t::BIG_HASH ? Z_SPEED_DEFAULT/2/SIZE : Z_SPEED_DEFAULT/SIZE };
	*_model = glm::translate( *_model, {0.0f, 0.0f, speed*ft} );
	*_myHurt->area = get_vec_point();

	//apple animation
	if( _apple_alive() )
	{
		_appleClock += ft;
		const glm::vec3 pos{ get_vec_point() };
		const glm::vec3 location{ std::cos( _appleClock )*APPLE_WAVE_PEAK + pos.x, pos.y, pos.z-APPLE_CLOSE_OFFSET };
		*_apple->area = location;
	}
}

void SuperSage::_attack_update()
{
	_attackTimer += FrameTime::get_mod();

	if( _attackTimer <= 0.0f )
		return;
	if( not _firstProc )
	{
		switch( _lastAttack )
		{
			case attacks_t::APPLE_GUARD:
				_apple->area->r = APPLE_START_SIZE;
				_appleClock = 0.0f;
				break;
			case attacks_t::LASER:
				{
					const auto rangle{ std::rand() * (RANDF*2)/RANDF };
					_laser.delta = { std::cos( rangle ), std::sin( rangle ) };
					_laser.pos = (_laser.delta * -LASER_START_MOD) + glm::vec2(GameManager::get_player_pos());
					_laser.delta = (LASER_START_MOD*2)/LASER_TIME * _laser.delta;
				}break;
			default: break;
		}
		_firstProc = true;
	}

	if( _attack_expired() )
		return;

	const float ctTime{ _attackTimer*(_get_attack_count(_lastAttack)/_get_attack_time(_lastAttack)) };
	if( ctTime >= 1.0f + _attackProc )
	{
		if( _lastAttack == attacks_t::BIG_HASH  )
		{
			const auto target{ GameManager::get_player_pos() };
			const auto me{ get_vec_point() };
			for( auto i = 0u; i < BIG_HASH_POINT_TOTAL; ++i )
			{
				Projectile * const foo{ GameManager::get_projectile() };
				if( nullptr == foo )
					break;

				const auto hashSpot{ BIG_HASH_POINTS[i]*BIG_HASH_SIZE };
				foo->start( target + hashSpot, me + hashSpot, 2.0f, 5.0f, 8.0f, this );
			}
		}
		++_attackProc;
	}

	//constant attacks
	switch( _lastAttack )
	{
		case attacks_t::LASER:
			_laser.pos += _laser.delta * static_cast<float>( FrameTime::get_mod() );
			{
				HitCircle * const foo{ GameManager::get_hit_circle() };
				if( foo == nullptr )
					break;
				const float z{ GameManager::get_player_pos().z };
				const glm::vec3 fullpos{ _laser.pos.x, _laser.pos.y, z };
				foo->start( 9.0f, this, { fullpos, LASER_SIZE } );
			}
			break;
		default: break;
	}
}

void SuperSage::draw()
{
	const float colorScale{ std::min( _iTime/I_TIME_MAX, 1.0f ) };
	const glm::vec4 col{ 1.0f, colorScale, colorScale, 1.0f };
	DrawList::add( {*_model, model::SUPER_SAGE, texture::SUPER_SAGE, col, -1.0f, 0, 1.0f - get_health(), texture::BLOOD_TEX} );
	_attack_draws();

	if( not _apple_alive() ) return;
	const Sphere& apos{ *_apple->area };
	glm::mat4 model{ glm::translate( glm::mat4(), apos.ct_vec3() ) };
	model = glm::scale( model, glm::vec3(apos.r) );
	DrawList::add( {model, model::APPLE, texture::APPLE, color::WHITE} );
}

void SuperSage::_attack_draws() const
{
	if( _attackTimer <= 0.0f or _attack_expired() )
		return;

	switch( _lastAttack )
	{
		case attacks_t::LASER:
			{
				glm::mat4 foo{ glm::translate( glm::mat4(), get_vec_point() ) };
				const float z{ GameManager::get_player_pos().z };
				const glm::vec3 rotTo{ gert::get_angles( { _laser.pos.x, _laser.pos.y, z }, get_vec_point() ) };
				foo = glm::rotate( foo, rotTo.y, { 1.0f, 0.0f, 0.0f } );
				foo = glm::rotate( foo, rotTo.x, { 0.0f, -1.0f, 0.0f } );
				const float dist{ get_vec_point().z - z };
				foo = glm::translate( foo, { 0.0f, 0.0f, -dist } );//offset to start at makin
				foo = glm::scale( foo, { LASER_SIZE, LASER_SIZE, dist } );

				DrawList::add( {foo, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, 1.0f} );
			}break;
		default:
			break;
	}
}

void SuperSage::start()
{
	BossBase::start();
	if( _myHurt == nullptr and (_myHurt = GameManager::get_hurt_circle()) == nullptr )
		return;

	_myHurt->target = this;
	_myHurt->area->r = SIZE;
	_hashHit = false;
	_lastAttack = attacks_t::TOTAL;
	_forceAttack = 0.0f;
	_appleClock = 0.0f;

	if( _apple == nullptr )
	{
		_apple = GameManager::get_hurt_circle();
		_apple->target = this;
	}
	*_apple->area = { get_vec_point(), 0.0f };
	_health = MAX_HP;
}

const char* SuperSage::get_dialogue_file( bool d ) const
{
	if( d )
		return "assets/story/super_die.txt";
	return "assets/story/super_start.txt";
}

byte SuperSage::_get_attack_count( attacks_t t ) const
{
	switch( t )
	{
		case attacks_t::TOTAL:
			return 1u;
		case attacks_t::APPLE_GUARD:
			return 1u;
		case attacks_t::LASER:
			return 1u;
		case attacks_t::BIG_HASH:
			return 4u;
	}
	return 1u;
}

float SuperSage::_get_attack_time( attacks_t t ) const
{
	switch( t )
	{
		case attacks_t::TOTAL:
			return 1.0f;
		case attacks_t::APPLE_GUARD:
			return 0.62f;
		case attacks_t::LASER:
			return LASER_TIME;
		case attacks_t::BIG_HASH:
			return 3.3f;
	}
	return 1.0f;
}

bool SuperSage::get_hurt( float damage, const HurtCircle* from )
{
	if( from == nullptr )
		return false;

	if( from == _apple )
	{
		if( not _apple_alive() )
			return false;

		_apple->area->r -= damage * 0.4f;
		return true;
	}

	if( from != _myHurt )
		return false;

	_health -= damage;
	_iTime = 0.0f;
	return true;
}

bool SuperSage::_apple_alive() const
{
	return _apple != nullptr and _apple->target == this and _apple->area->r > 1.2f;
}

float SuperSage::get_health() const
{
	return _health * RECIPROCAL(MAX_HP);
}

float SuperSage::get_misc() const
{
	return _forceAttack * RECIPROCAL(FORCE_ATTACK_TIME);
}

bool SuperSage::_attack_expired() const
{
	return _attackProc > _get_attack_count( _lastAttack );
}

unsigned SuperSage::get_music() const
{
	return music::JUNGLE_FEVER;
}
