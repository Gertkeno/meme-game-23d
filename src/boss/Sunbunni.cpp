#include <boss/Sunbunni.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <GameManager.hpp>
#include <gert_GLmath.hpp>
#include <Projectile.hpp>
#include <HurtCircle.hpp>
#include <FrameTime.hpp>
#include <ModelList.hpp>
#include <HitCircle.hpp>
#include <Wavefront.hpp>
#include <DrawList.hpp>
#include <random>

#ifdef _GERT_DEBUG
#include <iostream>
#endif

namespace
{
	constexpr float SIZE{ 4.4f };
	constexpr float MAX_HEALTH{ 340 };
	constexpr float MAX_RING_VELOCITY{ 7.0f };
	constexpr float RING_VELOCITY_DECAY{ MAX_RING_VELOCITY / 8 };

	constexpr float RING_VELOCITY_SCALAR{ 0.5f };
	constexpr float RING_RADIUS{ 5.0f };
	constexpr float ATTACK_DELAY{ -0.8f };
	constexpr float CROSS_RADIUS{ 2.7f };
	constexpr float RANDF( RAND_MAX );
	constexpr int PROTECT_DENSITY{ 8 };
	constexpr float I_TIME_MAX{ 0.4f };
	constexpr float EXPAND_HURT_RANGE{ 16.0f };
}

using namespace boss;

Sunbunni::Sunbunni()
{
	_myHurt = nullptr;
	*_model = glm::scale( glm::mat4(), glm::vec3(SIZE) );
}

Sunbunni::~Sunbunni()
{
	if( _myHurt != nullptr and _myHurt->target == nullptr )
		_myHurt->target = nullptr;
}

void Sunbunni::update()
{
	_iTime += FrameTime::get_mod();
	_ring.blueTime += FrameTime::get_mod();
	if( _ring.velocity > 0.0f )
	{
		const float deltaV( _ring.velocity * FrameTime::get_mod() );
		_ring.position = _ring.position.ct_vec3() + (_ring.vector * deltaV);
		_ring.velocity -= RING_VELOCITY_DECAY * FrameTime::get_mod();
	}
	_ring.position.z = GameManager::get_player_pos().z;

	if( _attack_expired() )
	{
		_attackProc = 0u;
		_attackTimer = ATTACK_DELAY;
		_firstProc = false;
		_lastAttack = random_fresh<attacks_t, attacks_t::TOTAL>(_lastAttack);
	}

	_attack_update();

	const auto pos{ get_vec_point() };
	const auto x{ _ring.position.x - pos.x };
	const auto y{ _ring.position.y - pos.y };
	static constexpr float zspeed{ Z_SPEED_DEFAULT/SIZE };
	*_model = glm::translate( *_model, {x/SIZE, y/SIZE, zspeed*FrameTime::get_mod()} );
	*_myHurt->area = get_vec_point();
}

void Sunbunni::_attack_update()
{
	_attackTimer += FrameTime::get_mod();

	if( _attackTimer <= 0.0f )
		return;
	if( not _firstProc )
	{
		switch( _lastAttack )
		{
			case attacks_t::CHANGE_VECTOR:
				_randomize_ring_vector();
				break;
			default: break;
		}
		_firstProc = true;
	}

	if( _attack_expired() )
		return;

	const float ctTime{ _attackTimer*(_get_attack_count(_lastAttack)/_get_attack_time(_lastAttack)) };
	if( ctTime >= 1.0f + _attackProc )
	{
		switch( _lastAttack )
		{
			case attacks_t::PROTECT_RING:
				for( auto i = 0; i < PROTECT_DENSITY; ++i )
				{
					Projectile * const f{ GameManager::get_projectile() };
					if( f == nullptr )
						break;

					glm::vec3 target{ _ring.position.ct_vec3() };
					static constexpr double t{ TAU/PROTECT_DENSITY  };
					target.y += std::sin( i * t ) * RING_RADIUS;
					target.x += std::cos( i * t ) * RING_RADIUS;
					target.z = _ring.position.z;
					f->start( target, get_vec_point(), 1.2f, 5.0f, 6, this );
				}
				break;
			default: break;
		}
		++_attackProc;
	}

	//constant attacks
	switch( _lastAttack )
	{
		case attacks_t::CROSS_LASER:
			if( _attackProc == 0 )
				_cross.x = GameManager::get_player_pos().x;
			else if( _attackProc == 1 )
				_cross.y = GameManager::get_player_pos().y;
			if( _attackProc > 1 ) // y hit
			{
				HitCircle * const foo { GameManager::get_hit_circle() };
				const auto pp{ GameManager::get_player_pos() };
				foo->start( 9.0f, this, {_cross.x, pp.y, pp.z, CROSS_RADIUS} );
			}
			if( _attackProc > 2 ) // x hit
			{
				HitCircle * const foo { GameManager::get_hit_circle() };
				const auto pp{ GameManager::get_player_pos() };
				foo->start( 9.0f, this, {pp.x, _cross.y, pp.z, CROSS_RADIUS} );
			}
			break;
		case attacks_t::EXPAND_HURT:
			{
				HitCircle * const foo{ GameManager::get_hit_circle() };
				if( nullptr == foo ) break;
				const Sphere area{ get_vec_point(), ctTime * EXPAND_HURT_RANGE };
				foo->start(9, this, area);
			}break;
		default: break;
	}
}

bool Sunbunni::get_hurt( float damage, const HurtCircle * from )
{
	if( from != _myHurt )
		return false;
	_ring.velocity += damage * RING_VELOCITY_SCALAR;
	if( _ring.velocity > MAX_RING_VELOCITY )
		_ring.velocity = MAX_RING_VELOCITY;

	const auto pp{ GameManager::get_player_pos() };
	if( collision::distance( pp, _ring.position ) <= _ring.position.r )
	{
		_health -= damage;
		_iTime = 0.0f;
	}
	else
		_ring.blueTime = 0.0f;
	return true;
}

byte Sunbunni::_get_attack_count( attacks_t t ) const
{
	switch( t )
	{
		case attacks_t::TOTAL:
			return 0u;
		case attacks_t::CHANGE_VECTOR:
			return 1u;
		case attacks_t::EXPAND_HURT:
			return 1u;
		case attacks_t::PROTECT_RING:
			return 7u;
		case attacks_t::CROSS_LASER:
			return 4u;
	}
	return 0u;
}

float Sunbunni::_get_attack_time( attacks_t t ) const
{
	switch( t )
	{
		case attacks_t::TOTAL:
			return 0.0f;
		case attacks_t::CHANGE_VECTOR:
			return 0.8f;
		case attacks_t::EXPAND_HURT:
			return 1.8f;
		case attacks_t::PROTECT_RING:
			return 2.8f;
		case attacks_t::CROSS_LASER:
			return 5.4f;
	}
	return 0.0f;
}

bool Sunbunni::_attack_expired() const
{
	return _attackProc > _get_attack_count( _lastAttack );
}

void Sunbunni::_randomize_ring_vector()
{
	const float x( std::rand() - RAND_MAX/2 );
	const float y( std::rand() - RAND_MAX/2 );
	const float t{ std::abs(x) + std::abs(y) };
	if( t == 0 )
		return;
	_ring.vector = { x/t, y/t, 0.0f };
}

void Sunbunni::start()
{
	BossBase::start();
	_randomize_ring_vector();

	if( _myHurt == nullptr and (_myHurt = GameManager::get_hurt_circle()) == nullptr )
		return;

	_myHurt->target = this;
	_myHurt->area->r = SIZE;
	_ring.position.x = _ring.position.y = 0.0f;
	_ring.position.r = RING_RADIUS;
	_ring.velocity = 0.0f;
	_ring.blueTime = 0.0f;
	_attackTimer = ATTACK_DELAY*2;
	_lastAttack = random_fresh<attacks_t, attacks_t::TOTAL>(_lastAttack);

	_health = MAX_HEALTH;
}

void Sunbunni::draw()
{
	const float colorScale{ std::min( _iTime/I_TIME_MAX, 1.0f ) };
	const glm::vec4 col{ 1.0f, colorScale, colorScale, 1.0f };
	DrawList::add( {*_model, model::SUNBUNNI, texture::SUNBUNNI, col, -1.0f, 0, 1.0f - get_health(), texture::CRACK_TEX} );

	const float blueScale{ std::min( _ring.blueTime/I_TIME_MAX, 1.0f ) };
	const glm::vec4 ringColor{ blueScale, blueScale, 1.0f, 1.0f };
	DrawList::add( {_ring.position.ct_mat(), model::RING, texture::PLAYER_SHIP, ringColor} );

	if( _attackTimer <= 0.0f ) return;

	switch( _lastAttack )
	{
	case attacks_t::EXPAND_HURT:
		{
			const Sphere foo{ get_vec_point(), _attackTimer/_get_attack_time(_lastAttack) * EXPAND_HURT_RANGE };

			const auto f{ int( _attackTimer * 1000/150)%4 };
			DrawList::add( {foo.ct_mat(), model::DEBUG_SPHERE, texture::ELECTRIC, color::WHITE, 0.0f, f} );
		}break;
	case attacks_t::CROSS_LASER:
		{
		const auto ppos{ GameManager::get_player_pos() };
		glm::mat4 modspace{ glm::translate( glm::mat4(), {_cross.x, ppos.y, ppos.z} ) };
		modspace = glm::rotate( modspace, static_cast<float>(M_PI/2.0f), { 0.0f, 1.0f, 0.0f } );
		modspace = glm::rotate( modspace, static_cast<float>(M_PI/2.0f), { 1.0f, 0.0f, 0.0f } );
		const float rads{ _attackProc > 1 ? CROSS_RADIUS : CROSS_RADIUS*0.8f };
		const float shadin{ _attackProc > 1 ? 1.0f : 0.0f };
		modspace = glm::scale( modspace, { rads, rads, 400.0f } );

		DrawList::add( {modspace, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, shadin} );
		if( _attackProc > 0 )
		{
			glm::mat4 modspace{ glm::translate( glm::mat4(), {ppos.x, _cross.y, ppos.z} ) };
			modspace = glm::rotate( modspace, static_cast<float>(M_PI/2.0f), { 0.0f, 1.0f, 0.0f } );
			const float rads{ _attackProc > 2 ? CROSS_RADIUS : CROSS_RADIUS*0.6f };
			const float shadin{ _attackProc > 2 ? 1.0f : 0.0f };
			modspace = glm::scale( modspace, { rads, rads, 400.0f } );

			DrawList::add( {modspace, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, shadin} );
		}
		}break;
	default:
		break;
	}
}

const char * Sunbunni::get_dialogue_file( bool d ) const
{
	if( d )
		return "assets/story/sunbunni_die.txt";
	return "assets/story/sunbunni_start.txt";
}

float Sunbunni::get_health() const
{
	return _health * RECIPROCAL(MAX_HEALTH);
}

float Sunbunni::get_misc() const
{
	return _ring.velocity * RECIPROCAL(MAX_RING_VELOCITY);
}

unsigned Sunbunni::get_music() const
{
	return music::UH_OH;
}
