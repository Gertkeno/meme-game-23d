#include <boss/Toaster.hpp>

#include <GameManager.hpp>
#include <HurtCircle.hpp>
#include <HitCircle.hpp>
#include <ModelList.hpp>
#include <FrameTime.hpp>
#include <Wavefront.hpp>
#include <Collision.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Projectile.hpp>
#include <random>
#include <DrawList.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG
using namespace boss;

namespace
{
	constexpr float MAX_HEALTH{ 275.0f };
	constexpr float MAX_BIDE{ 22.0f };
	constexpr float BIDE_REDUCTION{ 1.0f };
	constexpr float I_TIME{ 0.15f };
	constexpr float SCALE{ 3.2f };
	constexpr float HURT_SCALE{ SCALE * 1.1f };
	constexpr float ATTACK_DELAY{ -1.9f };

	constexpr float TOAST_LIFETIME{ 3.4f };
	constexpr float TOAST_SIZE{ 1.76f };
	constexpr float TOAST_SPEED{ 50.0f };//should move from start point to player in one second

	constexpr float LOCK_SCALE_RATIO{ 1.4f };
	constexpr float LOCK_SCALE_OFFSET{ 0.5f };

	constexpr byte CROSS_SHOT_COUNT{ 7u };
}

Toasterman::Toasterman()
{
	_myHurt = nullptr;
	_point.x = _point.y = 0.0f;
}

Toasterman::~Toasterman()
{
	if( _myHurt != nullptr )
		_myHurt->target = nullptr;
}

const char* Toasterman::get_dialogue_file( bool dying ) const
{
	if( dying )
		return "assets/story/toast_die.txt";
	return "assets/story/toast.txt";
}

bool Toasterman::_process_attack( attacks type )
{
	if( _attackTimer <= 0.0f ) return false;
	if( not _firstProc )//first proc
	{
		switch( type )
		{
			case LOCK_ON_ISH:
				static constexpr float GRAIN{ RECIPROCAL(333.3f) };
				_reticle.offset.x = (rand()%1000)*GRAIN - 1.5f;
				_reticle.offset.y = (rand()%1000)*GRAIN - 1.5f;
				_reticle.offset.z = (rand()%1000)*GRAIN - 1.5f;
				break;
			case BIDE:
				_iTime = I_TIME;
				break;
			default: break;
		}
		_firstProc = true;
	}
	const float ctTime{ _attackTimer*( _get_attack_count(type) / _get_attack_time(type) ) };
	if( ctTime >= ( _attackProc + 1.0f ) )
	{
		switch( type )
		{
			case LOCK_ON_ISH:
				if( _attackProc > 2 )
				{
					Projectile * const foo{ GameManager::get_projectile() };
					if( nullptr == foo ) break;
					foo->start( _reticle.point, get_vec_point(), 1.3f, 2.8f, 6.0f, this, 1.2f );
				}
				break;
			case CROSS:
				for( byte i = 0; i < CROSS_SHOT_COUNT; ++i )
				{
					static constexpr float DAMAGE{ 8.0f };
					static constexpr float SPEED{ 1.0f };
					static constexpr float LIFE{ 3.0f };
					static constexpr float SPREAD{ 12.0f };
					Projectile* foo;
					IF_SHOT_GET( foo )
					{
						glm::vec3 start{ get_vec_point() };
						if( _attackProc % 2 == 0 )
							start.x -= float(i)*RECIPROCAL(CROSS_SHOT_COUNT) * SPREAD - SPREAD/2;
						else
							start.y -= float(i)*RECIPROCAL(CROSS_SHOT_COUNT) * SPREAD - SPREAD/2;

						glm::vec3 dir{ start };
						dir.z -= 1.0f;
						foo->start( dir, start, SPEED, LIFE, DAMAGE, this );
					}
				}
				break;
			default: break;
		}
		++_attackProc;
	}

	//constantly
	switch( type )
	{
		case LOCK_ON_ISH:
			_reticle.point = GameManager::get_player_pos();
			_reticle.point += _reticle.offset;
			break;
		default: break;
	}

	return _attackProc >= _get_attack_count( type );
}

void Toasterman::update()
{
	_iTime += FrameTime::get_mod();
	_attackTimer += FrameTime::get_mod();
	if( _process_attack( _lastAttack ) )
	{
		_start_new_attack();
	}

	//start bread, reset points etc
	if( _bide >= MAX_BIDE )
	{
		{
			const float ztemp{ _point.z };
			_point = GameManager::get_player_pos();
			_point.z = ztemp;
		}
		//TODO: play anime teleport sound
		_bide = 0.0f;
		for( byte i = 0; i < MAX_TOAST; ++i )
		{
			_toast[i].timer = 0.0f;
			_toast[i].point = _point;
			_toast[i].point.y += i*3 - 1.5f;
		}
	}
	else if( _bide > 0.0f )
	{
		_bide -= BIDE_REDUCTION * FrameTime::get_mod();
	}

	//continue breads
	for( byte i = 0; i < MAX_TOAST; ++i )
	{
		if( _toast[i].timer >= TOAST_LIFETIME ) continue;
		_toast[i].point.z -= TOAST_SPEED * FrameTime::get_mod();
		_toast[i].timer += FrameTime::get_mod();
		HitCircle * const foo{ GameManager::get_hit_circle() };
		if( nullptr == foo ) continue;
		foo->start( 11.0f, this, {_toast[i].point, TOAST_SIZE} );
	}

	float zspeed{ Z_SPEED_DEFAULT };
	switch( _lastAttack )
	{
		case BIDE:
			zspeed = Z_SPEED_DEFAULT/2;
			break;
		default:
			break;
	}
	_point.z += zspeed * FrameTime::get_mod();
	*_model = glm::translate( glm::mat4(), _point );
	//hurt sphere update
	if( _myHurt != nullptr )
	{
		*_myHurt->area = Sphere{ get_vec_point(), HURT_SCALE };
		_myHurt->area->y -= 0.63f;
	}
}

void Toasterman::draw()
{
	glm::vec4 cmod{ color::WHITE };
	if( _iTime < I_TIME )
	{
		cmod.g = cmod.b = _iTime/I_TIME;
		if( _lastAttack == BIDE and _attackTimer > 0.0f )
		{
			cmod.r = cmod.g;
			cmod.b = 1.0f;
		}
	}
	glm::mat4 copyModel{ glm::scale( *_model, glm::vec3( SCALE ) ) };

	DrawList::add( {copyModel, model::TOASTER, texture::TOASTER, cmod, -1.0f, 0, 1.0f - get_health(), texture::CRACK_TEX} );

	//attack draws
	if( _attackTimer > 0.0f )
	{
		switch( _lastAttack )
		{
			case BIDE:
				DrawList::add( {glm::rotate( copyModel, _attackTimer, glm::vec3( 0.5f, 0.0f, 0.5f ) ), model::DEBUG_SPHERE, texture::ELECTRIC, color::WHITE} );
				break;
			case LOCK_ON_ISH:
				copyModel = glm::translate( glm::mat4(), _reticle.point );
				{
					const float scaleRatio{ 1.0f - _attackTimer*RECIPROCAL(_get_attack_time(LOCK_ON_ISH)) };
					copyModel = glm::scale( copyModel, glm::vec3( scaleRatio * LOCK_SCALE_RATIO + LOCK_SCALE_OFFSET ) );
					DrawList::add( {copyModel, model::PLANE_2D, texture::RETICLE, color::RED} );
				}
				break;
			default: break;
		}
	}

	//draw toast bread shots
	for( byte i = 0; i < MAX_TOAST; ++i )
	{
		if( _toast[i].timer >= TOAST_LIFETIME ) continue;
		copyModel = glm::translate( glm::mat4(), _toast[i].point );
		copyModel = glm::scale( copyModel, glm::vec3( TOAST_SIZE ) );
		copyModel = glm::rotate( copyModel, float(M_PI/-2), glm::vec3( 0, 1, 0 ) );
		copyModel = glm::rotate( copyModel, float(M_PI/2), glm::vec3( 0, 0, 1 ) );
		DrawList::add( {copyModel, model::BREAD, texture::ELECTRIC, color::WHITE} );
	}
}

void Toasterman::start()
{
	BossBase::start();
	_point = get_vec_point();
	for( byte i = 0; i < MAX_TOAST; ++i )
	{
		_toast[i].timer = TOAST_LIFETIME;
	}

	_bide = 0.0f;
	_attackTimer = 0.0f;
	_iTime = I_TIME;
	_lastAttack = TOTAL;

	if( _myHurt != nullptr ) return;
	IF_HURT_GET( _myHurt )
	{
		_health = MAX_HEALTH;
		_bide = 0.0f;
		_myHurt->target = this;
	}
}

bool Toasterman::get_alive() const
{
	return _health > 0.0f;
}

float Toasterman::get_health() const
{
	return _health*RECIPROCAL(MAX_HEALTH);
}

float Toasterman::get_misc() const
{
	return _bide * RECIPROCAL(MAX_BIDE);
}

bool Toasterman::get_hurt( float damage, const HurtCircle* from )
{
	if( from == nullptr or from != _myHurt ) return false;
	_iTime = 0.0f;
	_bide += _lastAttack == BIDE ? damage*2 : damage;
	_health -= damage;

	return true;
}

float Toasterman::_get_attack_time( attacks t ) const
{
	switch( t )
	{
		case BIDE: return 5.0f;
		case CROSS: return 4.32f;
		case LOCK_ON_ISH: return 3.55f;
		case TOTAL: break;
	}
	return 1.0f;
}

byte Toasterman::_get_attack_count( attacks t ) const
{
	switch( t )
	{
		case BIDE: break;
		case CROSS: return 5u;
		case LOCK_ON_ISH: return 16u;
		case TOTAL: break;
	}
	return 1u;
}

void Toasterman::_start_new_attack()
{
	_attackTimer = ATTACK_DELAY;
	_attackProc = 0u;
	_firstProc = false;
	_lastAttack = random_fresh<attacks, TOTAL>(_lastAttack);
}

unsigned Toasterman::get_music() const
{
	return music::SPARTA_TOASTER;
}
