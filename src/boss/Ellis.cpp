#include <boss/Ellis.hpp>
#include <Collision.hpp>
#include <HurtCircle.hpp>
#include <GameManager.hpp>
#include <algorithm>
#include <iterator>
#include <FrameTime.hpp>
#include <Projectile.hpp>
#include <ModelList.hpp>
#include <Wavefront.hpp>
#include <HitCircle.hpp>
#include <DrawList.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif
using namespace boss;
namespace//constants
{
	constexpr float ATTACK_DELAY{ -1.46f };
	constexpr float MAX_HEALTH{ 520.0f };
	constexpr float SIZE{ 4.4f };
	constexpr float BUD_TIME_MAX{ 3.7f };
	constexpr float BUD_HEALTH{ 32.0f };
	constexpr float BUD_DEATH_TIME{ 0.372f };
	constexpr float BUDDING_RADIUS_MIN{ 13.0f };
	constexpr float I_TIME{ 0.23f };
	constexpr float LASER_RADIUS{ 3.1f };
}

Ellis::Ellis()
{
	_poweringBud = &_buddies[0];

	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		_buddies[i].hurt = nullptr;
	}
}

Ellis::~Ellis()
{
	_buds_reset();
}

bool Ellis::_attack_update( attacks t )
{
	if( _attackTimer <= 0.0f ) return false;
	//first
	if( not _firstProc )
	{
		switch( t )
		{
			case POWER_UP:
				_poweringBud = _random_active_bud();
				break;
			default: break;
		}
		_firstProc = true;
	}
	//per count
	const float ctTime{ _attackTimer*(_get_attack_count(t)/_get_attack_time(t)) };
	if( ctTime >= _attackProc + 1.0f )
	{
		switch( t )
		{
			case RANDOM_SHOOTERS:
				{
					const Bud * const picked{ _random_active_bud() };
					if( nullptr == picked ) break;
					Projectile * const get{ GameManager::get_projectile() };
					if( nullptr == get ) break;
					glm::vec3 ctpos{ GameManager::get_player_pos() };
					ctpos.z += Z_SPEED_DEFAULT;
					get->start( ctpos, picked->point, 1.7f, 3.0f, 5.5f, this, 1.6f );
				}break;
			case POWER_UP:
				if( _attackProc == _get_attack_count(t)-1 )
				{
					if( _bud_dead( _poweringBud ) ) break;
					HitCircle * const get{ GameManager::get_hit_circle() };
					if( nullptr == get ) break;
					get->start( 7.0f, this, Sphere{ GameManager::get_player_pos(), 1.0f } );
				}
				break;
			default: break;
		}
		++_attackProc;
	}
	//constant
	switch( t )
	{
		case POWER_UP:
			if( _bud_dead( _poweringBud ) )
			{
				_cycle_attack();
			}
			break;
		case LASERS:
			if( _attackProc < 1 ) break;
			_per_active([this](Bud *t) {
				HitCircle * const pos{ GameManager::get_hit_circle() };
				if (nullptr == pos)
					return;
				const Sphere spot{ {t->point.x, t->point.y, GameManager::get_player_pos().z}, LASER_RADIUS };
				pos->start( 11.0f, this, spot );
			});
			break;
		default: break;
	}
	return _attackProc >= _get_attack_count( t );
}

void Ellis::update()
{
	_attackTimer += FrameTime::get_mod();
	if( _budTimer <= BUD_TIME_MAX )
		_budTimer += FrameTime::get_mod();
	if( _attack_update( _lastAttack ) )
		_cycle_attack();

	const bool time_up{ _budTimer > BUD_TIME_MAX * (_activeBuds > 1 ? 1.0f : 0.5f) };
	if( time_up and _lastAttack != attacks::LASERS )
	{
		Bud * const starter( _inactive_bud() );
		if( starter != nullptr )
		{
			Bud * const parent( _random_active_bud() );
			if( parent != nullptr )
			{
				if( _start_bud( starter, parent->point ) )
				{
					++_activeBuds;
				}
			}
		}
	}

	//bud updates
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		if( _bud_dead( i ) )
		{
			_buddies[i].deadTime += FrameTime::get_mod();
			continue;
		}
		_buddies[i].redTime += FrameTime::get_mod();
		_buddies[i].point.z += Z_SPEED_DEFAULT * FrameTime::get_mod();
		//hurtsphere updates
		*(_buddies[i].hurt->area) = Sphere{ _buddies[i].point, SIZE };
	}
	set_vec_point( _far_bud()->point );
}

void Ellis::draw()
{
	//bud draw
	glm::mat4 budmod;
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		if( _bud_dead( i ) ) continue;
		budmod = glm::translate( glm::mat4(), _buddies[i].point );
		budmod = glm::scale( budmod, glm::vec3( SIZE *0.8f) );
		glm::vec4 col = color::WHITE;
		if( _buddies[i].redTime < I_TIME )
		{
			col.g = col.b = _buddies[i].redTime/I_TIME;
		}
		DrawList::add( {budmod, model::ELLIS, texture::ELLIS, col, -1.0f, 0, 1.0f - (_buddies[i].health/BUD_HEALTH), texture::BLOOD_TEX} );
	}

	const float attackAnimater{ _attackTimer/_get_attack_time(_lastAttack)*static_cast<float>(TAU) };
	if( _attackTimer <= 0.0f )
		return;
	switch( _lastAttack )
	{
		case POWER_UP:
			if( not _bud_dead( _poweringBud ) )
			{
				budmod = glm::translate( glm::mat4(), _poweringBud->point );
				budmod = glm::rotate( budmod, attackAnimater, { 0.0f, 1.0f, 0.0f } );
				constexpr float SIGNALSIZE{ SIZE*1.2f };
				budmod = glm::scale( budmod, glm::vec3( SIGNALSIZE ) );
				DrawList::add( {budmod, model::DEBUG_SPHERE, texture::ELECTRIC, color::WHITE} );
			}
			break;
		case LASERS:
			{
				float shading;
				float radius;
				float wavy, wavx;
				static constexpr float WAVE_SIZE{ 0.11f };
				static constexpr float WAVE_REMAINDER{ 1.0f - WAVE_SIZE };
				static constexpr float WAVE_SPEED{ 2.0f };
				if( _attackProc >= 1 )
				{
					radius  = 1.0f;
					shading = 1.0f;
					wavx = WAVE_SIZE * cos( _attackTimer * WAVE_SPEED * TAU );
					wavy = WAVE_SIZE * sin( _attackTimer * WAVE_SPEED * TAU );
				}
				else
				{
					wavy = wavx = WAVE_SIZE;
					radius = 0.6f;
					shading = 0.0f;
				}
				radius = LASER_RADIUS*radius;
				_per_active( [&](Bud* t)
				{
					glm::mat4 mod = glm::translate( glm::mat4(), t->point );
					mod = glm::scale( mod, { radius, radius, 200 } );
					mod = glm::scale( mod, { WAVE_REMAINDER + wavx, WAVE_REMAINDER + wavy, 1.0f } );
					mod = glm::translate( mod, { 0.0f, 0.0f, -1.0f } );
					DrawList::add( {mod, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, shading} );
				});
			}
			break;
		default: break;
	}
}

void Ellis::start()
{
	//BossBase takes care of attack procs, first proc, and position
	BossBase::start();
	_budTimer = 0.0f;
	_health = MAX_HEALTH;

	_buds_reset();

	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		IF_HURT_GET( _buddies[i].hurt )
		{
			_buddies[i].hurt->target = this;
		}
	}
	if( _start_bud( _inactive_bud(), get_vec_point() ) )
	{
		_activeBuds = 1;
	}
}

void Ellis::_cycle_attack()
{
	_attackTimer = ATTACK_DELAY;
	_attackProc = 0u;
	_firstProc = false;

	_lastAttack = random_fresh<attacks, TOTAL>(_lastAttack);
}

void Ellis::_buds_reset()
{
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		if( _buddies[i].hurt != nullptr )
			_buddies[i].hurt->target = nullptr;
		_buddies[i].hurt = nullptr;
		_buddies[i].health = 0.0f;
		_buddies[i].redTime = I_TIME;
		_buddies[i].deadTime = BUD_DEATH_TIME;
	}
	_activeBuds = 0u;
}

bool Ellis::get_hurt( float damage, const HurtCircle* from )
{
	if( from == nullptr ) return false;
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		if( from != _buddies[i].hurt ) continue;
		if( _bud_dead( i ) ) continue;//check if bud is alive
		if( _activeBuds > 1 )
		{
			_buddies[i].health -= damage;
			if( _bud_dead( i ) )
			{
				_buddies[i].deadTime = 0.0f;
				--_activeBuds;
			}
		}
		_buddies[i].redTime = 0.0f;
		_health -= damage;
		return true;
	}
	return false;
}

bool Ellis::get_alive() const
{
	return _health > 0.0f;
}

float Ellis::get_health() const
{
	return _health * RECIPROCAL(MAX_HEALTH);
}

float Ellis::get_misc() const
{
	return _budTimer * RECIPROCAL(BUD_TIME_MAX);//return bud count normalized
}

const char * Ellis::get_dialogue_file( bool d ) const
{
	if( d )
		return "assets/story/ellis_die.txt";
	return "assets/story/ellis_start.txt";
}

float Ellis::_get_attack_time( attacks t ) const
{
	switch( t )
	{
		case RANDOM_SHOOTERS: return 3.2f;
		case POWER_UP: return 7.0f;
		case LASERS: return 3.6f;
		case TOTAL: return 1.0f;
	}
	return 0.0f;
}

byte Ellis::_get_attack_count( attacks t ) const
{
	switch( t )
	{
		case RANDOM_SHOOTERS: return _activeBuds;
		case POWER_UP: return 7u;
		case LASERS: return 2u;
		case TOTAL:	return 1u;
	}
	return 0u;
}

Ellis::Bud* Ellis::_inactive_bud()
{
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		if( _buddies[i].health > 0.0f ) continue;
		return &_buddies[i];
	}
	return nullptr;
}

Ellis::Bud* Ellis::_random_active_bud()
{
	byte randex[MAX_BUDS];
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		randex[i] = i;
	}
	std::random_shuffle( std::begin(randex), std::end(randex) );
	//randex values randomized for use as index in this next bit
	for( byte i = 0u; i < MAX_BUDS; ++i )
	{
		if( _bud_dead( randex[i] ) ) continue;
		return &_buddies[randex[i]];
	}
	return nullptr;
}

Ellis::Bud* Ellis::_far_bud()
{
	Bud* temp( _random_active_bud () );
	for( byte i = 1u; i < MAX_BUDS; ++i )
	{
		if( _bud_dead( i ) ) continue;
		if( temp->point.z > _buddies[i].point.z ) continue;
		temp = &_buddies[i];
	}
	return temp;
}

bool Ellis::_start_bud( Ellis::Bud* target, glm::vec3 home )
{
	if( target == nullptr ) return false;
	//constant sets
	_budTimer = 0.0f;
	target->health = BUD_HEALTH;
	target->redTime = I_TIME;
	constexpr int RANDOMGRIT{ 555 }; //defines how small the value between values will be for next rand()
	constexpr float TAUCONVERT{ 1/float(RANDOMGRIT) * M_PI*2 }; //conversion formula constant
	const float budAngle{ (rand()%RANDOMGRIT) * TAUCONVERT };
	target->point.x = home.x + cos( budAngle ) * BUDDING_RADIUS_MIN;
	target->point.y = home.y + sin( budAngle ) * BUDDING_RADIUS_MIN;
	target->point.z = home.z;
	return true;
}

bool Ellis::_bud_dead( byte index )
{
	return _buddies[index].health <= 0.0f;
}

bool Ellis::_bud_dead( Ellis::Bud* ptr )
{
	if( ptr == nullptr ) return true;
	return ptr->health <= 0.0f;
}

unsigned Ellis::get_music() const
{
	return music::THIS_WAY_THAT;
}
