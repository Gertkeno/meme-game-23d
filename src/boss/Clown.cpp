#include <boss/Clown.hpp>

#include <algorithm>
#include <FrameTime.hpp>
#include <HurtCircle.hpp>
#include <HitCircle.hpp>
#include <GameManager.hpp>
#include <DrawList.hpp>
#include <Wavefront.hpp>
#include <Projectile.hpp>
#include <Collision.hpp>
#include <random>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
using namespace boss;

namespace
{
	constexpr float MAX_HEALTH{ 666 + 100 };
	constexpr float SIZE{ 7.0f };
	constexpr float ATTACK_DELAY{ 0.8f };
	constexpr float I_TIME_MAX{ 0.28f };

	constexpr float RANDF(1.0f / RAND_MAX);

	constexpr float CLUSTER_TIME_MAX{ 2.0f };
	constexpr float CLUSTER_RADIUS{ 1.2f };
	constexpr float CLUSTER_SPREAD_MAX{ 2.0f - 1.0f };

	constexpr int SPEWER_POINTS{ 7 };
	constexpr float SPEWER_OFFSET{ 2.8f };
	constexpr float SPEWER_TIME{ 3.3f };

	constexpr float BOX_LASER_RADIUS{ 3.0f };
	constexpr float BOX_MIN_DIST{ -2.0f };
	constexpr float BOX_MAX_DIST{ 11.0f };
	constexpr float BOX_DIST_DIFF{ BOX_MAX_DIST - BOX_MIN_DIST };

	constexpr float BOX_TIME{ 4.4f };

	constexpr float BOMB_SPREAD{ 2.4f };
	constexpr float BOMB_FUSE_START{ 0.87f };
	constexpr float BOMB_FUSE_MIN{ -0.4f };
	constexpr float BOMB_RADIUS{ 2.36f };
}

Clown::Clown( bool fonz ):
	BossBase()
	,_myHurt{nullptr}
	,_fonzi{fonz}
	,_texture{fonz ? texture::FONZI : texture::BOBO }
{
	*_model = glm::scale( glm::mat4(), glm::vec3(SIZE) );
}

Clown::~Clown()
{
	if( _myHurt != nullptr )
		_myHurt->target = nullptr;
}

void Clown::update()
{
	const auto ft{ FrameTime::get_mod() };
	_iTime += ft;

	if( _attack_expired() )
	{
		_attackProc = 0u;
		_attackTimer = ATTACK_DELAY;
		_firstProc = false;
		_lastAttack = random_fresh<attacks_t, attacks_t::TOTAL>(_lastAttack);
	}

	_attack_update();

	for( auto & i : _bombs )
	{
		i.fuse -= ft;
		if( i.fuse < BOMB_FUSE_MIN )
			continue;
		if( i.fuse > 0.0f )
			continue;
		HitCircle * const foo{ GameManager::get_hit_circle() };
		if( foo == nullptr )
			break;
		foo->start( 12.0f, this, {i.pos, BOMB_RADIUS} );
	}

	const float speed( Z_SPEED_DEFAULT/SIZE * ft );
	*_model = glm::translate( *_model, {0.0f, 0.0f, speed} );
	if( _myHurt != nullptr )
		*_myHurt->area = get_vec_point();
}

void Clown::_attack_update()
{
	_attackTimer += FrameTime::get_mod();

	if( _attackTimer <= 0.0f )
		return;

	if( not _firstProc )
	{
		switch( _lastAttack )
		{
			case BOX_LASER:
				_laserStart = GameManager::get_player_pos().x;
				break;
			case CLUSTER:
				{
					_cluster.pos = get_vec_point();
					constexpr float travel{ Z_SPEED_DEFAULT * CLUSTER_TIME_MAX };
					const float distance{ glm::distance( _cluster.pos, GameManager::get_player_pos() ) - travel };
					const glm::vec3 diff( _cluster.pos - GameManager::get_player_pos() );
					_cluster.delta = glm::normalize( diff ) * -distance * RECIPROCAL(CLUSTER_TIME_MAX);
					_cluster.spread = std::rand() * RANDF * CLUSTER_SPREAD_MAX + 1.0f;
				}break;
			case SPEWER:
				{
					const float r{ std::rand() * RANDF * static_cast<float>(TAU) };
					const auto p{ GameManager::get_player_pos() };
					const float xr{ std::cos( r ) * SPEWER_OFFSET };
					const float yr{ std::sin( r ) * SPEWER_OFFSET };
					_spewPos = {p.x + xr, p.y + yr};
				}break;
			default: break;
		}
		_firstProc = true;
	}

	const float ctTime{ _attackTimer*(_get_attack_count(_lastAttack)/_get_attack_time(_lastAttack)) };
	if( ctTime > 1.0f + _attackProc )
	{
		switch( _lastAttack )
		{
			case SPEWER:
				if( _attackProc <= 2 )
					break;
				for( int i = 0; i < SPEWER_POINTS; ++i )
				{
					Projectile * const foo{ GameManager::get_projectile() };
					if( nullptr == foo )
						break;

					const auto y{ std::sin( float(i) * RECIPROCAL(SPEWER_POINTS) * TAU ) + _spewPos.y };
					const auto x{ std::cos( float(i) * RECIPROCAL(SPEWER_POINTS) * TAU ) + _spewPos.x };
					const glm::vec3 target{ x, y, GameManager::get_player_pos().z + Z_SPEED_DEFAULT/10 };

					foo->start( target, _exact_spew_pos(), 0.2f, 3.0f, 7.0f, this, 1.2f );
					foo->useModel = model::STAR;
					foo->useTex = texture::RED_TEST;
				}
				break;
			case ODD_BOMBS:
				{
					const auto ub{ _unused_bomb() };
					if( ub == nullptr )
						break;
					ub->fuse = BOMB_FUSE_START;
					const auto ppos{ GameManager::get_player_pos() };
					const float r( std::rand() * RANDF * TAU );
					const auto y{ std::sin( r )*BOMB_SPREAD + ppos.y };
					const auto x{ std::cos( r )*BOMB_SPREAD + ppos.x };
					ub->pos = {x,y,ppos.z+r+2.0f};
				}break;
			default: break;
		}
		++_attackProc;
	}

	//constants
	switch( _lastAttack )
	{
		case CLUSTER:
			_cluster.pos += _cluster.delta * static_cast<float>(FrameTime::get_mod());
			_for_cluster_point( [](glm::vec3 pos, const MovingActor * t)
			{
				HitCircle * const foo{ GameManager::get_hit_circle() };
				if( nullptr == foo ) return;
				foo->start( 9.5f, t, Sphere(pos, CLUSTER_RADIUS) );
			});
			break;
		case BOX_LASER:
			{
				HitCircle * const foo{ GameManager::get_hit_circle() };
				if( nullptr == foo )
					break;
				const float x { _get_laser_x() };
				const auto p { GameManager::get_player_pos() };
				foo-> start( 13.0f, this, { x, p.y, p.z, BOX_LASER_RADIUS } );
			}
			break;
		default: break;
	}
}

void Clown::draw()
{
	const float colorScale{ std::min( _iTime/I_TIME_MAX, 1.0f ) };
	const glm::vec4 col{ 1.0f, colorScale, colorScale, 1.0f };
	const auto t{ static_cast<texture::type_t>(_texture) };
	DrawList::add( {*_model, model::CLOWN, t, col, -1.0f, 0, 1.0f - get_health(), texture::BLOOD_TEX} );

	for( auto & i : _bombs )
	{
		if( i.fuse < BOMB_FUSE_MIN )
			continue;
		glm::mat4 pos{ glm::translate( glm::mat4(), i.pos ) };
		if( i.fuse <= 0.0f )
		{
			pos = glm::rotate( pos, i.pos.y*18, {0.0f,1.0f,1.0f} );
			pos = glm::rotate( pos, i.fuse*9 + i.pos.y, {1.0f,0.0f,1.0f} );
			pos = glm::scale( pos, glm::vec3(BOMB_RADIUS) );
			DrawList::add( {pos, model::DEBUG_SPHERE, texture::THRUSTER_EFFECT, color::RED} );
			continue;
		}
		const float t{ BOMB_FUSE_START - i.fuse * RECIPROCAL( BOMB_FUSE_START ) };
		const glm::vec4 color{ 1.0f, t, t, 1.0f };
		DrawList::add( {pos, model::STAR, texture::PLAYER_SHIP, color} );
	}

	if( _attackTimer <= 0.0f )
		return;

	switch( _lastAttack )
	{
		case CLUSTER:
			_for_cluster_point( [](glm::vec3 p, const MovingActor * t)
			{
				glm::mat4 clusterP{ glm::translate( glm::mat4(), p ) };
				clusterP = glm::scale( clusterP, glm::vec3(CLUSTER_RADIUS) );
				DrawList::add( {clusterP, model::STAR, texture::PLAYER_SHIP, color::WHITE} );
			});
			break;
		case SPEWER:
			{
				glm::mat4 sp{ glm::translate( glm::mat4(), _exact_spew_pos() ) };
				const float rot( _attackTimer * RECIPROCAL(SPEWER_TIME) * TAU );
				sp = glm::rotate( sp, rot, {0.0f, 1.0f, 0.0f } );
				DrawList::add( {sp, model::STAR, texture::ELECTRIC, color::WHITE} );
			}break;
		case BOX_LASER:
			{
				const auto ppos{ GameManager::get_player_pos() };
				glm::mat4 modspace{ glm::translate( glm::mat4(), {_get_laser_x(), ppos.y, ppos.z} ) };
				modspace = glm::rotate( modspace, static_cast<float>(M_PI/2.0f), { 0.0f, 1.0f, 0.0f } );
				modspace = glm::rotate( modspace, static_cast<float>(M_PI/2.0f), { 1.0f, 0.0f, 0.0f } );
				modspace = glm::scale( modspace, { BOX_LASER_RADIUS, BOX_LASER_RADIUS, 200.0f } );

				DrawList::add( {modspace, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, 1.0f} );
			}break;
		default: break;
	}
}

void Clown::start()
{
	BossBase::start();
	if( _myHurt == nullptr and (_myHurt = GameManager::get_hurt_circle()) == nullptr )
		return;

	_lastAttack = TOTAL;

	_myHurt->target = this;
	_myHurt->area->r = SIZE;
	_health = MAX_HEALTH;

	for( auto & i : _bombs )
	{
		i.fuse = BOMB_FUSE_MIN;
	}
}

const char * Clown::get_dialogue_file( bool d ) const
{
	if( _fonzi )
		return d ? "assets/story/fonzi_die.txt" : "assets/story/fonzi_start.txt";
	return d ? "assets/story/bobo_die.txt" : "assets/story/bobo_start.txt";
}

bool Clown::get_hurt( float d, const HurtCircle * from )
{
	if( from != _myHurt )
		return false;

	_health -= d;
	_iTime = 0.0f;
	return true;
}

float Clown::get_health() const
{
	return _health * RECIPROCAL(MAX_HEALTH);
}

float Clown::get_misc() const
{
	return 0.0f;
}

unsigned Clown::get_music() const
{
	return _fonzi ? music::WOOD_MAN : music::ZERO_TWO;
}

bool Clown::_attack_expired() const
{
	return _attackProc > _get_attack_count( _lastAttack );
}

byte Clown::_get_attack_count( attacks_t t ) const
{
	switch( t )
	{
		case CLUSTER: return 1u;
		case SPEWER: return 8u;
		case ODD_BOMBS: return MAX_BOMBS;
		case BOX_LASER:
		case TOTAL: return 1u;
	}
	return 1u;
}

float Clown::_get_attack_time( attacks_t t ) const
{
	switch( t )
	{
		case CLUSTER: return CLUSTER_TIME_MAX;
		case SPEWER: return SPEWER_TIME;
		case BOX_LASER: return BOX_TIME;
		case ODD_BOMBS: return 4.0f;
		case TOTAL: return 1.0f;
	}
	return 1.0f;
}

void Clown::_for_cluster_point( void (*f)(glm::vec3, const MovingActor*) ) const
{
	const float time{_attackTimer*RECIPROCAL(CLUSTER_TIME_MAX)};
	static const glm::vec3 points[]{
		{ 0.0f, 0.0f, 0.0f},
		{ 1.0f, 1.0f, 0.8f},
		{ 2.0f,-1.2f, 0.2f},
		{-1.1f, 0.4f,-0.9f},
		{ 0.2f,-1.6f,-0.4f},
	};
	//static const auto totalPoints{ sizeof(points) / sizeof(*points) };

	for( auto & i: points )
	{
		const glm::vec3 temp{ i * _cluster.spread * time + _cluster.pos };
		f(temp, this);
	}
}

glm::vec3 Clown::_exact_spew_pos() const
{
	const auto p{ GameManager::get_player_pos().z };
	return { _spewPos.x, _spewPos.y, p };
}

float Clown::_get_laser_x() const
{
	const float percent{ (BOX_TIME - _attackTimer) * RECIPROCAL(BOX_TIME) };
	int neg = _fonzi ? -1 : 1;
	return (percent * BOX_DIST_DIFF + BOX_MIN_DIST) * neg + _laserStart;
}

Clown::Bomb * Clown::_unused_bomb()
{
	for( auto & i : _bombs )
	{
		if( i.fuse > BOMB_FUSE_MIN )
			continue;
		return &i;
	}
	return nullptr;
}
