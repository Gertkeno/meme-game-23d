#include "Player.hpp"

#include <glm/common.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <ModelList.hpp>
#include <Wavefront.hpp>
#include <Texture.hpp>
#include <Shader.hpp>
#include <HurtCircle.hpp>
#include <Collision.hpp>
#include <GameManager.hpp>
#include <Projectile.hpp>
#include <HitCircle.hpp>
#include <gert_GLmath.hpp>
#include <ControllerList.hpp>
#include <FrameTime.hpp>
#include <DrawList.hpp>

#ifdef _GERT_DEBUG
#include <iostream>
#endif // _GERT_DEBUG

namespace
{
	constexpr float SCALING{ 2 };
	constexpr float I_TIME{ 0.8f };
	constexpr float SPEED{ 6.0f };
	constexpr float LEAN_SPEED{ 0.3f };
	constexpr float LEAN_MAX{ 15.0f };
	constexpr float LEAN_IDLE_LIMIT{ 0.2f };

	constexpr float BOOST_T{ 1.6f };
	constexpr float SHOT_TIMING{ 0.2f };
	constexpr float SHEILD_T{ 8.0f };
	constexpr float SHEILD_R{ 1.2f };

	constexpr float RETIC_LIMIT{ 6.8f };
	constexpr float RETIC_DIST{ 13.0f };
	constexpr float RETICLE_SCALE{ 0.6f };

	constexpr float MAX_SPECIAL{ 5.0f };
	constexpr float PLASER_RADIUS{ 1.1f };
	constexpr float ROLL_MAX{ 0.87f };
	constexpr float RING_RAD{ 0.8f };

	constexpr float RING_COST{ MAX_SPECIAL * 0.75f };
	constexpr byte RING_SHOTS{ 5u };
	constexpr float ROLL_COST{ MAX_SPECIAL * 0.65f };
}

Player::Player()
{
	//ctor
	_shotCount = 0;
	_sheildTimer = 0.0f;
	_myBonus = bonuses( RING_SHOT | SHEILD | LASER | BARREL_ROLL );
	*_model = glm::scale( glm::mat4(), glm::vec3( SCALING ) );

	_delta = new glm::vec3;
	_thruster = new glm::vec3{ 0.0f, 0.0f, -0.61080f };

	_reticle = new glm::vec3;

	///Pain
	_iTime = 0;
	_myHurt = nullptr;
	_frameInvicible = false;
	health = 0;
	_laserOn = false;
	_specialTimer = 0.0f;
	_lastShot = 0;

	///animation
	_active = false;
	_lastModelTrans = new glm::mat4;

	_rollTime = ROLL_MAX;
}

Player::~Player()
{
	//dtor
	delete _delta;
	delete _reticle;
	delete _thruster;
	delete _lastModelTrans;
}

void Player::update( void )
{
	///Clock updates
	_iTime += FrameTime::get_mod();
	_lastShot += FrameTime::get_mod();
	_specialTimer += FrameTime::get_mod();
	_rollTime += FrameTime::get_mod();
	if( _specialTimer > MAX_SPECIAL ) _specialTimer = MAX_SPECIAL;
	if( _iTime > I_TIME ) _sheildTimer += FrameTime::get_mod();
	_frameInvicible = false;
	*_reticle = get_vec_point();
	_reticle->z += RETIC_DIST;

	///Movement
	_leanXNormal = gControls[control_t::RIGHT] - gControls[control_t::LEFT];
	_leanYNormal = gControls[control_t::UP] - gControls[control_t::DOWN];
	_delta->y = -_leanYNormal * SPEED;
	_delta->x = -_leanXNormal * SPEED;

	///SPEED
	if( gControls[control_t::BOOST] > 0.5f )
	{
		_delta->z = Z_SPEED_DEFAULT*2;
		_delta->y *= BOOST_T;
		_delta->x *= BOOST_T;
		if( equal_bonus( MORE_SPEED ) )
		{
			_delta->y *= 1.5f;
			_delta->x *= 1.5f;
		}
	}
	else if( gControls[control_t::BRAKE] > 0.5f )
	{
		_delta->z = 0.0f;
		_rollTime = ROLL_MAX;
	}
	else
	{
		_delta->z = Z_SPEED_DEFAULT;
	}

	///SPECIALS
	_reticle->y -= _leanYNormal * RETIC_LIMIT;
	_reticle->x -= _leanXNormal * RETIC_LIMIT;
	if( gControls[control_t::SPECIAL] > 0.5f )
	{
		if( gControls[control_t::STAY] and equal_bonus( LASER ) and !gControls[control_t::BRAKE] and _specialTimer > MAX_SPECIAL*0.2f )
		{
			_laserOn = true;
		}
		else if( gControls[control_t::CONFIRM] and equal_bonus( RING_SHOT ) and _specialTimer > MAX_SPECIAL*0.75f )
		{
			// make ring shot
			_specialTimer -= RING_COST;
			_lastShot = 0.0f;
			for( char i = 0; i < RING_SHOTS; i++ )
			{
				glm::vec3 ctPos{ *_reticle };
				ctPos.y += sin( i*RECIPROCAL(RING_SHOTS) * TAU ) * RING_RAD;
				ctPos.x += cos( i*RECIPROCAL(RING_SHOTS) * TAU ) * RING_RAD;
				Projectile* foo;
				IF_SHOT_GET( foo )
				{
					static constexpr float damage{ 2.6f };
					static constexpr float life{ 3.0f };
					static constexpr float speed{ 1.2f };
					foo->start( ctPos, get_vec_point(), speed, life, damage, this );
				}
			}
		}
		else if( _rollTime > ROLL_MAX and gControls[control_t::BOOST] and equal_bonus( BARREL_ROLL ) and _specialTimer > MAX_SPECIAL*0.35f )
		{
			_specialTimer -= ROLL_COST;
			_rollTime = 0.0f;
		}
	}
	if( gControls[control_t::SPECIAL] < 0.5f or gControls[control_t::BRAKE] > 0.5f or gControls[control_t::STAY] < 0.5f or _specialTimer <= 0.0f )
	{
		_laserOn = false;
	}
	else if( _laserOn )
	{
		_specialTimer -= (2.5f)*FrameTime::get_mod();
	}

	///SHOOTING
	if( gControls[control_t::CONFIRM] > 0.5f and _lastShot > SHOT_TIMING )//bullet projectile shot shoot
	{
		Projectile* foo;
		IF_SHOT_GET( foo )
		{
			float damage{ 1.0f };
			float scale{ 0.8f };
			float speed{ 1.4f };
			if( equal_bonus( BIG_SHOT ) and _shotCount%3 == 0 )
			{
				damage *= 2; scale *= 2.5f;
			}
			if( equal_bonus( EXPRESS_SHOT ) and gControls[control_t::BOOST] > 0.5f )
			{
				damage *= 1.7f; speed *= 1.5f;
			}
			foo->start( *_reticle, get_vec_point(), speed, 2, damage, this, scale );
			_shotCount++;
		}
		_lastShot = 0;
	}
	if( _laserOn )
	{
		HitCircle* foo;
		IF_HIT_GET( foo )
		{
			const glm::vec3 axis{ gert::normal_points( get_vec_point(), *_reticle ) };
			const Sphere ar{ axis * -( get_vec_point().z - GameManager::get_boss_pos().z ) + get_vec_point(), PLASER_RADIUS };
			const float damage{ 2 * static_cast<float>(FrameTime::get_mod()) };
			foo->start( damage, this, ar );
		}
	}

	///SET POSITION
	_delta->x = _delta->x * FrameTime::get_mod() * RECIPROCAL(SCALING);
	_delta->y = _delta->y * FrameTime::get_mod() * RECIPROCAL(SCALING);
	_delta->z = _delta->z * FrameTime::get_mod() * RECIPROCAL(SCALING);
	if( gControls[control_t::STAY] > 0.5f )
	{
		_delta->y = 0;
		_delta->x = 0;
	}
	*_model = glm::translate( *_model, *_delta );

	///set hurtcirc
	if( _myHurt != nullptr )
	{
		*(_myHurt->area) = Sphere{ get_vec_point(), SCALING*0.6f };
	}
}

void Player::draw( void )
{
	float cracks{ health < 66.6f ? 1 - health*0.01f : 0.0f };

	const glm::vec4 cmod{ [&]()
	{
		if( _iTime < I_TIME )
		{
			const float p{ _iTime/I_TIME };
			return glm::vec4{ 1.0f, p, p, static_cast<int>(_iTime*10)%2 ? 1.0f : 0.2f };
		}
		return color::WHITE;
	}()};

	*_lastModelTrans = *_model;
	const auto& pos{ get_vec_point() };
	const glm::vec3 rot{ gert::get_angles( pos, *_reticle ) };
	float roll{ -M_PI/2.0f };
	if( _rollTime < ROLL_MAX ) roll += (_rollTime*RECIPROCAL(ROLL_MAX))*TAU;
	*_lastModelTrans = glm::rotate( *_lastModelTrans, roll, { 0, 0, 1 } );
	*_lastModelTrans = glm::rotate( *_lastModelTrans, rot.y, { 0, 1, 0 } );
	*_lastModelTrans = glm::rotate( *_lastModelTrans, rot.x, { 1, 0, 0 } );
	DrawList::add( {*_lastModelTrans, model::PLAYER_SHIP, texture::PLAYER_SHIP, cmod, -1.0f, 0, cracks, texture::CRACK_TEX} );
	if( equal_bonus( SHEILD ) )
	{
		const auto sheildR{ _sheildTimer/SHEILD_T * SHEILD_R };
		const Sphere sheildSpot{ pos, std::min( SHEILD_R, sheildR ) };
		glm::mat4 cts = sheildSpot.ct_mat();
		if( _sheildTimer >= SHEILD_T ) cts = glm::rotate( cts, _sheildTimer-SHEILD_T, { 0.5f, -0.5f, 0.0f } );
		DrawList::add( {cts, model::DEBUG_SPHERE, texture::YELLOW_TEST, cmod} );
	}
	if( _laserOn )
	{
		glm::mat4 modspace;
		static constexpr float dist{ 90.0f };
		modspace = glm::translate( modspace, pos );
		glm::vec3 rotTo = gert::get_angles( *_reticle, pos );//rotating
		modspace = glm::rotate( modspace, rotTo.y, { -1.0f, 0.0f, 0.0f } );
		modspace = glm::rotate( modspace, rotTo.x, { 0.0f, -1.0f, 0.0f } );
		modspace = glm::translate( modspace, { 0.0f, 0.0f, -dist } );
		modspace = glm::scale( modspace, { PLASER_RADIUS, PLASER_RADIUS, dist } );
		DrawList::add( {modspace, model::CYLINDER, texture::RED_TEST, color::WHITE, 0.0f, 0, 0.0f, 0, 1.0f} );
	}
}

void Player::reticle_draw( void )
{
	///thruster
	glm::vec3 realthrust = glm::vec3( *_lastModelTrans * glm::vec4( *_thruster, 1.0f ) );
	*_lastModelTrans = glm::translate( glm::mat4(), realthrust );
	float scaling{ 0.66666f };
	int frame{ 1 };
	if( gControls[control_t::BOOST] > 0.5f )
	{
		scaling = 1.0f;
		frame = 2;
	}
	else if( gControls[control_t::BRAKE] > 0.5f )
	{
		scaling = 0.3333333f;
		frame = 0;
	}
	*_lastModelTrans = glm::scale( *_lastModelTrans, glm::vec3( scaling ) );
	static constexpr float THRUST_WAVE_S{ 0.13f };
	static constexpr float THRUST_WAVE_R{ 1.0f - THRUST_WAVE_S };
	const float wavx{ THRUST_WAVE_S * static_cast<float>(cos(_iTime/0.677f*TAU)) };
	const float wavy{ THRUST_WAVE_S * static_cast<float>(sin(_iTime/0.677f*TAU)) };
	*_lastModelTrans = glm::scale( *_lastModelTrans, { THRUST_WAVE_R + wavx, THRUST_WAVE_R + wavy, 1.0f } );
	DrawList::add( {*_lastModelTrans, model::PLANE_2D, texture::THRUSTER_EFFECT, color::WHITE, 0.0f, frame, 0.0f, 0, 1.0f} );
	///reticle
	//gert::Shader::Active_Shader->set_uniform( "negativeColor", (_specialTimer/MAX_SPECIAL) );
	const float specialPercent{ _specialTimer/MAX_SPECIAL };
	*_lastModelTrans = glm::translate( glm::mat4(), *_reticle );
	*_lastModelTrans = glm::scale( *_lastModelTrans, glm::vec3(RETICLE_SCALE) );
	//since reticle is drawn twice just capture a function call
	const auto quickDraw{[&](){
		DrawList::add( {*_lastModelTrans, model::PLANE_2D, texture::RETICLE, color::WHITE, 0.0f, 0, 0.0f, 0, 1.0f, specialPercent} );
	}};
	quickDraw();
	const auto& p{ get_vec_point() };
	const glm::vec3 midPoint{ 0.5f * glm::vec3{ p.x - _reticle->x, p.y - _reticle->y, p.z - _reticle->z } };
	*_lastModelTrans = glm::translate( *_lastModelTrans, midPoint / RETICLE_SCALE );
	*_lastModelTrans = glm::rotate( *_lastModelTrans, static_cast<float>(M_PI*0.25f), glm::vec3( 0, 0, 1 ) );
	quickDraw();
}

bool Player::get_hurt( float damage, const HurtCircle* from )
{
	if( from != _myHurt ) return false;
	if( damage < 0 )
	{
		health -= damage;
		return true;
	}
	else if( damage == 0 )
	{
		_frameInvicible = true;
		return true;
	}
	if( _frameInvicible ) return false;
	if( _iTime <= I_TIME or _rollTime < ROLL_MAX ) return false;

	///typical hit registering, bonus effect etcs
	if( equal_bonus( REVENGE ) )
	{
		Projectile* foo;
		IF_SHOT_GET( foo )
		{
			foo->start( GameManager::get_boss_pos(), get_vec_point(), 2.0f, 2, damage/4, this, 1.3f );
		}
	}
	if( equal_bonus( SHEILD ) and _sheildTimer > SHEILD_T )
	{
		damage = 0;
		_sheildTimer = 0;
	}
	_iTime = 0;
	health -= damage;
	return true;
}

void Player::start( void )
{
	_rollTime = ROLL_MAX;
	_iTime = I_TIME;
	set_vec_point( glm::vec3(0.0f) );//reset position to 0.0 origin
	*_delta = glm::vec3(0.0f);//delta set to 0.0
	_lastShot = SHOT_TIMING;//ready next shot
	_specialTimer = 0.0f;
	_sheildTimer = 0.0f;
	health = 100.0f;
	_active = true;
	_myHurt = GameManager::get_hurt_circle();
	_myHurt->target = this;
}

bool Player::get_active( void )
{
	return health > 0;
}

bool Player::equal_bonus( bonuses compare )
{
	return (_myBonus & compare) == compare;
}

void Player::include_bonus( bonuses add )
{
	_myBonus = bonuses( _myBonus | add );
}

glm::vec3 Player::get_reticle()
{
	return *_reticle;
}

Player::bonuses Player::get_bonuses()
{
	return _myBonus;
}

void Player::reset()
{
	_shotCount = 0;
	_sheildTimer = 0.0f;
	_myBonus = bonuses(0);

	///Pain
	_iTime = I_TIME;
	_frameInvicible = false;
	health = 0.0f;
	_laserOn = false;
	_specialTimer = 0.0f;
	_lastShot = 0.0f;
	_rollTime = ROLL_MAX;

	///animation
	_leanYNormal = 0.0f;
	_leanXNormal = 0.0f;
	_active = false;
}
